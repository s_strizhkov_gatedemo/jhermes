package ru.verna.jhermes.actions.logic.ns;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

class NsSkipassTest {

    private static final String url = "products/ns/processSkipassReceipts";
    private static RequestSpecification spec;

    @BeforeAll
    static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder builder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_SKYNET);
        spec = builder.build();
    }

    /**
     * Прогрузка чеков Скипасс.
     */
    @Test
    void processScipassReceipts() {
        String date = LocalDateTime.now().minusDays(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));
        String fpd = "3123456789";
        String params = "{\n"
                + "    'taxcom': {\n"
                + "        'fd': {\n"
                + "            'type': '0',\n"
                + "            'fpd': '" + fpd + "',\n"
                + "            'date': '" + date + "',\n"
                + "            'sum': '1500.00',\n"
                + "            'path': '\\\\\\\\St03.exprem.dom\\\\kiasfoto\\\\Receipt\\\\00\\\\02.pdf',\n"
                + "            'items': {\n"
                + "                'row': [\n"
                + "                    {\n"
                + "                        'item': 'Билет',\n"
                + "                        'price1': '1500.00',\n"
                + "                        'param1212': '0',\n"
                + "                        'param1214': '4',\n"
                + "                        'price2': '1500.00'\n"
                + "                    },\n"
                + "                    {\n"
                + "                        'item': 'Билет',\n"
                + "                        'price1': '1500.00',\n"
                + "                        'param1212': '0',\n"
                + "                        'param1214': '4',\n"
                + "                        'price2': '1500.00'\n"
                + "                    }\n"
                + "                ]\n"
                + "            }\n"
                + "        }\n"
                + "    }\n"
                + "}";
        String response = given().spec(spec)
                .param("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .get(url)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.taxcom.fd.fpd", equalTo(fpd))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(url, response);
    }
}
