package ru.verna.jhermes.actions.logic.services;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;
import javax.servlet.http.HttpServletResponse;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

class DownloadAgrPaymentDataTest {

    private static final String downloadAgrPaymentDataUrl = "loadAgrPaymentData";
    private static RequestSpecification spec;

    @BeforeAll
    static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder builder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_SKYNET);
        spec = builder.build();
    }

    /**
     * Прогрузка чеков.
     */
    @Test
    void downloadAgrPaymentData() {
        String params = "{\n"
                + "    'payments': {\n"
                + "        'payment': [\n"
                + "            {\n"
                + "                'agrcalcid': '11125-ВЗР-5H0Y2',\n"
                + "                'transactiondate': '14.09.2018',\n"
                + "                'status': 'APPROVED',\n"
                + "                'filepath': '\\\\\\\\St03.exprem.dom\\\\kiasfoto\\\\Receipt\\\\test.pdf',\n"
                + "                'filename': 'test.pdf'\n"
                + "            },\n"
                + "            {\n"
                + "                'agrcalcid': '10596-НС-RQLEL',\n"
                + "                'transactiondate': '14.09.2018',\n"
                + "                'status': 'REFUNDED',\n"
                + "                'filepath': '\\\\\\\\St03.exprem.dom\\\\kiasfoto\\\\Receipt\\\\test.pdf',\n"
                + "                'filename': 'test.pdf'\n"
                + "            }\n"
                + "        ]\n"
                + "    }\n"
                + "}";
        String response = given().spec(spec)
                .param("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .get(downloadAgrPaymentDataUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.verify_result", equalTo("OK"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(downloadAgrPaymentDataUrl, response);
    }

    /**
     * Неудачная прогрузка чеков, из за незаполненого agrcalcid.
     */
    @Test
    void downloadAgrPaymentDataFail() {
        String params = "{\n"
                + "    'payments': {\n"
                + "        'payment': [\n"
                + "            {\n"
                + "                'transactiondate': '14.09.2018',\n"
                + "                'status': 'APPROVED',\n"
                + "                'filepath': '\\\\\\\\St03.exprem.dom\\\\kiasfoto\\\\Receipt\\\\test.pdf',\n"
                + "                'filename': 'test.pdf'\n"
                + "            },\n"
                + "            {\n"
                + "                'agrcalcid': '10596-НС-RQLEL',\n"
                + "                'transactiondate': '14.09.2018',\n"
                + "                'status': 'REFUNDED',\n"
                + "                'filepath': '\\\\\\\\St03.exprem.dom\\\\kiasfoto\\\\Receipt\\\\test.pdf',\n"
                + "                'filename': 'test.pdf'\n"
                + "            }\n"
                + "        ]\n"
                + "    }\n"
                + "}";
        String response = given().spec(spec)
                .param("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .get(downloadAgrPaymentDataUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code", equalTo("105"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(downloadAgrPaymentDataUrl, response);
    }
}
