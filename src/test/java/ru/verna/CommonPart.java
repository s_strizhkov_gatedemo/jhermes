package ru.verna;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import net.minidev.json.JSONArray;
import org.postgresql.ds.PGSimpleDataSource;
import ru.verna.commons.Utils;
import ru.verna.commons.log.Log;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.Properties;
import static io.restassured.RestAssured.given;
import static io.restassured.config.SSLConfig.sslConfig;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.notNullValue;

/**
 *
 */
public class CommonPart {

    /**
     * Токен пользователя www.
     */
    public static final String TOKEN_USER_WWW = "9e0c10d6a4e852f307ba4df305349f5becb7fd03dc4ff515d8839918865a69abd6341f9543d8eddd7d8d05e7d49b68db1c9f5f19a020b56f1bf6d1d4198f6760";
    /**
     * Токен пользователя userbolt.
     */
    public static final String TOKEN_USER_SKYNET = "560010f7b054f5a23e24cb59c81a06a0a157880809c73f73a3a9826fbc51ccead117bca5e4bc7c84aa23110e494b5f78954f875cdf5e070ce662baa2ffd42a89";
    /**
     * Токен пользователя public.
     */
    public static final String TOKEN_USER_PUBLIC = "tokenofpublicuser";
    /**
     * Токен пользователя ns.
     */
    public static final String TOKEN_USER_NS = "9e0c10d6a4e852f307ba4df305349f5becb7fd03dc4ff515d8839918865a69abd6341f9543d8eddd7d8d05e7d49b68db1c9f5f19a020b56f1bf6d1d4198f6760";
    /**
     * Токен пользователя cabinet.
     */
    public static final String TOKEN_CABINET_USER = "d7a3a11316d48b57560626001d57b97ff4fd86452c6cbba91750220ef28df13a94ba280c21a9f2774a1d271e8e9d8247c316a38201caf4b1565ba9875685108c";
    /**
     * Токен пользователя medins.
     */
    public static final String TOKEN_MEDINS_USER = "CB3564A5F04B012AA9B03F80427152F3B6A8BB1D92480F87612800C8EB7146EDA9E87D3A1C6E1F3239537CE9C788808435568A093C69C86827D1848912E43EA6";
    /**
     * Токен пользователя aplus.
     */
    public static final String TOKEN_APLUS_USER = "1912FAA9DE0194312E99A8CB1D9C2A6A68901EDCF25301EDB1E50B0E144EF7051628A2DD422AC1FE08C53F23AA39E42B88C7D9BCC6C3327C0BD3EAEB63CF4563";
    /**
     * Токен тестового пользователя user1.
     */
    public static final String TOKEN_TEST_USER = "user1111";

    private static final String RESOURCES_FILE_PATH = "test.properties";

    private static final Properties testProperties = new Properties();

    static {
        try (InputStream resource = CommonPart.class.getClassLoader().getResourceAsStream(RESOURCES_FILE_PATH)) {
            testProperties.load(resource);
        } catch (IOException e) {
            Log.TEST.error("Cannot read test resources = {}", e.getMessage());
        }
    }

    public static void setServer() {
        String testableAppserverScheme = System.getProperty("testable_appserver_scheme", "http");
        String testableAppserverHost = System.getProperty("testable_appserver_host", "localhost");
        Integer testableAppserverPort = Integer.parseInt(System.getProperty("testable_appserver_port", "8080"));
        String testableAppserverContextRoot = System.getProperty("testable_appserver_contextroot", "gate");
        /**
         * Хост тестируемого сервера приложений.
         */
        RestAssured.baseURI = testableAppserverScheme + "://" + testableAppserverHost;
        /**
         * Порт тестируемого сервера приложений.
         */
        RestAssured.port = testableAppserverPort;
        /**
         * Контекст приложения (путь) на тестируемом сервере приложений.
         */
        RestAssured.basePath = "/" + testableAppserverContextRoot;
        /**
         * Добавление поддержки SSL в конфигурацию
         */
        RestAssured.config = RestAssured.config().sslConfig(sslConfig().allowAllHostnames().relaxedHTTPSValidation());
    }

    public static Connection getConnection() throws SQLException {
        PGSimpleDataSource dataSource = new PGSimpleDataSource();
        dataSource.setUrl("jdbc:postgresql://" + System.getProperty("postgres_host", "localhost") + ":5432/jhermes");
        dataSource.setUser(getProperty("db.user"));
        dataSource.setPassword(getProperty("db.password"));
        return dataSource.getConnection();
    }

    public static String formatLocalDate(LocalDate localDate) {
        return localDate.format(Utils.DATE_FORMATTER);
    }

    public static String getProperty(String key) {
        return testProperties.getProperty(key);
    }

    public static RequestSpecBuilder getRequestSpecBuilder(String userToken) {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.addHeader(Log.CALL_CONTEXT_HEADER, Log.TEST_CALL_CONTEXT);
        if (Utils.notEmpty(userToken)) {
            builder.addHeader("authorization", "Bearer " + userToken);
        }
        return builder;
    }

    /**
     * Точка входа для процесса записи результатов КИАС запросов в БД.
     *
     * @param actionId ID действия, соответсвует полю ID в таблице public.actions
     * @param responseBody тело ответа, в формате json строки
     */
    public static void writeToDb(String actionId, String responseBody) {
        if (isWriteToDb() && Utils.notEmpty(responseBody)) {
            String decreasedArrayLengthBody = decreaseArrayLength(responseBody);
            if (isRewritable(actionId)) {
                writeResponseForAction(actionId, decreasedArrayLengthBody);
            }
        }
    }

    /**
     * Проверяет признак is_rewritable в таблице public.actions_data, указывающий являются ли данные по переданому
     * действию перезаписываемыми или их необходимо править руками.
     *
     * @param actionId ID действия, соответсвует полю ID в таблице public.actions
     * @return значение поля is_rewritable
     */
    private static boolean isRewritable(String actionId) {
        boolean result = true;
        final String sql = "SELECT is_rewritable FROM public.actions_data WHERE id = ?";
        try (Connection connection = getConnection();
                PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, actionId);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    result = resultSet.getBoolean(1);
                }
            }
        } catch (SQLException e) {
            Log.TEST.error(e.getLocalizedMessage());
        }
        return result;
    }

    /**
     * Проверяем задано ли в системе свойство writeToDb, регулирующее необходимость записи ответов в БД.
     *
     * @return true если свойство найдено и равно true, в противном случае false
     */
    private static boolean isWriteToDb() {
        return Boolean.parseBoolean(System.getProperty("writeToDb"));
    }

    /**
     * Если тело ответа содержит список объектов по пути data-result-{arrayName}, то уменьшаем размер до двух элементов.
     *
     * @param responseBody ответ сервиса КИАС
     * @return ответ с уменьшенным списком или неизмененнный ответ
     */
    private static String decreaseArrayLength(String responseBody) {
        DocumentContext documentContext = JsonPath.parse(responseBody);
        JSONArray jsonArray = null;
        String arrayPath = "$.data.result.row";
        try {
            // ищем список по пути $.data.result.row
            jsonArray = documentContext.read(arrayPath);
        } catch (PathNotFoundException e) {
            // пусто, так как нам не надо ничего обрабатывать, нужен просто факт есть ли указанный путь в респонсе
        }
        try {
            // если не нашли $.data.result.row по пути то ищем по $.data.result.bso
            if (jsonArray == null) {
                arrayPath = "$.data.result.bso";
                jsonArray = documentContext.read(arrayPath);
            }
        } catch (PathNotFoundException e) {
            // пусто, так как нам не надо ничего обрабатывать, нужен просто факт есть ли указанный путь в респонсе
        }
        if (jsonArray != null) {
            Iterator<Object> iterator = jsonArray.iterator();
            int count = 2;
            while (iterator.hasNext()) {
                iterator.next();
                if (count <= 0) {
                    iterator.remove();
                }
                count--;
            }
            documentContext.set(arrayPath, jsonArray);
        }
        return documentContext.jsonString();
    }

    /**
     * Для переданного действия создает или обновляет в БД тело ответа сервиса КИАС.
     *
     * @param actionId ID действия
     * @param responseBody тело ответа, в формате json строки
     */
    private static void writeResponseForAction(String actionId, String responseBody) {
        String theRightColumnFromDB = getTheRightColumnFromDB(responseBody);
        final String sql = "INSERT INTO public.actions_data(id, " + theRightColumnFromDB + ") VALUES (?, ?) "
                + "ON CONFLICT (id) DO UPDATE SET " + theRightColumnFromDB + " = ? WHERE actions_data.id = ?";
        try (Connection connection = getConnection();
                PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, actionId);
            ps.setString(2, responseBody);
            ps.setString(3, responseBody);
            ps.setString(4, actionId);
            ps.execute();
        } catch (SQLException e) {
            Log.TEST.error(e.getLocalizedMessage());
        }
    }

    /**
     * Возвращает имя колонки в таблице public.actions в которую будет произведена запись.
     *
     * @param responseBody тело ответа, для проверки на тип ответа (успешный или ошибочный)
     * @return имя колонки
     */
    private static String getTheRightColumnFromDB(String responseBody) {
        return isSuccessResponse(responseBody) ? "success_resp" : "error_resp";
    }

    /**
     * Парсит переданный ответ на наличии определенного тэга, для определения типа запроса.
     *
     * @param responseBody тело ответа
     * @return false если в теле ответа найден тэг data.error, в противном случае true
     */
    private static boolean isSuccessResponse(String responseBody) {
        try {
            JsonPath.read(responseBody, "$.data.error");
            return false;
        } catch (PathNotFoundException e) {
            // пусто, так как нам не надо ничего обрабатывать, нужен просто факт есть ли указанный путь в респонсе
        }
        return true;
    }

    /**
     * Созданет полную котировку НС-Онлайн и возвращает ее {@code calcId}.
     *
     * @return {@code calcId} созданной котировки
     */
    public static String getCalcId() {
        RequestSpecBuilder nsBuilder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_NS);
        RequestSpecification nsSpec = nsBuilder.build();
        final String startDate = CommonPart.formatLocalDate(LocalDate.now().plusDays(1));
        final String endDate = CommonPart.formatLocalDate(LocalDate.now().plusMonths(2));
        String params = "{\n"
                + "  'term': {\n"
                + "    'begin': '" + startDate + "',\n"
                + "    'end': '" + endDate + "'\n"
                + "  },\n"
                + "  'coverage': {\n"
                + "    'risks': {\n"
                + "      'risk': [\n"
                + "        {\n"
                + "          'id': 'ucRiskNSDeath',\n"
                + "          'currency': 'ucCurrencyRUB',\n"
                + "          'insuredSumm': 100000\n"
                + "        },\n"
                + "        {\n"
                + "          'id': 'ucRiskNSDisablement123',\n"
                + "          'currency': 'ucCurrencyRUB',\n"
                + "          'insuredSumm': 100000\n"
                + "        }\n"
                + "      ]\n"
                + "    }\n"
                + "  },\n"
                + "  'attributes': {\n"
                + "        'attribute': [\n"
                + "            {\n"
                + "                'id': 'ucAddAtrNSCoveragePeriod',\n"
                + "                'value': '1329041'\n"
                + "            },\n"
                + "            {\n"
                + "                'id': 'ucAddAtrNSCoverageArea',\n"
                + "                'value': '1326811'\n"
                + "            },\n"
                + "            {\n"
                + "                'id': 'ucAddAtrNSKindOfSport',\n"
                + "                'value': '1327471'\n"
                + "            },\n"
                + "            {\n"
                + "                'id': 'ucAddAtrNSOccupation',\n"
                + "                'value': '1328211'\n"
                + "            },\n"
                + "            {\n"
                + "                'id': 'ucAddAtrCommon_PromoCode',\n"
                + "                'value': ''\n"
                + "            },\n"
                + "            {\n"
                + "                'id': 'ucPartnerId',\n"
                + "                'value': 'ucVernaTest'\n"
                + "            },\n"
                + "            {\n"
                + "                'id': 'ucAddAtrNSKindOfSport_Other',\n"
                + "                'value': 'Баскетбол'\n"
                + "            }\n"
                + "        ]\n"
                + "    },\n"
                + "  'insurants': {\n"
                + "    'insurant': [\n"
                + "      {\n"
                + "        'isn': '',\n"
                + "        'firstName': 'Гермес',\n"
                + "        'lastName': 'Гермесов',\n"
                + "        'middleName': 'Гермесович',\n"
                + "        'birthDate': '01.01.1980',\n"
                + "        'gender': 'М',\n"
                + "        'documents': {\n"
                + "          'document': [\n"
                + "            {\n"
                + "              'date': '02.01.1998',\n"
                + "              'issuedBy': 'УФМС',\n"
                + "              'number': '010101',\n"
                + "              'series': '0715',\n"
                + "              'type': 'ucRFPassport'\n"
                + "            }\n"
                + "          ]\n"
                + "        },\n"
                + "        'address': {\n"
                + "          'region': {\n"
                + "            'custom_value': 'г Москва',\n"
                + "            'name': 'Москва',\n"
                + "            'abbr': 'г',\n"
                + "            'code': '7700000000000'\n"
                + "          },\n"
                + "          'district': {\n"
                + "            'custom_value': ''\n"
                + "          },\n"
                + "          'city': {\n"
                + "            'custom_value': 'г Москва',\n"
                + "            'name': 'Москва',\n"
                + "            'abbr': 'г',\n"
                + "            'code': '7700000000000'\n"
                + "          },\n"
                + "          'locality': {\n"
                + "            'custom_value': ''\n"
                + "          },\n"
                + "          'street': {\n"
                + "            'custom_value': 'ул Тверская',\n"
                + "            'name': 'Тверская',\n"
                + "            'abbr': 'ул',\n"
                + "            'code': '77000000000287700'\n"
                + "          },\n"
                + "          'house': '12',\n"
                + "          'building': '',\n"
                + "          'flat': '',\n"
                + "          'postcode': '125009'\n"
                + "        }\n"
                + "      }\n"
                + "    ]\n"
                + "  },\n"
                + "  'promoCode': '321',\n"
                + "  'insurer': {\n"
                + "    'isn': '',\n"
                + "    'birthDate': '01.01.1980',\n"
                + "    'firstName': 'Гермес',\n"
                + "    'middleName': 'Гермесович',\n"
                + "    'lastName': 'Гермесов',\n"
                + "    'phone': '+79381111111',\n"
                + "    'email': 'verna@test.ru',\n"
                + "    'gender': 'Ж',\n"
                + "    'documents': {\n"
                + "      'document': [\n"
                + "        {\n"
                + "          'type': 'ucRFPassport',\n"
                + "          'series': '0715',\n"
                + "          'number': '111111',\n"
                + "          'date': '02.01.1998',\n"
                + "          'issuedBy': 'УФМС'\n"
                + "        }\n"
                + "      ]\n"
                + "    },\n"
                + "    'address': {\n"
                + "      'region': {\n"
                + "        'custom_value': 'г Москва',\n"
                + "        'name': 'Москва',\n"
                + "        'abbr': 'г',\n"
                + "        'code': '7700000000000'\n"
                + "      },\n"
                + "      'district': {\n"
                + "        'custom_value': ''\n"
                + "      },\n"
                + "      'city': {\n"
                + "        'custom_value': 'г Москва',\n"
                + "        'name': 'Москва',\n"
                + "        'abbr': 'г',\n"
                + "        'code': '7700000000000'\n"
                + "      },\n"
                + "      'locality': {\n"
                + "        'custom_value': ''\n"
                + "      },\n"
                + "      'street': {\n"
                + "        'custom_value': 'ул Тверская',\n"
                + "        'name': 'Тверская',\n"
                + "        'abbr': 'ул',\n"
                + "        'code': '77000000000287700'\n"
                + "      },\n"
                + "      'house': '12',\n"
                + "      'building': '',\n"
                + "      'flat': '',\n"
                + "      'postcode': '125009'\n"
                + "    }\n"
                + "  },\n"
                + "  'clauses': {\n"
                + "    'clause': [\n"
                + "      {\n"
                + "        'id': 'ucClausesCommonSalesChannel',\n"
                + "        'value': 'ucClausesCommonSalesChannel_Online'\n"
                + "      }\n"
                + "    ]\n"
                + "  },\n"
                + "  'calcId': '',\n"
                + "  'isn': '',\n"
                + "  'premium': '',\n"
                + "  'remark': 'Эта котировка создана автотестами'\n"
                + "}";
        String response = given().spec(nsSpec)
                .header("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
                .formParam("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .post("products/ns/createCalc")
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.calcId", notNullValue())
                .body("data.result.insurer.isn.toInteger()", isA(int.class))
                .body("data.result.insurants.insurant.isn.toInteger()", isA(int.class))
                .extract().response().getBody().asString();
        return JsonPath.read(response, "data.result.calcId");
    }
}
