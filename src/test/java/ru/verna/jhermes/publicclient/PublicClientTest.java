package ru.verna.jhermes.publicclient;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;
import javax.servlet.http.HttpServletResponse;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

/**
 * Проверка действий для приложения /public.
 */
class PublicClientTest {

    private static final String getCalcInfoExtendedUrl = "getCalcInfoExtended";
    private static final String getInvoiceInfoUrl = "getInvoiceInfo";
    private static final String setInvoicePaidUrl = "invoiceSetPaid";
    private static final String getDocumentUrl = "getDocument";
    private static final String getLicenseUrl = "getLicense";

    private static RequestSpecification spec;
    private static RequestSpecification nsSpec;
    private static String calcId;

    @BeforeAll
    static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder builder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_PUBLIC);
        spec = builder.build();
        RequestSpecBuilder nsBuilder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_NS);
        nsSpec = nsBuilder.build();
        calcId = CommonPart.getCalcId();
    }

    /**
     * Просто вызов getLicense
     */
    @Test
    void getLicense() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(getLicenseUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.AppName", is("WEBSALE"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(getLicenseUrl, response);
    }

    @Test
    void getLicenseFail() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(getLicenseUrl + 1)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code", is("104"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(getLicenseUrl, response);
    }

    /**
     * Получение информации о статусе котировки и полиса
     */
    @Test
    void getCalcInfoExtended() {
        String response = given().spec(spec)
                .param("calc_id", calcId)
                .log().ifValidationFails(LogDetail.URI)
                .get(getCalcInfoExtendedUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.product.product_isn", equalTo("1280351"),
                        "data.result.calc.calc_id", equalTo(calcId))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(getCalcInfoExtendedUrl, response);
    }

    @Test
    void getCalcInfoExtendedFail() {
        String response = given().spec(spec)
                .param("calc_id", "xxx")
                .log().ifValidationFails(LogDetail.URI)
                .get(getCalcInfoExtendedUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.text", startsWith("Котировка с заданным идентификатором"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(getCalcInfoExtendedUrl, response);
    }

    /**
     * Получение информации о счете.
     */
    @Test
    void getInvoiceInfo() {
        String response = given().spec(spec)
                .param("isn", "102290483")
                .log().ifValidationFails(LogDetail.URI)
                .get(getInvoiceInfoUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.number", equalTo("ЗКВ-ОП-000-096401/18"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(getInvoiceInfoUrl, response);
    }

    @Test
    void getInvoiceInfoFail() {
        String response = given().spec(spec)
                .param("isn", "1111111111")
                .log().ifValidationFails(LogDetail.URI)
                .get(getInvoiceInfoUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.text", equalTo("Счет на оплату не найден."))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(getInvoiceInfoUrl, response);
    }

    /**
     * Фиксация оплаты счета.
     */
    @Test
    void setInvoicePaid() {
        given().spec(spec)
                .param("isn", "102290483")
                .log().ifValidationFails(LogDetail.URI)
                .get(setInvoicePaidUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.success", equalTo("1"))
                .extract().response().getBody().asString();
    }

    @Test
    void setInvoicePaidFail() {
        given().spec(spec)
                .param("isn", "1111111111")
                .log().ifValidationFails(LogDetail.URI)
                .get(setInvoicePaidUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.text", containsString("данные не найдены"))
                .extract().response().getBody().asString();
    }

    /**
     * Получение документа (файла)
     */
    @Test
    void getDocument() {
        int documentId = 151312186;
        String response = given().spec(spec)
                .param("document_id", documentId)
                .log().ifValidationFails(LogDetail.URI)
                .get(getDocumentUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.path", containsString(String.valueOf(documentId)),
                        "data.result.document_isn.toInteger()", equalTo(documentId))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(getDocumentUrl, response);
    }

    @Test
    void getDocumentFail() {
        int fakeDocumentId = 111;
        String response = given().spec(spec)
                .param("document_id", fakeDocumentId)
                .log().ifValidationFails(LogDetail.URI)
                .get(getDocumentUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.text", startsWith("Документ с заданным идентификатором"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(getDocumentUrl, response);
    }
}
