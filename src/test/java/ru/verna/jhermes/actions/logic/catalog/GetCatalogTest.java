package ru.verna.jhermes.actions.logic.catalog;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import ru.verna.commons.XmlUtils;

public class GetCatalogTest {

    @Test
    public void checkKiasErrorResponse() throws Exception {
        Document response = XmlUtils.parseToXml("<data>\n"
                + "    <error>\n"
                + "        <row>\n"
                + "            <attr_name>system</attr_name>\n"
                + "            <code/>\n"
                + "            <text>ORA-06508: PL/SQL: невозможно найти вызываемый блок программы</text>\n"
                + "        </row>\n"
                + "        <row>\n"
                + "            <attr_name>system</attr_name>\n"
                + "            <code/>\n"
                + "            <text>ORA-06508: PL/SQL: невозможно найти вызываемый блок программы</text>\n"
                + "        </row>\n"
                + "    </error>\n"
                + "</data>");
        Document document = new GetCatalog().transformKiasResponse(response);
        Assertions.assertEquals(XmlUtils.convertToString(response).replaceAll("\\s", ""), XmlUtils.convertToString(document).replaceAll("\\s", ""));
    }
}
