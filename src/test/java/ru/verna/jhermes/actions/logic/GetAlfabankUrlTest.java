package ru.verna.jhermes.actions.logic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class GetAlfabankUrlTest {

    private static GetAlfabankUrl getAlfabankUrl;

    @BeforeAll
    static void setUpClass() {
        getAlfabankUrl = new GetAlfabankUrl();
    }

    @Test
    void getCalcSumLongEqual() {
        double expected = 583.80;
        Assertions.assertEquals(58380, getAlfabankUrl.getCalcSumLong(expected));
    }
}
