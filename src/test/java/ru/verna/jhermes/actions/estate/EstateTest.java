package ru.verna.jhermes.actions.estate;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import ru.verna.CommonPart;
import java.time.LocalDate;

/**
 *
 */
public abstract class EstateTest {

    protected static RequestSpecification spec;

    static String startDate = CommonPart.formatLocalDate(LocalDate.now());
    static String endDate = CommonPart.formatLocalDate(LocalDate.now().plusMonths(2));
    String failStartDate = CommonPart.formatLocalDate(LocalDate.now().minusDays(1));

    static String calcId = "";

    @BeforeAll
    public static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder builder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_WWW);
        spec = builder.build();
    }
}
