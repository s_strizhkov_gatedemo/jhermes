package ru.verna.jhermes.actions.estate;

import com.jayway.jsonpath.JsonPath;
import io.restassured.filter.log.LogDetail;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;
import javax.servlet.http.HttpServletResponse;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

class HouseTest extends EstateTest {

    private static final String createFullCalcUrl = "products/estate/house/createCalc";
    private static final String karusselUrl = "products/estate/house/karussel";
    private static final String updateFullCalcUrl = "products/estate/house/updateCalc";

    @BeforeAll
    public static void setUpClass() {
        EstateTest.setUpClass();
        calcId = getCalcId();
    }

    @Test
    void karussel() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(karusselUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.product[0].isn", not(isEmptyOrNullString()),
                        "data.result.product[0].insured_sum", not(isEmptyOrNullString()),
                        "data.result.product[0].name", not(isEmptyOrNullString()),
                        "data.result.product.size()", is(5))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(karusselUrl, response);
    }

    @Test
    void karusselFail() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(karusselUrl + 1)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.text", startsWith("Действие products"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(karusselUrl, response);
    }

    @Test
    void getPaymentData() {
        String path = "getPaymentData";
        given().spec(spec)
                .param("calc_id", calcId)
                .log().ifValidationFails(LogDetail.URI)
                .get(path)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.product_name", containsString("Бизнес"))
                .body("data.result.calc_sum.toInteger()", isA(int.class))
                .extract().response().getBody().asString();
    }

    @Test
    void createFullCalc() {
        String params = "{\n"
                + "    'insuranceObject': {\n"
                + "        'address': {\n"
                + "            'city': {\n"
                + "                'code': '7700000000000',\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г'\n"
                + "            },\n"
                + "            'street': {\n"
                + "                'code': '77000000000287700',\n"
                + "                'custom_value': 'ул Тверская',\n"
                + "                'name': 'Тверская',\n"
                + "                'abbr': 'ул'\n"
                + "            },\n"
                + "            'postcode': '125009',\n"
                + "            'region': {\n"
                + "                'code': '7700000000000',\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г'\n"
                + "            },\n"
                + "            'house': 12\n"
                + "        }\n"
                + "    },\n"
                + "    'productId': 'ucProductMyApartment3600',\n"
                + "    'insurer': {\n"
                + "        'firstName': 'Гермес',\n"
                + "        'lastName': 'Гермесов',\n"
                + "        'gender': 'Ж',\n"
                + "        'isn': '',\n"
                + "        'phone': '+79381111111',\n"
                + "        'documents': {\n"
                + "            'document': {\n"
                + "                'date': '02.01.1998',\n"
                + "                'number': '111111',\n"
                + "                'series': '0715',\n"
                + "                'issuedBy': 'УФМС',\n"
                + "                'type': 'ucRFPassport'\n"
                + "            }\n"
                + "        },\n"
                + "        'address': {\n"
                + "            'region': {\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г',\n"
                + "                'code': '7700000000000'\n"
                + "            },\n"
                + "            'district': {\n"
                + "                'custom_value': ''\n"
                + "            },\n"
                + "            'city': {\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г',\n"
                + "                'code': '7700000000000'\n"
                + "            },\n"
                + "            'locality': {\n"
                + "                'custom_value': ''\n"
                + "            },\n"
                + "            'street': {\n"
                + "                'custom_value': 'ул Тверская',\n"
                + "                'name': 'Тверская',\n"
                + "                'abbr': 'ул',\n"
                + "                'code': '77000000000287700'\n"
                + "            },\n"
                + "            'house': '12',\n"
                + "            'building': '',\n"
                + "            'flat': '',\n"
                + "            'postcode': '125009'\n"
                + "        },\n"
                + "        'middleName': 'Гермесович',\n"
                + "        'birthDate': '01.01.1980',\n"
                + "        'email': 'verna@test.ru'\n"
                + "    },\n"
                + "    'clauses': {\n"
                + "        'clause': {\n"
                + "            'id': 'ucClausesCommonSalesChannel',\n"
                + "            'value': 'ucClausesCommonSalesChannel_Online'\n"
                + "        }\n"
                + "    },\n"
                + "    'term': {\n"
                + "        'end': '" + endDate + "',\n"
                + "        'begin': '" + startDate + "'\n"
                + "    },\n"
                + "    'attributes': {\n"
                + "        'attribute': [{\n"
                + "            'id': 'ucAddAtrNSCoveragePeriod',\n"
                + "            'value': '1329041'\n"
                + "        },\n"
                + "        {\n"
                + "            'id': 'ucPartnerId',\n"
                + "            'value': 'ucVernaTest'\n"
                + "        }\n"
                + "    ]},\n"
                + "    'remark': 'Эта котировка создана автотестами'\n"
                + "}";
        String response = given().spec(spec)
                .param("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .get(createFullCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.calcId", notNullValue())
                .body("data.result.insurer.isn.toInteger()", isA(int.class))
                .body("data.result.isn", notNullValue())
                .extract().response().getBody().asString();
        CommonPart.writeToDb(createFullCalcUrl, response);
    }

    @Test
    void createFullCalcFail() {
        String params = "{\n"
                + "    'insuranceObject': {\n"
                + "        'address': {\n"
                + "            'city': {\n"
                + "                'code': '7700000000000',\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г'\n"
                + "            },\n"
                + "            'street': {\n"
                + "                'code': '77000000000287700',\n"
                + "                'custom_value': 'ул Тверская',\n"
                + "                'name': 'Тверская',\n"
                + "                'abbr': 'ул'\n"
                + "            },\n"
                + "            'postcode': '125009',\n"
                + "            'region': {\n"
                + "                'code': '7700000000000',\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г'\n"
                + "            },\n"
                + "            'house': 12\n"
                + "        }\n"
                + "    },\n"
                + "    'productId': 'ucProductMyApartment3600',\n"
                + "    'insurer': {\n"
                + "        'firstName': 'Гермес',\n"
                + "        'lastName': 'Гермесов',\n"
                + "        'gender': 'Ж',\n"
                + "        'isn': '',\n"
                + "        'phone': '+79381111111',\n"
                + "        'documents': {\n"
                + "            'document': {\n"
                + "                'date': '02.01.1998',\n"
                + "                'number': '111111',\n"
                + "                'series': '0715',\n"
                + "                'issuedBy': 'УФМС',\n"
                + "                'type': 'ucRFPassport'\n"
                + "            }\n"
                + "        },\n"
                + "        'address': {\n"
                + "            'region': {\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г',\n"
                + "                'code': '7700000000000'\n"
                + "            },\n"
                + "            'district': {\n"
                + "                'custom_value': ''\n"
                + "            },\n"
                + "            'city': {\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г',\n"
                + "                'code': '7700000000000'\n"
                + "            },\n"
                + "            'locality': {\n"
                + "                'custom_value': ''\n"
                + "            },\n"
                + "            'street': {\n"
                + "                'custom_value': 'ул Тверская',\n"
                + "                'name': 'Тверская',\n"
                + "                'abbr': 'ул',\n"
                + "                'code': '77000000000287700'\n"
                + "            },\n"
                + "            'house': '12',\n"
                + "            'building': '',\n"
                + "            'flat': '',\n"
                + "            'postcode': '125009'\n"
                + "        },\n"
                + "        'middleName': 'Гермесович',\n"
                + "        'birthDate': '01.01.1980',\n"
                + "        'email': 'verna@test.ru'\n"
                + "    },\n"
                + "    'clauses': {\n"
                + "        'clause': {\n"
                + "            'id': 'ucClausesCommonSalesChannel',\n"
                + "            'value': 'ucClausesCommonSalesChannel_Online'\n"
                + "        }\n"
                + "    },\n"
                + "    'term': {\n"
                + "        'end': '" + endDate + "',\n"
                + "        'begin': '" + failStartDate + "'\n"
                + "    },\n"
                + "    'attributes': {\n"
                + "        'attribute': [{\n"
                + "            'id': 'ucAddAtrNSCoveragePeriod',\n"
                + "            'value': '1329041'\n"
                + "        },\n"
                + "        {\n"
                + "            'id': 'ucPartnerId',\n"
                + "            'value': 'ucVernaTest'\n"
                + "        }\n"
                + "    ]},\n"
                + "    'remark': 'Эта котировка создана автотестами'\n"
                + "}";
        String response = given().spec(spec)
                .param("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .get(createFullCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row[0].text", startsWith("Дата начала действия полиса"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(createFullCalcUrl, response);
    }

    @Test
    void updateFullCalc() {
        String params = "{\n"
                + "    'insuranceObject': {\n"
                + "        'address': {\n"
                + "            'city': {\n"
                + "                'code': '7700000000000',\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г'\n"
                + "            },\n"
                + "            'street': {\n"
                + "                'code': '77000000000287700',\n"
                + "                'custom_value': 'ул Тверская',\n"
                + "                'name': 'Тверская',\n"
                + "                'abbr': 'ул'\n"
                + "            },\n"
                + "            'postcode': '125009',\n"
                + "            'region': {\n"
                + "                'code': '7700000000000',\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г'\n"
                + "            },\n"
                + "            'house': 12\n"
                + "        }\n"
                + "    },\n"
                + "    'productId': 'ucProductMyApartment3600',\n"
                + "    'insurer': {\n"
                + "        'firstName': 'Гермес',\n"
                + "        'lastName': 'Гермесов',\n"
                + "        'gender': 'Ж',\n"
                + "        'isn': '" + CommonPart.getProperty("HouseTest.insurer.isn") + "',\n"
                + "        'phone': '+79381111111',\n"
                + "        'documents': {\n"
                + "            'document': {\n"
                + "                'date': '02.01.1998',\n"
                + "                'number': '111111',\n"
                + "                'series': '0715',\n"
                + "                'issuedBy': 'УФМС',\n"
                + "                'type': 'ucRFPassport'\n"
                + "            }\n"
                + "        },\n"
                + "        'address': {\n"
                + "            'region': {\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г',\n"
                + "                'code': '7700000000000'\n"
                + "            },\n"
                + "            'district': {\n"
                + "                'custom_value': ''\n"
                + "            },\n"
                + "            'city': {\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г',\n"
                + "                'code': '7700000000000'\n"
                + "            },\n"
                + "            'locality': {\n"
                + "                'custom_value': ''\n"
                + "            },\n"
                + "            'street': {\n"
                + "                'custom_value': 'ул Тверская',\n"
                + "                'name': 'Тверская',\n"
                + "                'abbr': 'ул',\n"
                + "                'code': '77000000000287700'\n"
                + "            },\n"
                + "            'house': '12',\n"
                + "            'building': '',\n"
                + "            'flat': '',\n"
                + "            'postcode': '125009'\n"
                + "        },\n"
                + "        'middleName': 'Гермесович',\n"
                + "        'birthDate': '01.01.1980',\n"
                + "        'email': 'verna@test.ru'\n"
                + "    },\n"
                + "    'clauses': {\n"
                + "        'clause': {\n"
                + "            'id': 'ucClausesCommonSalesChannel',\n"
                + "            'value': 'ucClausesCommonSalesChannel_Online'\n"
                + "        }\n"
                + "    },\n"
                + "    'term': {\n"
                + "        'end': '" + endDate + "',\n"
                + "        'begin': '" + startDate + "'\n"
                + "    },\n"
                + "    'attributes': {\n"
                + "        'attribute': [{\n"
                + "            'id': 'ucAddAtrNSCoveragePeriod',\n"
                + "            'value': '1329041'\n"
                + "        },\n"
                + "        {\n"
                + "            'id': 'ucPartnerId',\n"
                + "            'value': 'ucVernaTest'\n"
                + "        }\n"
                + "    ]},\n"
                + "    'remark': 'Эта котировка создана автотестами',\n"
                + "    'calcId': '" + calcId + "',\n"
                + "    'isn': '" + CommonPart.getProperty("HouseTest.isn") + "',\n"
                + "    'premium': '" + CommonPart.getProperty("HouseTest.premium") + "'\n"
                + "}";
        String response = given().spec(spec)
                .param("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .get(updateFullCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.calcId", notNullValue())
                .body("data.result.insurer.isn.toInteger()", isA(int.class))
                .body("data.result.isn", notNullValue())
                .extract().response().getBody().asString();
        CommonPart.writeToDb(updateFullCalcUrl, response);
    }

    @Test
    void updateFullCalcFail() {
        String params = "{\n"
                + "    'insuranceObject': {\n"
                + "        'address': {\n"
                + "            'city': {\n"
                + "                'code': '7700000000000',\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г'\n"
                + "            },\n"
                + "            'street': {\n"
                + "                'code': '77000000000287700',\n"
                + "                'custom_value': 'ул Тверская',\n"
                + "                'name': 'Тверская',\n"
                + "                'abbr': 'ул'\n"
                + "            },\n"
                + "            'postcode': '125009',\n"
                + "            'region': {\n"
                + "                'code': '7700000000000',\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г'\n"
                + "            },\n"
                + "            'house': 12\n"
                + "        }\n"
                + "    },\n"
                + "    'productId': 'ucProductMyApartment3600',\n"
                + "    'insurer': {\n"
                + "        'firstName': 'Гермес',\n"
                + "        'lastName': 'Гермесов',\n"
                + "        'gender': 'Ж',\n"
                + "        'isn': '" + CommonPart.getProperty("HouseTest.insurer.isn") + "',\n"
                + "        'phone': '+79381111111',\n"
                + "        'documents': {\n"
                + "            'document': {\n"
                + "                'date': '02.01.1998',\n"
                + "                'number': '111111',\n"
                + "                'series': '0715',\n"
                + "                'issuedBy': 'УФМС',\n"
                + "                'type': 'ucRFPassport'\n"
                + "            }\n"
                + "        },\n"
                + "        'address': {\n"
                + "            'region': {\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г',\n"
                + "                'code': '7700000000000'\n"
                + "            },\n"
                + "            'district': {\n"
                + "                'custom_value': ''\n"
                + "            },\n"
                + "            'city': {\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г',\n"
                + "                'code': '7700000000000'\n"
                + "            },\n"
                + "            'locality': {\n"
                + "                'custom_value': ''\n"
                + "            },\n"
                + "            'street': {\n"
                + "                'custom_value': 'ул Тверская',\n"
                + "                'name': 'Тверская',\n"
                + "                'abbr': 'ул',\n"
                + "                'code': '77000000000287700'\n"
                + "            },\n"
                + "            'house': '12',\n"
                + "            'building': '',\n"
                + "            'flat': '',\n"
                + "            'postcode': '125009'\n"
                + "        },\n"
                + "        'middleName': 'Гермесович',\n"
                + "        'birthDate': '01.01.1980',\n"
                + "        'email': 'verna@test.ru'\n"
                + "    },\n"
                + "    'clauses': {\n"
                + "        'clause': {\n"
                + "            'id': 'ucClausesCommonSalesChannel',\n"
                + "            'value': 'ucClausesCommonSalesChannel_Online'\n"
                + "        }\n"
                + "    },\n"
                + "    'term': {\n"
                + "        'end': '" + endDate + "',\n"
                + "        'begin': '" + failStartDate + "'\n"
                + "    },\n"
                + "    'attributes': {\n"
                + "        'attribute': [{\n"
                + "            'id': 'ucAddAtrNSCoveragePeriod',\n"
                + "            'value': '1329041'\n"
                + "        },\n"
                + "        {\n"
                + "            'id': 'ucPartnerId',\n"
                + "            'value': 'ucVernaTest'\n"
                + "        }\n"
                + "    ]},\n"
                + "    'remark': 'Эта котировка создана автотестами',\n"
                + "    'calcId': '" + calcId + "',\n"
                + "    'isn': '" + CommonPart.getProperty("HouseTest.isn") + "',\n"
                + "    'premium': '" + CommonPart.getProperty("HouseTest.premium") + "'\n"
                + "}";
        String response = given().spec(spec)
                .param("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .get(updateFullCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row[0].text", startsWith("Дата начала действия полиса"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(updateFullCalcUrl, response);
    }

    private static String getCalcId() {
        String params = "{\n"
                + "    'insuranceObject': {\n"
                + "        'address': {\n"
                + "            'city': {\n"
                + "                'code': '7700000000000',\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г'\n"
                + "            },\n"
                + "            'street': {\n"
                + "                'code': '77000000000287700',\n"
                + "                'custom_value': 'ул Тверская',\n"
                + "                'name': 'Тверская',\n"
                + "                'abbr': 'ул'\n"
                + "            },\n"
                + "            'postcode': '125009',\n"
                + "            'region': {\n"
                + "                'code': '7700000000000',\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г'\n"
                + "            },\n"
                + "            'house': 12\n"
                + "        }\n"
                + "    },\n"
                + "    'productId': 'ucProductMyApartment3600',\n"
                + "    'insurer': {\n"
                + "        'firstName': 'Гермес',\n"
                + "        'lastName': 'Гермесов',\n"
                + "        'gender': 'Ж',\n"
                + "        'isn': '',\n"
                + "        'phone': '+79381111111',\n"
                + "        'documents': {\n"
                + "            'document': {\n"
                + "                'date': '02.01.1998',\n"
                + "                'number': '111111',\n"
                + "                'series': '0715',\n"
                + "                'issuedBy': 'УФМС',\n"
                + "                'type': 'ucRFPassport'\n"
                + "            }\n"
                + "        },\n"
                + "        'address': {\n"
                + "            'region': {\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г',\n"
                + "                'code': '7700000000000'\n"
                + "            },\n"
                + "            'district': {\n"
                + "                'custom_value': ''\n"
                + "            },\n"
                + "            'city': {\n"
                + "                'custom_value': 'г Москва',\n"
                + "                'name': 'Москва',\n"
                + "                'abbr': 'г',\n"
                + "                'code': '7700000000000'\n"
                + "            },\n"
                + "            'locality': {\n"
                + "                'custom_value': ''\n"
                + "            },\n"
                + "            'street': {\n"
                + "                'custom_value': 'ул Тверская',\n"
                + "                'name': 'Тверская',\n"
                + "                'abbr': 'ул',\n"
                + "                'code': '77000000000287700'\n"
                + "            },\n"
                + "            'house': '12',\n"
                + "            'building': '',\n"
                + "            'flat': '',\n"
                + "            'postcode': '125009'\n"
                + "        },\n"
                + "        'middleName': 'Гермесович',\n"
                + "        'birthDate': '01.01.1980',\n"
                + "        'email': 'verna@test.ru'\n"
                + "    },\n"
                + "    'clauses': {\n"
                + "        'clause': {\n"
                + "            'id': 'ucClausesCommonSalesChannel',\n"
                + "            'value': 'ucClausesCommonSalesChannel_Online'\n"
                + "        }\n"
                + "    },\n"
                + "    'term': {\n"
                + "        'end': '" + endDate + "',\n"
                + "        'begin': '" + startDate + "'\n"
                + "    },\n"
                + "    'attributes': {\n"
                + "        'attribute': [{\n"
                + "            'id': 'ucAddAtrNSCoveragePeriod',\n"
                + "            'value': '1329041'\n"
                + "        },\n"
                + "        {\n"
                + "            'id': 'ucPartnerId',\n"
                + "            'value': 'ucVernaTest'\n"
                + "        }\n"
                + "    ]},\n"
                + "    'remark': 'Эта котировка создана автотестами'\n"
                + "}";
        String response = given().spec(spec)
                .param("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .get(createFullCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.calcId", notNullValue())
                .body("data.result.insurer.isn.toInteger()", isA(int.class))
                .body("data.result.isn", notNullValue())
                .extract().response().getBody().asString();
        return JsonPath.read(response, "data.result.calcId");
    }
}
