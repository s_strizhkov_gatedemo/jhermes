package ru.verna.jhermes.actions.estate;

import com.jayway.jsonpath.JsonPath;
import io.restassured.filter.log.LogDetail;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;
import javax.servlet.http.HttpServletResponse;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

class LiabilityTest extends EstateTest {

    private static final String createFullCalcUrl = "products/liability/createCalc";
    private static final String karusselUrl = "products/liability/karussel";
    private static final String updateFullCalcUrl = "products/liability/updateCalc";

    @BeforeAll
    public static void setUpClass() {
        EstateTest.setUpClass();
        calcId = getCalcId();
    }

    @Test
    void karussel() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(karusselUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.product[0].isn", not(isEmptyOrNullString()),
                        "data.result.product[0].insured_sum", not(isEmptyOrNullString()),
                        "data.result.product[0].name", not(isEmptyOrNullString()),
                        "data.result.product.size()", is(5))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(karusselUrl, response);
    }

    @Test
    void karusselFail() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(karusselUrl + 1)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.text", startsWith("Действие products"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(karusselUrl, response);
    }

    @Test
    void getPaymentData() {
        String path = "getPaymentData";
        given().spec(spec)
                .param("calc_id", calcId)
                .log().ifValidationFails(LogDetail.URI)
                .get(path)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.product_name", containsString("Бизнес"))
                .body("data.result.calc_sum.toInteger()", isA(int.class))
                .extract().response().getBody().asString();
    }

    @Test
    void createFullCalc() {
        String params = "{\n" +
                "    'insurant': {\n" +
                "        'firstName': 'Гермес',\n" +
                "        'lastName': 'Гермесов',\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7300000100000,\n" +
                "                'custom_value': 'г Ульяновск',\n" +
                "                'name': 'Ульяновск',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 1,\n" +
                "            'street': {\n" +
                "                'code': 73000001000054000,\n" +
                "                'custom_value': 'ул Рабочая',\n" +
                "                'name': 'Рабочая',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 432007,\n" +
                "            'region': {\n" +
                "                'code': 7300000000000,\n" +
                "                'custom_value': 'Ульяновская обл',\n" +
                "                'name': 'Ульяновская',\n" +
                "                'abbr': 'обл'\n" +
                "            },\n" +
                "            'house': 1,\n" +
                "            'building': 1\n" +
                "        },\n" +
                "        'gender': 'М',\n" +
                "        'documents': {\n" +
                "            'document': {\n" +
                "                'date': '01.07.2018',\n" +
                "                'number': 111111,\n" +
                "                'series': '0715',\n" +
                "                'issuedBy': 'УФМС',\n" +
                "                'type': 'ucRFPassport'\n" +
                "            }\n" +
                "        },\n" +
                "        'isn': '',\n" +
                "        'middleName': 'Гермесович',\n" +
                "        'birthDate': '15.07.1993'\n" +
                "    },\n" +
                "    'premium': '',\n" +
                "    'insuranceObject': {\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7300000100000,\n" +
                "                'custom_value': 'г Ульяновск',\n" +
                "                'name': 'Ульяновск',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 1,\n" +
                "            'street': {\n" +
                "                'code': 73000001000054000,\n" +
                "                'custom_value': 'ул Рабочая',\n" +
                "                'name': 'Рабочая',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 432007,\n" +
                "            'region': {\n" +
                "                'code': 7300000000000,\n" +
                "                'custom_value': 'Ульяновская обл',\n" +
                "                'name': 'Ульяновская',\n" +
                "                'abbr': 'обл'\n" +
                "            },\n" +
                "            'house': 1,\n" +
                "            'building': 1\n" +
                "        }\n" +
                "    },\n" +
                "    'productId': 'ucProductMyLiability3000',\n" +
                "    'isn': '',\n" +
                "    'insurer': {\n" +
                "        'firstName': 'Гермес',\n" +
                "        'lastName': 'Гермесов',\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7300000100000,\n" +
                "                'custom_value': 'г Ульяновск',\n" +
                "                'name': 'Ульяновск',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 1,\n" +
                "            'street': {\n" +
                "                'code': 73000001000054000,\n" +
                "                'custom_value': 'ул Рабочая',\n" +
                "                'name': 'Рабочая',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 432007,\n" +
                "            'region': {\n" +
                "                'code': 7300000000000,\n" +
                "                'custom_value': 'Ульяновская обл',\n" +
                "                'name': 'Ульяновская',\n" +
                "                'abbr': 'обл'\n" +
                "            },\n" +
                "            'house': 1,\n" +
                "            'building': 1\n" +
                "        },\n" +
                "        'gender': 'М',\n" +
                "        'documents': {\n" +
                "            'document': {\n" +
                "                'date': '01.07.2018',\n" +
                "                'number': 111111,\n" +
                "                'series': '0715',\n" +
                "                'issuedBy': 'УФМС',\n" +
                "                'type': 'ucRFPassport'\n" +
                "            }\n" +
                "        },\n" +
                "        'isn': '',\n" +
                "        'phone': '+79384147912',\n" +
                "        'middleName': 'Гермесович',\n" +
                "        'birthDate': '15.07.1993',\n" +
                "        'email': 'germesov@gmail.com'\n" +
                "    },\n" +
                "    'clauses': {\n" +
                "        'clause': {\n" +
                "            'id': 'ucClausesCommonSalesChannel',\n" +
                "            'value': 'ucClausesCommonSalesChannel_Online'\n" +
                "        }\n" +
                "    },\n" +
                "    'calcId': '',\n" +
                "    'promocode': '',\n" +
                "    'term': {\n" +
                "        'end': '" + endDate + "',\n" +
                "        'begin': '" + startDate + "'\n" +
                "    },\n" +
                "    'attributes': {\n" +
                "        'attribute': [{\n" +
                "            'id': 'ucAddAtrNSCoveragePeriod',\n" +
                "            'value': '1329041'\n" +
                "        },\n" +
                "        {\n" +
                "            'id': 'ucPartnerId',\n" +
                "            'value': 'ucVernaTest'\n" +
                "        }\n" +
                "    ]},\n" +
                "    'remark': 'Эта котировка создана автотестами'\n" +
                "}";
        String response = given().spec(spec)
                .header("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
                .formParam("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .post(createFullCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.calcId", notNullValue())
                .body("data.result.insurer.isn.toInteger()", isA(int.class))
                .body("data.result.isn", notNullValue())
                .extract().response().getBody().asString();
        CommonPart.writeToDb(createFullCalcUrl, response);
    }

    @Test
    void createFullCalcFail() {
        String params = "{\n" +
                "    'insurant': {\n" +
                "        'firstName': 'Гермес',\n" +
                "        'lastName': 'Гермесов',\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7300000100000,\n" +
                "                'custom_value': 'г Ульяновск',\n" +
                "                'name': 'Ульяновск',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 1,\n" +
                "            'street': {\n" +
                "                'code': 73000001000054000,\n" +
                "                'custom_value': 'ул Рабочая',\n" +
                "                'name': 'Рабочая',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 432007,\n" +
                "            'region': {\n" +
                "                'code': 7300000000000,\n" +
                "                'custom_value': 'Ульяновская обл',\n" +
                "                'name': 'Ульяновская',\n" +
                "                'abbr': 'обл'\n" +
                "            },\n" +
                "            'house': 1,\n" +
                "            'building': 1\n" +
                "        },\n" +
                "        'gender': 'М',\n" +
                "        'documents': {\n" +
                "            'document': {\n" +
                "                'date': '01.07.2018',\n" +
                "                'number': 111111,\n" +
                "                'series': '0715',\n" +
                "                'issuedBy': 'УФМС',\n" +
                "                'type': 'ucRFPassport'\n" +
                "            }\n" +
                "        },\n" +
                "        'isn': '',\n" +
                "        'middleName': 'Гермесович',\n" +
                "        'birthDate': '15.07.1993'\n" +
                "    },\n" +
                "    'premium': '',\n" +
                "    'insuranceObject': {\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7300000100000,\n" +
                "                'custom_value': 'г Ульяновск',\n" +
                "                'name': 'Ульяновск',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 1,\n" +
                "            'street': {\n" +
                "                'code': 73000001000054000,\n" +
                "                'custom_value': 'ул Рабочая',\n" +
                "                'name': 'Рабочая',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 432007,\n" +
                "            'region': {\n" +
                "                'code': 7300000000000,\n" +
                "                'custom_value': 'Ульяновская обл',\n" +
                "                'name': 'Ульяновская',\n" +
                "                'abbr': 'обл'\n" +
                "            },\n" +
                "            'house': 1,\n" +
                "            'building': 1\n" +
                "        }\n" +
                "    },\n" +
                "    'productId': 'ucProductMyLiability3000',\n" +
                "    'isn': '',\n" +
                "    'insurer': {\n" +
                "        'firstName': 'Гермес',\n" +
                "        'lastName': 'Гермесов',\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7300000100000,\n" +
                "                'custom_value': 'г Ульяновск',\n" +
                "                'name': 'Ульяновск',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 1,\n" +
                "            'street': {\n" +
                "                'code': 73000001000054000,\n" +
                "                'custom_value': 'ул Рабочая',\n" +
                "                'name': 'Рабочая',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 432007,\n" +
                "            'region': {\n" +
                "                'code': 7300000000000,\n" +
                "                'custom_value': 'Ульяновская обл',\n" +
                "                'name': 'Ульяновская',\n" +
                "                'abbr': 'обл'\n" +
                "            },\n" +
                "            'house': 1,\n" +
                "            'building': 1\n" +
                "        },\n" +
                "        'gender': 'М',\n" +
                "        'documents': {\n" +
                "            'document': {\n" +
                "                'date': '01.07.2018',\n" +
                "                'number': 111111,\n" +
                "                'series': '0715',\n" +
                "                'issuedBy': 'УФМС',\n" +
                "                'type': 'ucRFPassport'\n" +
                "            }\n" +
                "        },\n" +
                "        'isn': '',\n" +
                "        'phone': '+79384147912',\n" +
                "        'middleName': 'Гермесович',\n" +
                "        'birthDate': '15.07.1993',\n" +
                "        'email': 'germesov@gmail.com'\n" +
                "    },\n" +
                "    'clauses': {\n" +
                "        'clause': {\n" +
                "            'id': 'ucClausesCommonSalesChannel',\n" +
                "            'value': 'ucClausesCommonSalesChannel_Online'\n" +
                "        }\n" +
                "    },\n" +
                "    'calcId': '',\n" +
                "    'promocode': '',\n" +
                "    'term': {\n" +
                "        'end': '" + endDate + "',\n" +
                "        'begin': '" + failStartDate + "'\n" +
                "    },\n" +
                "    'attributes': {\n" +
                "        'attribute': [{\n" +
                "            'id': 'ucAddAtrNSCoveragePeriod',\n" +
                "            'value': '1329041'\n" +
                "        },\n" +
                "        {\n" +
                "            'id': 'ucPartnerId',\n" +
                "            'value': 'ucVernaTest'\n" +
                "        }\n" +
                "    ]},\n" +
                "    'remark': 'Эта котировка создана автотестами'\n" +
                "}";
        String response = given().spec(spec)
                .header("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
                .formParam("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .post(createFullCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row[0].text", startsWith("Дата начала действия полиса"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(createFullCalcUrl, response);
    }

    @Test
    void updateFullCalc() {
        String params = "{\n" +
                "    'insurant': {\n" +
                "        'firstName': 'Гермес',\n" +
                "        'lastName': 'Гермесов',\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7300000100000,\n" +
                "                'custom_value': 'г Ульяновск',\n" +
                "                'name': 'Ульяновск',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 1,\n" +
                "            'street': {\n" +
                "                'code': 73000001000054000,\n" +
                "                'custom_value': 'ул Рабочая',\n" +
                "                'name': 'Рабочая',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 432007,\n" +
                "            'region': {\n" +
                "                'code': 7300000000000,\n" +
                "                'custom_value': 'Ульяновская обл',\n" +
                "                'name': 'Ульяновская',\n" +
                "                'abbr': 'обл'\n" +
                "            },\n" +
                "            'house': 1,\n" +
                "            'building': 1\n" +
                "        },\n" +
                "        'gender': 'М',\n" +
                "        'documents': {\n" +
                "            'document': {\n" +
                "                'date': '01.07.2018',\n" +
                "                'number': 111111,\n" +
                "                'series': '0715',\n" +
                "                'issuedBy': 'УФМС',\n" +
                "                'type': 'ucRFPassport'\n" +
                "            }\n" +
                "        },\n" +
                "        'isn': '" + CommonPart.getProperty("LiabilityTest.insurant.isn") + "',\n" +
                "        'middleName': 'Гермесович',\n" +
                "        'birthDate': '15.07.1993'\n" +
                "    },\n" +
                "    'premium': '',\n" +
                "    'insuranceObject': {\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7300000100000,\n" +
                "                'custom_value': 'г Ульяновск',\n" +
                "                'name': 'Ульяновск',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 1,\n" +
                "            'street': {\n" +
                "                'code': 73000001000054000,\n" +
                "                'custom_value': 'ул Рабочая',\n" +
                "                'name': 'Рабочая',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 432007,\n" +
                "            'region': {\n" +
                "                'code': 7300000000000,\n" +
                "                'custom_value': 'Ульяновская обл',\n" +
                "                'name': 'Ульяновская',\n" +
                "                'abbr': 'обл'\n" +
                "            },\n" +
                "            'house': 1,\n" +
                "            'building': 1\n" +
                "        }\n" +
                "    },\n" +
                "    'productId': 'ucProductMyLiability3000',\n" +
                "    'isn': '" + CommonPart.getProperty("LiabilityTest.isn") + "',\n" +
                "    'insurer': {\n" +
                "        'firstName': 'Гермес',\n" +
                "        'lastName': 'Гермесов',\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7300000100000,\n" +
                "                'custom_value': 'г Ульяновск',\n" +
                "                'name': 'Ульяновск',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 1,\n" +
                "            'street': {\n" +
                "                'code': 73000001000054000,\n" +
                "                'custom_value': 'ул Рабочая',\n" +
                "                'name': 'Рабочая',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 432007,\n" +
                "            'region': {\n" +
                "                'code': 7300000000000,\n" +
                "                'custom_value': 'Ульяновская обл',\n" +
                "                'name': 'Ульяновская',\n" +
                "                'abbr': 'обл'\n" +
                "            },\n" +
                "            'house': 1,\n" +
                "            'building': 1\n" +
                "        },\n" +
                "        'gender': 'М',\n" +
                "        'documents': {\n" +
                "            'document': {\n" +
                "                'date': '01.07.2018',\n" +
                "                'number': 111111,\n" +
                "                'series': '0715',\n" +
                "                'issuedBy': 'УФМС',\n" +
                "                'type': 'ucRFPassport'\n" +
                "            }\n" +
                "        },\n" +
                "        'isn': '" + CommonPart.getProperty("LiabilityTest.insurer.isn") + "',\n" +
                "        'phone': '+79384147912',\n" +
                "        'middleName': 'Гермесович',\n" +
                "        'birthDate': '15.07.1993',\n" +
                "        'email': 'germesov@gmail.com'\n" +
                "    },\n" +
                "    'clauses': {\n" +
                "        'clause': {\n" +
                "            'id': 'ucClausesCommonSalesChannel',\n" +
                "            'value': 'ucClausesCommonSalesChannel_Online'\n" +
                "        }\n" +
                "    },\n" +
                "    'calcId': '" + calcId + "',\n" +
                "    'promocode': '',\n" +
                "    'term': {\n" +
                "        'end': '" + endDate + "',\n" +
                "        'begin': '" + startDate + "'\n" +
                "    },\n" +
                "    'attributes': {\n" +
                "        'attribute': [{\n" +
                "            'id': 'ucAddAtrNSCoveragePeriod',\n" +
                "            'value': '1329041'\n" +
                "        },\n" +
                "        {\n" +
                "            'id': 'ucPartnerId',\n" +
                "            'value': 'ucVernaTest'\n" +
                "        }\n" +
                "    ]},\n" +
                "    'remark': 'Эта котировка создана автотестами'\n" +
                "}";
        String response = given().spec(spec)
                .header("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
                .formParam("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .post(updateFullCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.calcId", notNullValue())
                .body("data.result.insurer.isn.toInteger()", isA(int.class))
                .body("data.result.isn", notNullValue())
                .extract().response().getBody().asString();
        CommonPart.writeToDb(updateFullCalcUrl, response);
    }

    @Test
    void updateFullCalcFail() {
        String params = "{\n" +
                "    'insurant': {\n" +
                "        'firstName': 'Гермес',\n" +
                "        'lastName': 'Гермесов',\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7300000100000,\n" +
                "                'custom_value': 'г Ульяновск',\n" +
                "                'name': 'Ульяновск',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 1,\n" +
                "            'street': {\n" +
                "                'code': 73000001000054000,\n" +
                "                'custom_value': 'ул Рабочая',\n" +
                "                'name': 'Рабочая',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 432007,\n" +
                "            'region': {\n" +
                "                'code': 7300000000000,\n" +
                "                'custom_value': 'Ульяновская обл',\n" +
                "                'name': 'Ульяновская',\n" +
                "                'abbr': 'обл'\n" +
                "            },\n" +
                "            'house': 1,\n" +
                "            'building': 1\n" +
                "        },\n" +
                "        'gender': 'М',\n" +
                "        'documents': {\n" +
                "            'document': {\n" +
                "                'date': '01.07.2018',\n" +
                "                'number': 111111,\n" +
                "                'series': '0715',\n" +
                "                'issuedBy': 'УФМС',\n" +
                "                'type': 'ucRFPassport'\n" +
                "            }\n" +
                "        },\n" +
                "        'isn': '" + CommonPart.getProperty("LiabilityTest.insurant.isn") + "',\n" +
                "        'middleName': 'Гермесович',\n" +
                "        'birthDate': '15.07.1993'\n" +
                "    },\n" +
                "    'premium': '',\n" +
                "    'insuranceObject': {\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7300000100000,\n" +
                "                'custom_value': 'г Ульяновск',\n" +
                "                'name': 'Ульяновск',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 1,\n" +
                "            'street': {\n" +
                "                'code': 73000001000054000,\n" +
                "                'custom_value': 'ул Рабочая',\n" +
                "                'name': 'Рабочая',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 432007,\n" +
                "            'region': {\n" +
                "                'code': 7300000000000,\n" +
                "                'custom_value': 'Ульяновская обл',\n" +
                "                'name': 'Ульяновская',\n" +
                "                'abbr': 'обл'\n" +
                "            },\n" +
                "            'house': 1,\n" +
                "            'building': 1\n" +
                "        }\n" +
                "    },\n" +
                "    'productId': 'ucProductMyLiability3000',\n" +
                "    'isn': '" + CommonPart.getProperty("LiabilityTest.isn") + "',\n" +
                "    'insurer': {\n" +
                "        'firstName': 'Гермес',\n" +
                "        'lastName': 'Гермесов',\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7300000100000,\n" +
                "                'custom_value': 'г Ульяновск',\n" +
                "                'name': 'Ульяновск',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 1,\n" +
                "            'street': {\n" +
                "                'code': 73000001000054000,\n" +
                "                'custom_value': 'ул Рабочая',\n" +
                "                'name': 'Рабочая',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 432007,\n" +
                "            'region': {\n" +
                "                'code': 7300000000000,\n" +
                "                'custom_value': 'Ульяновская обл',\n" +
                "                'name': 'Ульяновская',\n" +
                "                'abbr': 'обл'\n" +
                "            },\n" +
                "            'house': 1,\n" +
                "            'building': 1\n" +
                "        },\n" +
                "        'gender': 'М',\n" +
                "        'documents': {\n" +
                "            'document': {\n" +
                "                'date': '01.07.2018',\n" +
                "                'number': 111111,\n" +
                "                'series': '0715',\n" +
                "                'issuedBy': 'УФМС',\n" +
                "                'type': 'ucRFPassport'\n" +
                "            }\n" +
                "        },\n" +
                "        'isn': '" + CommonPart.getProperty("LiabilityTest.insurer.isn") + "',\n" +
                "        'phone': '+79384147912',\n" +
                "        'middleName': 'Гермесович',\n" +
                "        'birthDate': '15.07.1993',\n" +
                "        'email': 'germesov@gmail.com'\n" +
                "    },\n" +
                "    'clauses': {\n" +
                "        'clause': {\n" +
                "            'id': 'ucClausesCommonSalesChannel',\n" +
                "            'value': 'ucClausesCommonSalesChannel_Online'\n" +
                "        }\n" +
                "    },\n" +
                "    'calcId': '" + calcId + "',\n" +
                "    'promocode': '',\n" +
                "    'term': {\n" +
                "        'end': '" + endDate + "',\n" +
                "        'begin': '" + failStartDate + "'\n" +
                "    },\n" +
                "    'attributes': {\n" +
                "        'attribute': [{\n" +
                "            'id': 'ucAddAtrNSCoveragePeriod',\n" +
                "            'value': '1329041'\n" +
                "        },\n" +
                "        {\n" +
                "            'id': 'ucPartnerId',\n" +
                "            'value': 'ucVernaTest'\n" +
                "        }\n" +
                "    ]},\n" +
                "    'remark': 'Эта котировка создана автотестами'\n" +
                "}";
        String response = given().spec(spec)
                .header("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
                .formParam("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .post(updateFullCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row[0].text", startsWith("Дата начала действия полиса"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(updateFullCalcUrl, response);
    }

    private static String getCalcId() {
        String params = "{\n" +
                "    'insurant': {\n" +
                "        'firstName': 'Гермес',\n" +
                "        'lastName': 'Гермесов',\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7300000100000,\n" +
                "                'custom_value': 'г Ульяновск',\n" +
                "                'name': 'Ульяновск',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 1,\n" +
                "            'street': {\n" +
                "                'code': 73000001000054000,\n" +
                "                'custom_value': 'ул Рабочая',\n" +
                "                'name': 'Рабочая',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 432007,\n" +
                "            'region': {\n" +
                "                'code': 7300000000000,\n" +
                "                'custom_value': 'Ульяновская обл',\n" +
                "                'name': 'Ульяновская',\n" +
                "                'abbr': 'обл'\n" +
                "            },\n" +
                "            'house': 1,\n" +
                "            'building': 1\n" +
                "        },\n" +
                "        'gender': 'М',\n" +
                "        'documents': {\n" +
                "            'document': {\n" +
                "                'date': '01.07.2018',\n" +
                "                'number': 111111,\n" +
                "                'series': '0715',\n" +
                "                'issuedBy': 'УФМС',\n" +
                "                'type': 'ucRFPassport'\n" +
                "            }\n" +
                "        },\n" +
                "        'isn': '',\n" +
                "        'middleName': 'Гермесович',\n" +
                "        'birthDate': '15.07.1993'\n" +
                "    },\n" +
                "    'premium': '',\n" +
                "    'insuranceObject': {\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7300000100000,\n" +
                "                'custom_value': 'г Ульяновск',\n" +
                "                'name': 'Ульяновск',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 1,\n" +
                "            'street': {\n" +
                "                'code': 73000001000054000,\n" +
                "                'custom_value': 'ул Рабочая',\n" +
                "                'name': 'Рабочая',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 432007,\n" +
                "            'region': {\n" +
                "                'code': 7300000000000,\n" +
                "                'custom_value': 'Ульяновская обл',\n" +
                "                'name': 'Ульяновская',\n" +
                "                'abbr': 'обл'\n" +
                "            },\n" +
                "            'house': 1,\n" +
                "            'building': 1\n" +
                "        }\n" +
                "    },\n" +
                "    'productId': 'ucProductMyLiability3000',\n" +
                "    'isn': '',\n" +
                "    'insurer': {\n" +
                "        'firstName': 'Гермес',\n" +
                "        'lastName': 'Гермесов',\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7300000100000,\n" +
                "                'custom_value': 'г Ульяновск',\n" +
                "                'name': 'Ульяновск',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 1,\n" +
                "            'street': {\n" +
                "                'code': 73000001000054000,\n" +
                "                'custom_value': 'ул Рабочая',\n" +
                "                'name': 'Рабочая',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 432007,\n" +
                "            'region': {\n" +
                "                'code': 7300000000000,\n" +
                "                'custom_value': 'Ульяновская обл',\n" +
                "                'name': 'Ульяновская',\n" +
                "                'abbr': 'обл'\n" +
                "            },\n" +
                "            'house': 1,\n" +
                "            'building': 1\n" +
                "        },\n" +
                "        'gender': 'М',\n" +
                "        'documents': {\n" +
                "            'document': {\n" +
                "                'date': '01.07.2018',\n" +
                "                'number': 111111,\n" +
                "                'series': '0715',\n" +
                "                'issuedBy': 'УФМС',\n" +
                "                'type': 'ucRFPassport'\n" +
                "            }\n" +
                "        },\n" +
                "        'isn': '',\n" +
                "        'phone': '+79384147912',\n" +
                "        'middleName': 'Гермесович',\n" +
                "        'birthDate': '15.07.1993',\n" +
                "        'email': 'germesov@gmail.com'\n" +
                "    },\n" +
                "    'clauses': {\n" +
                "        'clause': {\n" +
                "            'id': 'ucClausesCommonSalesChannel',\n" +
                "            'value': 'ucClausesCommonSalesChannel_Online'\n" +
                "        }\n" +
                "    },\n" +
                "    'calcId': '',\n" +
                "    'promocode': '',\n" +
                "    'term': {\n" +
                "        'end': '" + endDate + "',\n" +
                "        'begin': '" + startDate + "'\n" +
                "    },\n" +
                "    'attributes': {\n" +
                "        'attribute': [{\n" +
                "            'id': 'ucAddAtrNSCoveragePeriod',\n" +
                "            'value': '1329041'\n" +
                "        },\n" +
                "        {\n" +
                "            'id': 'ucPartnerId',\n" +
                "            'value': 'ucVernaTest'\n" +
                "        }\n" +
                "    ]},\n" +
                "    'remark': 'Эта котировка создана автотестами'\n" +
                "}";
        String response = given().spec(spec)
                .header("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
                .formParam("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .post(createFullCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.calcId", notNullValue())
                .body("data.result.insurer.isn.toInteger()", isA(int.class))
                .body("data.result.isn", notNullValue())
                .extract().response().getBody().asString();
        return JsonPath.read(response, "data.result.calcId");
    }
}
