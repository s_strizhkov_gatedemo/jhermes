package ru.verna.jhermes.actions;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;
import javax.servlet.http.HttpServletResponse;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

class GetPrintFormTest {

    private static final String url = "getPrintForm";
    private static RequestSpecification spec;

    @BeforeAll
    static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder builder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_CABINET_USER);
        spec = builder.build();
    }

    @Test
    void getPrintForm() {
        given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .param("ISN", "102446184")
                .param("TemplateISN", 2003)
                .param("ClassID", 3)
                .get(url)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("Document.FileName", startsWith("C:\\inetpub"));
    }

    @Test
    void getPrintFormFile() {
        given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .param("isn2", "102446184")
                .param("TemplateISN", 2003)
                .get(url)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.size()", equalTo(2));
    }

    @Test
    void getPrintFormKiasFile() {
        given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .param("ISN", "00000")
                .param("TemplateISN", 2003)
                .param("ClassID", 3)
                .get(url)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.text", containsString("Анкета не заполнена данными Контур-Фокуса"));
    }
}
