package ru.verna.jhermes.actions;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import ru.verna.CommonPart;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

/**
 * Обращение по адресу https://server/gate//////action: несколько слэшей перед именем действия равнозначны одному слэшу.
 */
class RemoveSlashesTest {
    private static final String constantName = "constantName";
    private static final String constantValue = "ucDictiNSCoverageTerritory";
    private static final String includeArchived = "includeArchived";
    private static final String booleanTrue = "true";
    private static final String catalogList = "catalog/list";
    private static RequestSpecification spec;
    private static String url;

    @BeforeAll
    static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder builder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_WWW);
        spec = builder.build();

        // полный url, на конце нет слэша
        url = RestAssured.baseURI + ":" + RestAssured.port + RestAssured.basePath;
    }

    /**
     * Обращение по адресу https://server/gate/action: нормальное обращение.
     */
    @Test
    void oneSlash() {
        String fullUrl = url + "/" + catalogList;
        String response = given().spec(spec)
                .param(constantName, constantValue)
                .param(includeArchived, booleanTrue)
                .log().ifValidationFails(LogDetail.URI)
                .urlEncodingEnabled(false)
                .get(fullUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", is(2))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogList, response);
    }

    /**
     * Обращение по адресу https://server/gate//action: два слэша перед именем действия равнозначны одному слэшу.
     */
    @Test
    void twoSlashes() {
        String fullUrl = url + "//" + catalogList;
        String response = given().spec(spec)
                .param(constantName, constantValue)
                .param(includeArchived, booleanTrue)
                .log().ifValidationFails(LogDetail.URI)
                .urlEncodingEnabled(false)
                .get(fullUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", is(2))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogList, response);
    }

    /**
     * Обращение по адресу https://server/gate///action: три слэша перед именем действия равнозначны одному слэшу.
     */
    @Test
    void threeSlashes() {
        String fullUrl = url + "///" + catalogList;
        String response = given().spec(spec)
                .param(constantName, constantValue)
                .param(includeArchived, booleanTrue)
                .log().ifValidationFails(LogDetail.URI)
                .urlEncodingEnabled(false)
                .get(fullUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", is(2))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogList, response);
    }

    /**
     * Обращение по адресу https://server/gate//////action: несколько слэшей перед именем действия равнозначны одному слэшу.
     */
    @Test
    void manySlashes() {
        String fullUrl = url + "//////" + catalogList;
        String response = given().spec(spec)
                .param(constantName, constantValue)
                .param(includeArchived, booleanTrue)
                .log().ifValidationFails(LogDetail.URI)
                .urlEncodingEnabled(false)
                .get(fullUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", is(2))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogList, response);
    }

}
