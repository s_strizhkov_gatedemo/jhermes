package ru.verna.jhermes.actions;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;
import javax.servlet.http.HttpServletResponse;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

class AgreementTest {

    private static final String createAgreementUrl = "createAgreement";
    private static final String cancellationAgreementUrl = "agreementCancellation";
    private static final String getCalcInfoExtendedUrl = "getCalcInfoExtended";
    /**
     * Запускаемая в тестах котировка
     */
    private static String calcId = "";
    /**
     * Запускаемая в тестах котировка
     */
    private static RequestSpecification nsSpec;
    private static RequestSpecification testSpec;
    private static RequestSpecification publicSpec;
    private static RequestSpecification cabinetSpec;
    private static RequestSpecification medinsSpec;

    @BeforeAll
    static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder testBuilder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_TEST_USER);
        testSpec = testBuilder.build();
        RequestSpecBuilder nsBuilder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_NS);
        nsSpec = nsBuilder.build();
        RequestSpecBuilder publicBuilder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_PUBLIC);
        publicSpec = publicBuilder.build();
        RequestSpecBuilder cabinetBuilder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_CABINET_USER);
        cabinetSpec = cabinetBuilder.build();
        RequestSpecBuilder medinsBuilder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_MEDINS_USER);
        medinsSpec = medinsBuilder.build();
        calcId = CommonPart.getCalcId();
    }

    /**
     * После каждого теста привентивно аннулируем созданный договор.
     */
    @AfterEach
    void cleanUp() {
        given().spec(testSpec)
                .param("calc_id", calcId)
                .log().ifValidationFails(LogDetail.URI)
                .get(cancellationAgreementUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.status", equalTo("Ok"));
    }

    /**
     * Создание договора, проверка статуса - Выпущен, аннулировние по calc_id, проверка статуса - Аннулирован.
     */
    @Test
    void createCheckAndCancelAgreementByCalcId() {
        given().spec(nsSpec)
                .param("calc_id", calcId)
                .log().ifValidationFails(LogDetail.URI)
                .get(createAgreementUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.policy_id", equalTo(calcId));

        given().spec(publicSpec)
                .param("calc_id", calcId)
                .log().ifValidationFails(LogDetail.URI)
                .get(getCalcInfoExtendedUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.product.product_isn", equalTo("1280351"),
                        "data.result.calc.calc_id", equalTo(calcId),
                        "data.result.policy.status", equalTo("Выпущен"),
                        "data.result.policy.policy_id", equalTo(calcId));

        given().spec(testSpec)
                .param("calc_id", calcId)
                .log().ifValidationFails(LogDetail.URI)
                .get(cancellationAgreementUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.status", equalTo("Ok"));

        given().spec(publicSpec)
                .param("calc_id", calcId)
                .log().ifValidationFails(LogDetail.URI)
                .get(getCalcInfoExtendedUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.product.product_isn", equalTo("1280351"),
                        "data.result.calc.calc_id", equalTo(calcId),
                        "data.result.policy.status", equalTo("Аннулирован"),
                        "data.result.policy.policy_id", equalTo(calcId));
    }

    /**
     * Создание договора, проверка статуса - Выпущен, аннулировние по agreement_id, проверка статуса - Аннулирован.
     */
    @Test
    void createCheckAndCancelAgreementByAgreementId() {
        given().spec(nsSpec)
                .param("calc_id", calcId)
                .log().ifValidationFails(LogDetail.URI)
                .get(createAgreementUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.policy_id", equalTo(calcId));

        given().spec(publicSpec)
                .param("calc_id", calcId)
                .log().ifValidationFails(LogDetail.URI)
                .get(getCalcInfoExtendedUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.product.product_isn", equalTo("1280351"),
                        "data.result.calc.calc_id", equalTo(calcId),
                        "data.result.policy.status", equalTo("Выпущен"),
                        "data.result.policy.policy_id", equalTo(calcId));

        given().spec(testSpec)
                .param("agreement_id", calcId)
                .log().ifValidationFails(LogDetail.URI)
                .get(cancellationAgreementUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.status", equalTo("Ok"));

        given().spec(publicSpec)
                .param("calc_id", calcId)
                .log().ifValidationFails(LogDetail.URI)
                .get(getCalcInfoExtendedUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.product.product_isn", equalTo("1280351"),
                        "data.result.calc.calc_id", equalTo(calcId),
                        "data.result.policy.status", equalTo("Аннулирован"),
                        "data.result.policy.policy_id", equalTo(calcId));
    }

    /**
     * Попытка создания договора при наличии договора в статусе Выпущен приводит к ошибке.
     */
    @Test
    void createSecondAgreementFail() {
        given().spec(nsSpec)
                .param("calc_id", calcId)
                .log().ifValidationFails(LogDetail.URI)
                .get(createAgreementUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.policy_id", equalTo(calcId));

        given().spec(nsSpec)
                .param("calc_id", calcId)
                .log().ifValidationFails(LogDetail.URI)
                .get(createAgreementUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.text", containsString(
                        "На основе текущей котировки уже создан договор. Повторное создание договора запрещено."));
    }

    @Test
    void getNewAgreementISN() {
        given().spec(cabinetSpec)
                .log().ifValidationFails(LogDetail.URI)
                .get("getNewAgreementIsn")
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.newIsn", notNullValue());
    }

    @Test
    void getAgreementAttachments() {
        given().spec(medinsSpec)
                .param("agreementIsn", "23229938")
                .log().ifValidationFails(LogDetail.URI)
                .get("getAgreementAttachments")
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.attachments.attachment.size()", equalTo(6));
    }

    @Test
    void getPrintableDocument() {
        given().spec(medinsSpec)
                .param("ISN", "23230320")
                .param("TemplateISN", "2262")
                .param("ClassID", "2")
                .log().ifValidationFails(LogDetail.URI)
                .get("getPrintableDocument")
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("Document.FileName", startsWith("C:"));
    }
}
