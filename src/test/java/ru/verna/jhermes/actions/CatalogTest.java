package ru.verna.jhermes.actions;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;
import javax.servlet.http.HttpServletResponse;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

class CatalogTest {

    private static final String constantName = "constantName";
    private static final String includeArchived = "includeArchived";
    private static final String booleanTrue = "true";
    private static final String booleanFalse = "false";
    private static final String catalogCountries = "catalog/countries";
    private static final String catalogCountriesSchengen = "catalog/countries/schengen";
    private static final String catalogList = "catalog/list";
    private static final String catalogNsCoverageterm = "catalog/ns/coverageterm";
    private static final String catalogNsCoverageterritory = "catalog/ns/coverageterritory";
    private static final String catalogNsProfessions = "catalog/ns/professions";
    private static final String catalogNsSports = "catalog/ns/sports";
    private static RequestSpecification spec;

    @BeforeAll
    static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder builder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_WWW);
        spec = builder.build();
    }

    @Test
    void coverageTerritory() {
        String response = given().spec(spec)
                .param(constantName, "ucDictiNSCoverageTerritory")
                .param(includeArchived, booleanTrue)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogList)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", is(2))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogList, response);
    }

    @Test
    void coverageTerritoryFail() {
        String response = given().spec(spec)
                .param(constantName, "ucDictiNSCoverageTerritory1")
                .param(includeArchived, booleanTrue)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogList)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.attr_name", equalTo(constantName))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogList, response);
    }

    @Test
    void aliasCoverageTerritory() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogNsCoverageterritory)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", is(2))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogNsCoverageterritory, response);
    }

    @Test
    void aliasCoverageTerritoryFail() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogNsCoverageterritory + 1)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code", equalTo("104"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogNsCoverageterritory, response);
    }

    @Test
    void sports() {
        given().spec(spec)
                .param(constantName, "ucDictiNSSports")
                .param(includeArchived, booleanFalse)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogList)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", greaterThanOrEqualTo(2));
    }

    @Test
    void aliasSports() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogNsSports)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", greaterThanOrEqualTo(2))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogNsSports, response);
    }

    @Test
    void aliasSportsFail() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogNsSports + 1)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code", equalTo("104"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogNsSports, response);
    }

    @Test
    void coverageTerm() {
        given().spec(spec)
                .param(constantName, "ucDictiNSCoverageTerm")
                .param(includeArchived, booleanFalse)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogList)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", greaterThanOrEqualTo(2));
    }

    @Test
    void aliasCoverageTerm() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogNsCoverageterm)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", greaterThanOrEqualTo(2))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogNsCoverageterm, response);
    }

    @Test
    void aliasCoverageTermFail() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogNsCoverageterm + 1)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code", equalTo("104"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogNsCoverageterm, response);
    }

    @Test
    void profession() {
        given().spec(spec)
                .param(constantName, "ucDictiNSProfession")
                .param(includeArchived, booleanFalse)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogList)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", greaterThanOrEqualTo(2));
    }

    @Test
    void aliasProfession() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogNsProfessions)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", greaterThanOrEqualTo(2))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogNsProfessions, response);
    }

    @Test
    void aliasProfessionFail() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogNsProfessions + 1)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code", equalTo("104"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogNsProfessions, response);
    }

    /**
     * Получение справочника стран через вызов catalog/list.
     */
    @Test
    void countriesThroughCatalogList() {
        given().spec(spec)
                .param(constantName, "ucClausesVZRDepartureCountry")
                .param(includeArchived, booleanFalse)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogList)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", greaterThanOrEqualTo(1));
    }

    /**
     * Получение справочника стран через вызов соответствующего catalog/countries.
     */
    @Test
    void countries() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogCountries)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", greaterThanOrEqualTo(1))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogCountries, response);
    }

    @Test
    void countriesFail() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogCountries + 1)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code", equalTo("104"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogCountries, response);
    }

    /**
     * Получение справочника стран Шенгена.
     */
    @Test
    void schengenCountries() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogCountriesSchengen)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", greaterThanOrEqualTo(1))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogCountriesSchengen, response);
    }

    @Test
    void schengenCountriesFail() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogCountriesSchengen + 1)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code", equalTo("104"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogCountriesSchengen, response);
    }

    /**
     * Возврат ошибки при запросе справочника с ошибочной константой через вызов catalog/list.
     */
    @Test
    void failedCountriesThroughCatalogList() {
        given().spec(spec)
                .param(constantName, "ucClausesVZRDepartureCountry1")
                .param(includeArchived, booleanFalse)
                .log().ifValidationFails(LogDetail.URI)
                .get(catalogList)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.size().toInteger()", greaterThanOrEqualTo(1));
    }

    /**
     * Получение справочника стран с добавлением параметра {@code level}.
     */
    @Test
    void countriesLevel() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .param("level", "1:")
                .get(catalogCountries)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", greaterThanOrEqualTo(1))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(catalogCountries, response);
    }
}
