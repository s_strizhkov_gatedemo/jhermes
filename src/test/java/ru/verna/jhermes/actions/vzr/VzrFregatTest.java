package ru.verna.jhermes.actions.vzr;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;
import javax.servlet.http.HttpServletResponse;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

class VzrFregatTest {

    private static final String uploadagrUrl = "products/vzr/uploadagr"; //USER_BALT_UploadAgr
    private static final String terminareagrUrl = "products/vzr/terminareagr"; //USER_BALT_TerminareAgr
    private static final String updateinsuredUrl = "products/vzr/updateinsured"; //USER_BALT_UpdateInsured
    private static final String clearagrchangeregUrl = "products/vzr/clearagrchangereg"; //USER_BALT_ClearAgrChangeReg
    private static final String clearinsuredchangeregUrl = "products/vzr/clearinsuredchangereg"; //USER_BALT_ClearInsuredChangeReg
    private static RequestSpecification spec;

    @BeforeAll
    static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder builder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_SKYNET);
        spec = builder.build();
    }

    @Test
    void uploadAgr() {
        String response = given().spec(spec)
                .param("type_query", "сBaltAssistanceVZR")
                .log().ifValidationFails(LogDetail.URI)
                .get(uploadagrUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.bordero", notNullValue())
                .extract().response().getBody().asString();
        CommonPart.writeToDb(uploadagrUrl, response);
    }

    @Test
    void uploadAgrFail() {
        String response = given().spec(spec)
                .param("type_query", "xxx")
                .log().ifValidationFails(LogDetail.URI)
                .get(uploadagrUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.text", startsWith("Неправильное значение для"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(uploadagrUrl, response);
    }

    @Test
    void terminateAgr() {
        String response = given().spec(spec)
                .param("type_query", "сBaltAssistanceVZR")
                .log().ifValidationFails(LogDetail.URI)
                .get(terminareagrUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.bordero", notNullValue())
                .extract().response().getBody().asString();
        CommonPart.writeToDb(terminareagrUrl, response);
    }

    @Test
    void terminateAgrFail() {
        String response = given().spec(spec)
                .param("type_query", "xxx")
                .log().ifValidationFails(LogDetail.URI)
                .get(terminareagrUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.text", startsWith("Неправильное значение для"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(terminareagrUrl, response);
    }

    @Test
    void updateInsured() {
        String response = given().spec(spec)
                .param("type_query", "сBaltAssistanceVZR")
                .log().ifValidationFails(LogDetail.URI)
                .get(updateinsuredUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.bordero", notNullValue())
                .extract().response().getBody().asString();
        CommonPart.writeToDb(updateinsuredUrl, response);
    }

    @Test
    void updateInsuredFail() {
        String response = given().spec(spec)
                .param("type_query", "xxx")
                .log().ifValidationFails(LogDetail.URI)
                .get(updateinsuredUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.text", startsWith("Неправильное значение для параметра"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(updateinsuredUrl, response);
    }

    @Test
    void clearAgrChangeReg() {
        String params = "{\n"
                + "    'bordero': {\n"
                + "        'id_policy_ic': [\n"
                + "            23114053,\n"
                + "            23114054\n"
                + "        ]\n"
                + "    }\n"
                + "}";
        String response = given().spec(spec)
                .param("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .get(clearagrchangeregUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.status", is("Ok"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(clearagrchangeregUrl, response);
    }

    @Test
    void clearAgrChangeRegFail() {
        String params = "{\n"
                + "    'bordero': {\n"
                + "        'id_policy_ic': [\n"
                + "            xxx,\n"
                + "            yyy\n"
                + "        ]\n"
                + "    }\n"
                + "}";
        String response = given().spec(spec)
                .param("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .get(clearagrchangeregUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.text", startsWith("Исключение при попытке получения"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(clearagrchangeregUrl, response);
    }

    @Test
    void clearInsuredChangeReg() {
        String params = "{\n"
                + "    'insured_list': {\n"
                + "        'insured': [\n"
                + "            {\n"
                + "                'id_policy_ic': 23114053,\n"
                + "                'id_person_ic': 23114054\n"
                + "            },\n"
                + "            {\n"
                + "                'id_policy_ic': 23114055,\n"
                + "                'id_person_ic': 23114056\n"
                + "            }\n"
                + "        ]\n"
                + "    }\n"
                + "}";
        String response = given().spec(spec)
                .param("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .get(clearinsuredchangeregUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.status", is("Ok"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(clearinsuredchangeregUrl, response);
    }

    @Disabled // отключен 15.11.2018 до ближайшего обновления БД из-за разного поведения боевого и тестового окружения
    @Test
    void clearInsuredChangeRegFail() {
        String params = "{\n"
                + "    'insured_list': {\n"
                + "        'insured': [\n"
                + "            {\n"
                + "                'id_policy_ic': xxx,\n"
                + "                'id_person_ic': xxx\n"
                + "            },\n"
                + "            {\n"
                + "                'id_policy_ic': yyy,\n"
                + "                'id_person_ic': yyy\n"
                + "            }\n"
                + "        ]\n"
                + "    }\n"
                + "}";
        String response = given().spec(spec)
                .param("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .get(clearinsuredchangeregUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.status", is("Ok"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(clearinsuredchangeregUrl, response);
    }
}
