package ru.verna.jhermes.actions.vzr;

import com.jayway.jsonpath.JsonPath;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

class VzrTest {

    private static final String expressCalcUrl = "products/vzr/expressCalc";
    private static final String createFullCalcUrl = "products/vzr/createCalc";
    private static final String updateFullCalcUrl = "products/vzr/updateCalc";
    private static RequestSpecification spec;
    /**
     * Запускаемая в тестах котировка
     */
    private static String calcId = "";
    private static final String startDate = CommonPart.formatLocalDate(LocalDate.now().plusDays(10));
    private static final String endDate = CommonPart.formatLocalDate(LocalDate.now().plusDays(99));
    private final String failStartDate = CommonPart.formatLocalDate(LocalDate.now().minusDays(1));

    @BeforeAll
    static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder builder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_WWW);
        spec = builder.build();
        calcId = getCalcId();
    }

    @Test
    void expressCalc() {
        String params = "{\n" +
                "    'coverage': {\n" +
                "        'risks': {\n" +
                "            'risk': [\n" +
                "                {\n" +
                "                    'insuredSumm': 35000,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskPackageVZRComfort'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 1500,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRTripCancellation'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 500,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRBaggageLoss'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 1000,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRLegalAssistance'\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "    },\n" +
                "    'clauses': {\n" +
                "        'clause': [\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 15260\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 199760\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 14270\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    'term': {\n" +
                "        'end': '" + endDate + "',\n" +
                "        'begin': '" + startDate + "'\n" +
                "    },\n" +
                "    'attributes': {\n" +
                "        'attribute': [\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRInsurredDaysCount',\n" +
                "                'value': 30\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRBaggageLotsCount',\n" +
                "                'value': 2\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRPolicyType',\n" +
                "                'value': 'ucDictiVZRPolicyTypeMultivisa'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrNSKindOfSport',\n" +
                "                'value': 'ucDictiNSSportNone'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrNSKindOfSport_Other',\n" +
                "                'value': 'Не занимаюсь спортом'\n" +
                "            },\n" +
                "            {\n" +
                "               'id': 'ucPartnerId',\n" +
                "                'value': 'ucVernaTest'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRTripType',\n" +
                "                'value': 'ucVZRTripTypeBusiness'\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    'insurants': {\n" +
                "        'insurant': [\n" +
                "            {\n" +
                "                'age': 35\n" +
                "            },\n" +
                "            {\n" +
                "                'age': 25\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    'remark': 'Эта котировка создана автотестами'\n" +
                "}";
        String response = given().spec(spec)
                .param("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .get(expressCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.calc_sum.toDouble()", isA(double.class))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(expressCalcUrl, response);
    }

    @Test
    void failMissingRequiredExpressCalc() {
        String params = "{\n" +
                "    'coverage': {\n" +
                "        'risks': {\n" +
                "            'risk': [\n" +
                "                {\n" +
                "                    'insuredSumm': 35000,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskPackageVZRComfort'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 1500,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRTripCancellation'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 500,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRBaggageLoss'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 1000,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRLegalAssistance'\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "    },\n" +
                "    'clauses': {\n" +
                "        'clause': [\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 15260\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 199760\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 14270\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    'term': {\n" +
                "        'end': '" + endDate + "',\n" +
                "        'begin': '" + startDate + "'\n" +
                "    },\n" +
                "    'attributes': {\n" +
                "        'attribute': [\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRInsurredDaysCount',\n" +
                "                'value': 30\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRPolicyType',\n" +
                "                'value': 'ucDictiVZRPolicyTypeMultivisa'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrNSKindOfSport',\n" +
                "                'value': 'ucDictiNSSportNone'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrNSKindOfSport_Other',\n" +
                "                'value': 'Не занимаюсь спортом'\n" +
                "            },\n" +
                "            {\n" +
                "               'id': 'ucPartnerId',\n" +
                "                'value': 'ucVernaTest'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRTripType',\n" +
                "                'value': 'ucVZRTripTypeBusiness'\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n"
                + "  'remark': 'Эта котировка создана автотестами'\n"
                + "}";
        String response = given().spec(spec)
                .param("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .get(expressCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code.toInteger()", isA(int.class))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(expressCalcUrl, response);
    }

    @Test
    void createFullCalc() {
        String params = "{\n" +
                "    'coverage': {\n" +
                "        'risks': {\n" +
                "            'risk': [\n" +
                "                {\n" +
                "                    'insuredSumm': 35000,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskPackageVZRComfort'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 1500,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRTripCancellation'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 500,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRBaggageLoss'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 1000,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRLegalAssistance'\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "    },\n" +
                "    'premium': '',\n" +
                "    'isn': '',\n" +
                "    'clauses': {\n" +
                "        'clause': [\n" +
                "            {\n" +
                "                'id': 'ucClausesCommonSalesChannel',\n" +
                "                'value': 'ucClausesCommonSalesChannel_Online'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 13610\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 13620\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 13610\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    'insurer': {\n" +
                "        'firstName': 'Гермес',\n" +
                "        'lastName': 'Гермесов',\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7700000000000,\n" +
                "                'custom_value': 'г Москва',\n" +
                "                'name': 'Москва',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 300,\n" +
                "            'street': {\n" +
                "                'code': 77000000000287700,\n" +
                "                'custom_value': 'ул Тверская',\n" +
                "                'name': 'Тверская',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 125009,\n" +
                "            'region': {\n" +
                "                'code': 7700000000000,\n" +
                "                'custom_value': 'г Москва',\n" +
                "                'name': 'Москва',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'house': 12,\n" +
                "            'building': 1\n" +
                "        },\n" +
                "        'gender': 'М',\n" +
                "        'isn': '',\n" +
                "        'phone': '+79184111147',\n" +
                "        'documents': {\n" +
                "            'document': {\n" +
                "                'date': '02.01.1998',\n" +
                "                'number': 111111,\n" +
                "                'series': '0715',\n" +
                "                'issuedBy': 'УФМС',\n" +
                "                'type': 'ucRFPassport'\n" +
                "            }\n" +
                "        },\n" +
                "        'middleName': 'Гермесович',\n" +
                "        'birthDate': '01.01.1980',\n" +
                "        'email': 'germesov@verna-group.ru'\n" +
                "    },\n" +
                "    'calcId': '',\n" +
                "    'term': {\n" +
                "        'end': '" + endDate + "',\n" +
                "        'begin': '" + startDate + "'\n" +
                "    },\n" +
                "    'promoCode': '',\n" +
                "    'attributes': {\n" +
                "        'attribute': [\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRPolicyType',\n" +
                "                'value': 'ucDictiVZRPolicyTypeMultivisa'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrNSKindOfSport',\n" +
                "                'value': 'ucDictiNSSportNone'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrNSKindOfSport_Other',\n" +
                "                'value': 'Не занимаюсь спортом'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRTripType',\n" +
                "                'value': 'ucVZRTripTypeNonBusiness'\n" +
                "            },\n" +
                "            {\n" +
                "               'id': 'ucPartnerId',\n" +
                "                'value': 'ucVernaTest'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRInsurredDaysCount',\n" +
                "                'value': 30\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    'insurants': {\n" +
                "        'insurant': {\n" +
                "            'firstName': 'Гермес',\n" +
                "            'lastName': 'Гермесов',\n" +
                "            'firstNameLat': 'Germes',\n" +
                "            'lastNameLat': 'Germesov',\n" +
                "            'address': {\n" +
                "                'city': {\n" +
                "                    'code': 7700000000000,\n" +
                "                    'custom_value': 'г Москва',\n" +
                "                    'name': 'Москва',\n" +
                "                    'abbr': 'г'\n" +
                "                },\n" +
                "                'street': {\n" +
                "                    'code': 77000000000287700,\n" +
                "                    'custom_value': 'ул Тверская',\n" +
                "                    'name': 'Тверская',\n" +
                "                    'abbr': 'ул'\n" +
                "                },\n" +
                "                'flat': 300,\n" +
                "                'district': {\n" +
                "                    'custom_value': ''\n" +
                "                },\n" +
                "                'postcode': 125009,\n" +
                "                'locality': {\n" +
                "                    'custom_value': ''\n" +
                "                },\n" +
                "                'region': {\n" +
                "                    'code': 7700000000000,\n" +
                "                    'custom_value': 'г Москва',\n" +
                "                    'name': 'Москва',\n" +
                "                    'abbr': 'г'\n" +
                "                },\n" +
                "                'house': 12,\n" +
                "                'building': 1\n" +
                "            },\n" +
                "            'gender': 'М',\n" +
                "            'isn': '',\n" +
                "            'documents': {\n" +
                "                'document': {\n" +
                "                    'date': '02.01.1998',\n" +
                "                    'number': '010101',\n" +
                "                    'series': '0715',\n" +
                "                    'issuedBy': 'УФМС',\n" +
                "                    'type': 'ucRFPassport'\n" +
                "                }\n" +
                "            },\n" +
                "            'middleName': 'Гермесович',\n" +
                "            'birthDate': '01.01.1980'\n" +
                "        }\n" +
                "    },\n" +
                "    'remark': 'Эта котировка создана автотестами'\n" +
                "}";
        String response = given().spec(spec)
                .header("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
                .formParam("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .post(createFullCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.calcId", notNullValue())
                .body("data.result.insurer.isn.toInteger()", isA(int.class))
                .body("data.result.insurants.insurant.isn.toInteger()", isA(int.class))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(createFullCalcUrl, response);
    }

    @Test
    void failMissingRequiredFullCalc() {
        String params = "{\n"
                + "  'hash': 'v7u4vv78',\n"
                + "  'coverage': {\n"
                + "    'risks': {\n"
                + "      'risk': [\n"
                + "        {\n"
                + "          'hash': '8gi81prd',\n"
                + "          'id': 'ucRiskPackageVZRComfort',\n"
                + "          'currency': 'ucCurrencyUSD',\n"
                + "          'insuredSumm': '50000'\n"
                + "        },\n"
                + "        {\n"
                + "          'hash': '1bf5kggf3',\n"
                + "          'id': 'ucRiskVZRBaggageLoss',\n"
                + "          'currency': 'ucCurrencyUSD',\n"
                + "          'insuredSumm': '200'\n"
                + "        }\n"
                + "      ]\n"
                + "    }\n"
                + "  },\n"
                + "  'attributes': {\n"
                + "    'attribute': [\n"
                + "      {\n"
                + "        'hash': 'n258gq9a',\n"
                + "        'id': 'ucAddAtrNSKindOfSport',\n"
                + "        'value': '1327841'\n"
                + "      },\n"
                + "      {\n"
                + "        'hash': '17njfamlt',\n"
                + "        'id': 'ucAddAtrNSKindOfSport_Other',\n"
                + "        'value': 'Акробатика'\n"
                + "      },\n"
                + "      {\n"
                + "        'hash': 'ovt7dv2e',\n"
                + "        'id': 'ucAddAtrVZRTripType',\n"
                + "        'value': 'ucVZRTripTypeNonBusiness'\n"
                + "      },\n"
                + "      {\n"
                + "        'hash': 'ri6a7r4i',\n"
                + "        'id': 'ucAddAtrVZRBaggageLotsCount',\n"
                + "        'value': '1'\n"
                + "      },\n"
                + "      {\n"
                + "        'hash': 'fpp6cg9p',\n"
                + "        'id': 'ucAddAtrVZRInsurredDaysCount',\n"
                + "        'value': '1'\n"
                + "      },\n"
                + "      {\n"
                + "         'id': 'ucPartnerId',\n"
                + "          'value': 'ucVernaTest'\n"
                + "      },\n"
                + "      {\n"
                + "        'hash': '16n7bo79i',\n"
                + "        'id': 'ucAddAtrVZRPolicyType',\n"
                + "        'value': 'ucDictiVZRPolicyTypeSinglevisa'\n"
                + "      }\n"
                + "    ]\n"
                + "  },\n"
                + "  'insurants': {\n"
                + "    'insurant': [\n"
                + "      {\n"
                + "        'hash': 'e8td2svi',\n"
                + "        'isn': '',\n"
                + "        'firstName': '',\n"
                + "        'firstNameLat': 'Ian',\n"
                + "        'lastName': 'Гермесов',\n"
                + "        'lastNameLat': 'Germesov',\n"
                + "        'middleName': 'Гермесович',\n"
                + "        'birthDate': '01.01.1980',\n"
                + "        'gender': 'М',\n"
                + "        'documents': {\n"
                + "          'document': [\n"
                + "            {\n"
                + "              'hash': '12su8up9s',\n"
                + "              'date': '20.05.2010',\n"
                + "              'number': '',\n"
                + "              'series': '2222',\n"
                + "              'issuedBy': 'SADAD',\n"
                + "              'type': 'ucRFInternationalPassport'\n"
                + "            }\n"
                + "          ]\n"
                + "        }\n"
                + "      }\n"
                + "    ]\n"
                + "  },\n"
                + "  'clauses': {\n"
                + "    'clause': [\n"
                + "      {\n"
                + "        'hash': '4s6oqk3u',\n"
                + "        'id': 'ucClausesCommonSalesChannel',\n"
                + "        'value': 'ucClausesCommonSalesChannel_Online'\n"
                + "      },\n"
                + "      {\n"
                + "        'hash': '15gapabfq',\n"
                + "        'id': 'ucClausesVZRDepartureCountry',\n"
                + "        'value': '13910'\n"
                + "      }\n"
                + "    ]\n"
                + "  },\n"
                + "  'term': {\n"
                + "    'hash': '63t2pgif',\n"
                + "    'begin': '" + startDate + "',\n"
                + "    'end': '" + endDate + "'\n"
                + "  },\n"
                + "  'insurer': {\n"
                + "    'hash': '15vqfldm0',\n"
                + "    'isn': '',\n"
                + "    'birthDate': '01.01.1980',\n"
                + "    'firstName': 'Гермес',\n"
                + "    'middleName': 'Гермесович',\n"
                + "    'lastName': 'Гермесов',\n"
                + "    'phone': '+79181111111',\n"
                + "    'email': 'test@verna-group.ru',\n"
                + "    'gender': 'М',\n"
                + "    'documents': {\n"
                + "      'document': [\n"
                + "        {\n"
                + "          'type': 'ucRFPassport',\n"
                + "          'series': '0715',\n"
                + "          'number': '111111',\n"
                + "          'date': '02.01.1998',\n"
                + "          'issuedBy': 'УФМС'\n"
                + "        }\n"
                + "      ]\n"
                + "    },\n"
                + "    'address': {\n"
                + "      'region': {\n"
                + "        'custom_value': 'г. Москва',\n"
                + "        'name': 'Москва',\n"
                + "        'abbr': 'г',\n"
                + "        'code': '7700000000000'\n"
                + "      },\n"
                + "      'district': {\n"
                + "        'custom_value': ''\n"
                + "      },\n"
                + "      'city': {\n"
                + "        'custom_value': 'г. Москва',\n"
                + "        'name': 'Москва',\n"
                + "        'abbr': 'г',\n"
                + "        'code': '7700000000000'\n"
                + "      },\n"
                + "      'locality': {\n"
                + "        'custom_value': ''\n"
                + "      },\n"
                + "      'street': {\n"
                + "        'custom_value': 'ул. Тверская',\n"
                + "        'name': 'Тверская',\n"
                + "        'abbr': 'ул',\n"
                + "        'code': '77000000000287700'\n"
                + "      },\n"
                + "      'house': '12',\n"
                + "      'building': '1',\n"
                + "      'flat': '300',\n"
                + "      'postcode': '125009'\n"
                + "    }\n"
                + "  },\n"
                + "  'calcId': '',\n"
                + "  'isn': '',\n"
                + "  'premium': '',\n"
                + "  'remark': 'Эта котировка создана автотестами'\n"
                + "}";
        String response = given().spec(spec)
                .header("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
                .formParam("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .post(createFullCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.size().toInteger()", equalTo(2))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(createFullCalcUrl, response);
    }

    @Test
    void updateFullCalc() {
        String params = "{\n" +
                "    'coverage': {\n" +
                "        'risks': {\n" +
                "            'risk': [\n" +
                "                {\n" +
                "                    'insuredSumm': 35000,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskPackageVZRComfort'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 1500,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRTripCancellation'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 500,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRBaggageLoss'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 1000,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRLegalAssistance'\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "    },\n" +
                "    'premium': '',\n" +
                "    'isn': '" + CommonPart.getProperty("VzrTest.isn") + "',\n" +
                "    'clauses': {\n" +
                "        'clause': [\n" +
                "            {\n" +
                "                'id': 'ucClausesCommonSalesChannel',\n" +
                "                'value': 'ucClausesCommonSalesChannel_Online'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 13610\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 13620\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 13610\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    'insurer': {\n" +
                "        'firstName': 'Гермес',\n" +
                "        'lastName': 'Гермесов',\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7700000000000,\n" +
                "                'custom_value': 'г Москва',\n" +
                "                'name': 'Москва',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 300,\n" +
                "            'street': {\n" +
                "                'code': 77000000000287700,\n" +
                "                'custom_value': 'ул Тверская',\n" +
                "                'name': 'Тверская',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 125009,\n" +
                "            'region': {\n" +
                "                'code': 7700000000000,\n" +
                "                'custom_value': 'г Москва',\n" +
                "                'name': 'Москва',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'house': 12,\n" +
                "            'building': 1\n" +
                "        },\n" +
                "        'gender': 'М',\n" +
                "        'isn': '" + CommonPart.getProperty("VzrTest.insurer.isn") + "',\n" +
                "        'phone': '+79184111147',\n" +
                "        'documents': {\n" +
                "            'document': {\n" +
                "                'date': '02.01.1998',\n" +
                "                'number': 111111,\n" +
                "                'series': '0715',\n" +
                "                'issuedBy': 'УФМС',\n" +
                "                'type': 'ucRFPassport'\n" +
                "            }\n" +
                "        },\n" +
                "        'middleName': 'Гермесович',\n" +
                "        'birthDate': '01.01.1980',\n" +
                "        'email': 'germesov@verna-group.ru'\n" +
                "    },\n" +
                "    'calcId': '" + calcId + "',\n" +
                "    'term': {\n" +
                "        'end': '" + endDate + "',\n" +
                "        'begin': '" + startDate + "'\n" +
                "    },\n" +
                "    'promoCode': '',\n" +
                "    'attributes': {\n" +
                "        'attribute': [\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRPolicyType',\n" +
                "                'value': 'ucDictiVZRPolicyTypeMultivisa'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrNSKindOfSport',\n" +
                "                'value': 'ucDictiNSSportNone'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrNSKindOfSport_Other',\n" +
                "                'value': 'Не занимаюсь спортом'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRTripType',\n" +
                "                'value': 'ucVZRTripTypeNonBusiness'\n" +
                "            },\n" +
                "            {\n" +
                "               'id': 'ucPartnerId',\n" +
                "                'value': 'ucVernaTest'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRInsurredDaysCount',\n" +
                "                'value': 30\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    'insurants': {\n" +
                "        'insurant': {\n" +
                "            'firstName': 'Гермес',\n" +
                "            'lastName': 'Гермесов',\n" +
                "            'firstNameLat': 'Germes',\n" +
                "            'lastNameLat': 'Germesov',\n" +
                "            'address': {\n" +
                "                'city': {\n" +
                "                    'code': 7700000000000,\n" +
                "                    'custom_value': 'г Москва',\n" +
                "                    'name': 'Москва',\n" +
                "                    'abbr': 'г'\n" +
                "                },\n" +
                "                'street': {\n" +
                "                    'code': 77000000000287700,\n" +
                "                    'custom_value': 'ул Тверская',\n" +
                "                    'name': 'Тверская',\n" +
                "                    'abbr': 'ул'\n" +
                "                },\n" +
                "                'flat': 300,\n" +
                "                'district': {\n" +
                "                    'custom_value': ''\n" +
                "                },\n" +
                "                'postcode': 125009,\n" +
                "                'locality': {\n" +
                "                    'custom_value': ''\n" +
                "                },\n" +
                "                'region': {\n" +
                "                    'code': 7700000000000,\n" +
                "                    'custom_value': 'г Москва',\n" +
                "                    'name': 'Москва',\n" +
                "                    'abbr': 'г'\n" +
                "                },\n" +
                "                'house': 12,\n" +
                "                'building': 1\n" +
                "            },\n" +
                "            'gender': 'М',\n" +
                "            'isn': '" + CommonPart.getProperty("VzrTest.insurant.isn") + "',\n" +
                "            'documents': {\n" +
                "                'document': {\n" +
                "                    'date': '02.01.1998',\n" +
                "                    'number': '010101',\n" +
                "                    'series': '0715',\n" +
                "                    'issuedBy': 'УФМС',\n" +
                "                    'type': 'ucRFPassport'\n" +
                "                }\n" +
                "            },\n" +
                "            'middleName': 'Гермесович',\n" +
                "            'birthDate': '01.01.1980'\n" +
                "        }\n" +
                "    },\n"
                + "  'remark': 'Эта котировка создана автотестами'\n"
                + "}";
        String response = given().spec(spec)
                .header("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
                .formParam("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .post(updateFullCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.calcId", notNullValue())
                .body("data.result.insurer.isn.toInteger()", isA(int.class))
                .body("data.result.insurants.insurant.isn.toInteger()", isA(int.class))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(updateFullCalcUrl, response);
    }

    @Test
    void updateFullCalcFail() {
        String params = "{\n" +
                "    'coverage': {\n" +
                "        'risks': {\n" +
                "            'risk': [\n" +
                "                {\n" +
                "                    'insuredSumm': 35000,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskPackageVZRComfort'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 1500,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRTripCancellation'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 500,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRBaggageLoss'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 1000,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRLegalAssistance'\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "    },\n" +
                "    'premium': '',\n" +
                "    'isn': '" + CommonPart.getProperty("VzrTest.isn") + "',\n" +
                "    'clauses': {\n" +
                "        'clause': [\n" +
                "            {\n" +
                "                'id': 'ucClausesCommonSalesChannel',\n" +
                "                'value': 'ucClausesCommonSalesChannel_Online'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 13610\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 13620\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 13610\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    'insurer': {\n" +
                "        'firstName': 'Гермес',\n" +
                "        'lastName': 'Гермесов',\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7700000000000,\n" +
                "                'custom_value': 'г Москва',\n" +
                "                'name': 'Москва',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 300,\n" +
                "            'street': {\n" +
                "                'code': 77000000000287700,\n" +
                "                'custom_value': 'ул Тверская',\n" +
                "                'name': 'Тверская',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 125009,\n" +
                "            'region': {\n" +
                "                'code': 7700000000000,\n" +
                "                'custom_value': 'г Москва',\n" +
                "                'name': 'Москва',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'house': 12,\n" +
                "            'building': 1\n" +
                "        },\n" +
                "        'gender': 'М',\n" +
                "        'isn': '" + CommonPart.getProperty("VzrTest.insurer.isn") + "',\n" +
                "        'phone': '+79184111147',\n" +
                "        'documents': {\n" +
                "            'document': {\n" +
                "                'date': '02.01.1998',\n" +
                "                'number': 111111,\n" +
                "                'series': '0715',\n" +
                "                'issuedBy': 'УФМС',\n" +
                "                'type': 'ucRFPassport'\n" +
                "            }\n" +
                "        },\n" +
                "        'middleName': 'Гермесович',\n" +
                "        'birthDate': '01.01.1980',\n" +
                "        'email': 'germesov@verna-group.ru'\n" +
                "    },\n" +
                "    'calcId': '" + CommonPart.getProperty("VzrTest.calcId") + "',\n" +
                "    'term': {\n" +
                "        'end': '" + endDate + "',\n" +
                "        'begin': '" + failStartDate + "'\n" +
                "    },\n" +
                "    'promoCode': '',\n" +
                "    'attributes': {\n" +
                "        'attribute': [\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRPolicyType',\n" +
                "                'value': 'ucDictiVZRPolicyTypeMultivisa'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrNSKindOfSport',\n" +
                "                'value': 'ucDictiNSSportNone'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrNSKindOfSport_Other',\n" +
                "                'value': 'Не занимаюсь спортом'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRTripType',\n" +
                "                'value': 'ucVZRTripTypeNonBusiness'\n" +
                "            },\n" +
                "            {\n" +
                "               'id': 'ucPartnerId',\n" +
                "                'value': 'ucVernaTest'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRInsurredDaysCount',\n" +
                "                'value': 30\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    'insurants': {\n" +
                "        'insurant': {\n" +
                "            'firstName': 'Гермес',\n" +
                "            'lastName': 'Гермесов',\n" +
                "            'firstNameLat': 'Germes',\n" +
                "            'lastNameLat': 'Germesov',\n" +
                "            'address': {\n" +
                "                'city': {\n" +
                "                    'code': 7700000000000,\n" +
                "                    'custom_value': 'г Москва',\n" +
                "                    'name': 'Москва',\n" +
                "                    'abbr': 'г'\n" +
                "                },\n" +
                "                'street': {\n" +
                "                    'code': 77000000000287700,\n" +
                "                    'custom_value': 'ул Тверская',\n" +
                "                    'name': 'Тверская',\n" +
                "                    'abbr': 'ул'\n" +
                "                },\n" +
                "                'flat': 300,\n" +
                "                'district': {\n" +
                "                    'custom_value': ''\n" +
                "                },\n" +
                "                'postcode': 125009,\n" +
                "                'locality': {\n" +
                "                    'custom_value': ''\n" +
                "                },\n" +
                "                'region': {\n" +
                "                    'code': 7700000000000,\n" +
                "                    'custom_value': 'г Москва',\n" +
                "                    'name': 'Москва',\n" +
                "                    'abbr': 'г'\n" +
                "                },\n" +
                "                'house': 12,\n" +
                "                'building': 1\n" +
                "            },\n" +
                "            'gender': 'М',\n" +
                "            'isn': '" + CommonPart.getProperty("VzrTest.insurant.isn") + "',\n" +
                "            'documents': {\n" +
                "                'document': {\n" +
                "                    'date': '02.01.1998',\n" +
                "                    'number': '010101',\n" +
                "                    'series': '0715',\n" +
                "                    'issuedBy': 'УФМС',\n" +
                "                    'type': 'ucRFPassport'\n" +
                "                }\n" +
                "            },\n" +
                "            'middleName': 'Гермесович',\n" +
                "            'birthDate': '01.01.1980'\n" +
                "        }\n" +
                "    },\n" +
                "    'remark': 'Эта котировка создана автотестами'\n" +
                "}";
        String response = given().spec(spec)
                .header("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
                .formParam("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .post(updateFullCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row[0].text", startsWith("Дата начала действия полиса"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(updateFullCalcUrl, response);
    }

    /**
     * Информация о тестовой котировке НС-онлайна
     */
    @Test
    void getPaymentData() {
        String path = "getPaymentData";
        given().spec(spec)
                .param("calc_id", calcId)
                .log().ifValidationFails(LogDetail.URI)
                .get(path)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.product_name", startsWith("ВЗР-Онлайн"),
                        "data.result.calc_sum", notNullValue())
                .extract().response().getBody().asString();
    }

    private static String getCalcId() {
        String params = "{\n" +
                "    'coverage': {\n" +
                "        'risks': {\n" +
                "            'risk': [\n" +
                "                {\n" +
                "                    'insuredSumm': 35000,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskPackageVZRComfort'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 1500,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRTripCancellation'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 500,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRBaggageLoss'\n" +
                "                },\n" +
                "                {\n" +
                "                    'insuredSumm': 1000,\n" +
                "                    'currency': 'ucCurrencyUSD',\n" +
                "                    'id': 'ucRiskVZRLegalAssistance'\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "    },\n" +
                "    'premium': '',\n" +
                "    'isn': '',\n" +
                "    'clauses': {\n" +
                "        'clause': [\n" +
                "            {\n" +
                "                'id': 'ucClausesCommonSalesChannel',\n" +
                "                'value': 'ucClausesCommonSalesChannel_Online'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 13610\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 13620\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucClausesVZRDepartureCountry',\n" +
                "                'value': 13610\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    'insurer': {\n" +
                "        'firstName': 'Гермес',\n" +
                "        'lastName': 'Гермесов',\n" +
                "        'address': {\n" +
                "            'city': {\n" +
                "                'code': 7700000000000,\n" +
                "                'custom_value': 'г Москва',\n" +
                "                'name': 'Москва',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'flat': 300,\n" +
                "            'street': {\n" +
                "                'code': 77000000000287700,\n" +
                "                'custom_value': 'ул Тверская',\n" +
                "                'name': 'Тверская',\n" +
                "                'abbr': 'ул'\n" +
                "            },\n" +
                "            'district': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'locality': {\n" +
                "                'custom_value': ''\n" +
                "            },\n" +
                "            'postcode': 125009,\n" +
                "            'region': {\n" +
                "                'code': 7700000000000,\n" +
                "                'custom_value': 'г Москва',\n" +
                "                'name': 'Москва',\n" +
                "                'abbr': 'г'\n" +
                "            },\n" +
                "            'house': 12,\n" +
                "            'building': 1\n" +
                "        },\n" +
                "        'gender': 'М',\n" +
                "        'isn': '',\n" +
                "        'phone': '+79184111147',\n" +
                "        'documents': {\n" +
                "            'document': {\n" +
                "                'date': '02.01.1998',\n" +
                "                'number': 111111,\n" +
                "                'series': '0715',\n" +
                "                'issuedBy': 'УФМС',\n" +
                "                'type': 'ucRFPassport'\n" +
                "            }\n" +
                "        },\n" +
                "        'middleName': 'Гермесович',\n" +
                "        'birthDate': '01.01.1980',\n" +
                "        'email': 'germesov@verna-group.ru'\n" +
                "    },\n" +
                "    'calcId': '',\n" +
                "    'term': {\n" +
                "        'end': '" + endDate + "',\n" +
                "        'begin': '" + startDate + "'\n" +
                "    },\n" +
                "    'promoCode': '',\n" +
                "    'attributes': {\n" +
                "        'attribute': [\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRPolicyType',\n" +
                "                'value': 'ucDictiVZRPolicyTypeMultivisa'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrNSKindOfSport',\n" +
                "                'value': 'ucDictiNSSportNone'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrNSKindOfSport_Other',\n" +
                "                'value': 'Не занимаюсь спортом'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRTripType',\n" +
                "                'value': 'ucVZRTripTypeNonBusiness'\n" +
                "            },\n" +
                "            {\n" +
                "               'id': 'ucPartnerId',\n" +
                "                'value': 'ucVernaTest'\n" +
                "            },\n" +
                "            {\n" +
                "                'id': 'ucAddAtrVZRInsurredDaysCount',\n" +
                "                'value': 30\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    'insurants': {\n" +
                "        'insurant': {\n" +
                "            'firstName': 'Гермес',\n" +
                "            'lastName': 'Гермесов',\n" +
                "            'firstNameLat': 'Germes',\n" +
                "            'lastNameLat': 'Germesov',\n" +
                "            'address': {\n" +
                "                'city': {\n" +
                "                    'code': 7700000000000,\n" +
                "                    'custom_value': 'г Москва',\n" +
                "                    'name': 'Москва',\n" +
                "                    'abbr': 'г'\n" +
                "                },\n" +
                "                'street': {\n" +
                "                    'code': 77000000000287700,\n" +
                "                    'custom_value': 'ул Тверская',\n" +
                "                    'name': 'Тверская',\n" +
                "                    'abbr': 'ул'\n" +
                "                },\n" +
                "                'flat': 300,\n" +
                "                'district': {\n" +
                "                    'custom_value': ''\n" +
                "                },\n" +
                "                'postcode': 125009,\n" +
                "                'locality': {\n" +
                "                    'custom_value': ''\n" +
                "                },\n" +
                "                'region': {\n" +
                "                    'code': 7700000000000,\n" +
                "                    'custom_value': 'г Москва',\n" +
                "                    'name': 'Москва',\n" +
                "                    'abbr': 'г'\n" +
                "                },\n" +
                "                'house': 12,\n" +
                "                'building': 1\n" +
                "            },\n" +
                "            'gender': 'М',\n" +
                "            'isn': '',\n" +
                "            'documents': {\n" +
                "                'document': {\n" +
                "                    'date': '02.01.1998',\n" +
                "                    'number': '010101',\n" +
                "                    'series': '0715',\n" +
                "                    'issuedBy': 'УФМС',\n" +
                "                    'type': 'ucRFPassport'\n" +
                "                }\n" +
                "            },\n" +
                "            'middleName': 'Гермесович',\n" +
                "            'birthDate': '01.01.1980'\n" +
                "        }\n" +
                "    },\n" +
                "    'remark': 'Эта котировка создана автотестами'\n" +
                "}";
        String response = given().spec(spec)
                .header("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
                .formParam("params", params)
                .log().ifValidationFails(LogDetail.URI)
                .post(createFullCalcUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.calcId", notNullValue())
                .body("data.result.insurer.isn.toInteger()", isA(int.class))
                .body("data.result.insurants.insurant.isn.toInteger()", isA(int.class))
                .extract().response().getBody().asString();
        return JsonPath.read(response, "data.result.calcId");
    }
}
