package ru.verna.jhermes.actions;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;
import javax.servlet.http.HttpServletResponse;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

class GetInvoicePaymentUrlTest {

    private static final String createAndBuyUrl = "getInvoicePaymentUrl";
    private static RequestSpecification spec;

    @BeforeAll
    static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder builder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_NS);
        spec = builder.build();
    }

    @Test
    void getInvoicePaymentUrl() {
        given().spec(spec)
                .param("isn", "102290483")
                .param("partner_success_url", "http://localhost:8080/TEST/successPayment.jsp?p1=v1&p2=v2&p2=v3")
                .param("partner_failure_url", "http://localhost:8080/TEST/failurePayment.jsp?p3=v4&p5=v6")
                .param("partner_callback_url", "http://localhost:8080/TEST/callback.jsp?p1=v1&p2=v2&p2=v3")
                .param("postbank_url", "http://localhost:8080/TEST/postbank_url.jsp?p1=v1&p2=v2&p2=v3")
                .log().ifValidationFails(LogDetail.URI)
                .get(createAndBuyUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.url", containsString("https"))
                .body("data.result.url", containsString("payment_ru.html"));
    }
}
