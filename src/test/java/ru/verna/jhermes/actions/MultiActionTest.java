package ru.verna.jhermes.actions;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;
import javax.servlet.http.HttpServletResponse;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

/**
 *
 */
class MultiActionTest {

    private static final String multipleactionUrl = "multipleaction";
    private static RequestSpecification spec;

    @BeforeAll
    static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder builder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_WWW);
        spec = builder.build();
    }

    /**
     * Выполнение списка действий.
     */
    @Test
    void multipleAction() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .param("actions", (Object[]) new String[] {"catalog/ns/coverageterritory", "/catalog/ns/sports", "catalog/ns/coverageterm"})
                .get(multipleactionUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.catalog_ns_coverageterritory.result.row.size().toInteger()", greaterThanOrEqualTo(1))
                .body("data._catalog_ns_sports.error.size().toInteger()", equalTo(2))
                .body("data.catalog_ns_coverageterm.result.row.size().toInteger()", greaterThanOrEqualTo(1))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(multipleactionUrl, response);
    }
}
