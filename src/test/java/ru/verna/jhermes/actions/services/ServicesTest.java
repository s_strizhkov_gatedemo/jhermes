package ru.verna.jhermes.actions.services;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

/**
 * Тестирование непродающих сервисов типа проверки БСО.
 */
class ServicesTest {

    private static final String checkagentUrl = "checkAgent";
    private static final String checkbsoUrl = "checkBSO";
    private static final String coefbypromocodeUrl = "products/coefbypromocode";
    private static final String lostbsoUrl = "lostBSO";
    private static final String promocodeRequestDate = CommonPart.formatLocalDate(LocalDate.now());
    private static RequestSpecification spec;

    @BeforeAll
    static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder builder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_WWW);
        spec = builder.build();
    }

    @Test
    void correctCheckBSO() {
        String response = given().spec(spec)
                .param("bso_number", "0000004")
                .log().ifValidationFails(LogDetail.URI)
                .get(checkbsoUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.bso.size().toInteger()", greaterThanOrEqualTo(1))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(checkbsoUrl, response);
    }

    @Test
    void correctCheckBSOFail() {
        String response = given().spec(spec)
                .param("bso_number", "0000004123")
                .log().ifValidationFails(LogDetail.URI)
                .get(checkbsoUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.text", startsWith("В БД нет бланков"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(checkbsoUrl, response);
    }

    @Test
    void correctLostBSO() {
        String response = given().spec(spec)
                .param("year", 2017)
                .param("month", 1)
                .log().ifValidationFails(LogDetail.URI)
                .get(lostbsoUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.bso.size().toInteger()", greaterThanOrEqualTo(1))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(lostbsoUrl, response);
    }

    @Test
    void correctLostBSOFullYear() {
        given().spec(spec)
                .param("year", 2016)
                .param("month", 0)
                .log().ifValidationFails(LogDetail.URI)
                .get(lostbsoUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.bso.size().toInteger()", greaterThanOrEqualTo(1));
    }

    @Test
    void correctLostBSOWrongYear() {
        String response = given().spec(spec)
                .param("year", 1)
                .param("month", 0)
                .log().ifValidationFails(LogDetail.URI)
                .get(lostbsoUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.size().toInteger()", is(2))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(lostbsoUrl, response);
    }

    @Test
    void correctLostBSOWrongArgs() {
        given().spec(spec)
                .param("year", "qwe")
                .log().ifValidationFails(LogDetail.URI)
                .get(lostbsoUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.size().toInteger()", is(3));
    }

    @Test
    void correctLostBSOEmptyYear() {
        given().spec(spec)
                .param("year", 1901)
                .param("month", 0)
                .log().ifValidationFails(LogDetail.URI)
                .get(lostbsoUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.text", notNullValue());
    }

    @Test
    void correctLostBSONoMonth() {
        given().spec(spec)
                .param("year", 2017)
                .param("month", 0)
                .log().ifValidationFails(LogDetail.URI)
                .get(lostbsoUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.bso.size().toInteger()", greaterThanOrEqualTo(1));
    }

    @Test
    void checkAgentByNumber() {
        String response = given().spec(spec)
                .param("query", "200/17")
                .log().ifValidationFails(LogDetail.URI)
                .get(checkagentUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.name", notNullValue(),
                        "data.result.id", is("200/17-1"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(checkagentUrl, response);
    }

    @Test
    void checkAgentByName() {
        given().spec(spec)
                .param("query", "альфа-кре")
                .log().ifValidationFails(LogDetail.URI)
                .get(checkagentUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.name", notNullValue(),
                        "data.result.id", is("207/16-1"));
    }

    @Test
    void checkAgentManyResults() {
        String response = given().spec(spec)
                .param("query", "альфа")
                .log().ifValidationFails(LogDetail.URI)
                .get(checkagentUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.text", startsWith("Найдено "))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(checkagentUrl, response);
    }

    @Test
    void checkAgentNoResults() {
        given().spec(spec)
                .param("query", "no foundable query")
                .log().ifValidationFails(LogDetail.URI)
                .get(checkagentUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.text", startsWith("Совпадений не найдено"));
    }

    @Test
    void getCoefByPromocode() {
        String response = given().spec(spec)
                .param("productId", "ucVernaProductNSOnline")
                .param("promoCode", "TESTSALE90")
                .param("date", promocodeRequestDate)
                .log().ifValidationFails(LogDetail.URI)
                .get(coefbypromocodeUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.coeff.toDouble()", isA(double.class))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(coefbypromocodeUrl, response);
    }

    @Test
    void getCoefByNotFoundPromocode() {
        given().spec(spec)
                .param("productId", "ucVernaProductNSOnline")
                .param("promoCode", "XXXYYY")
                .param("date", promocodeRequestDate)
                .log().ifValidationFails(LogDetail.URI)
                .get(coefbypromocodeUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.coeff.toDouble()", is(1.0));
    }

    @Test
    void getCoefByPromocodeFail() {
        String response = given().spec(spec)
                .param("productId", "xxxYYY")
                .param("promoCode", "XXXYYY")
                .param("date", promocodeRequestDate)
                .log().ifValidationFails(LogDetail.URI)
                .get(coefbypromocodeUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.row.text", startsWith("Не найден страховой продукт"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(coefbypromocodeUrl, response);
    }
}
