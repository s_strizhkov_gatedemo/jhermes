package ru.verna.jhermes;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;

import javax.servlet.http.HttpServletResponse;

import java.util.Locale;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static ru.verna.commons.model.Constants.ERRORCODE.ACTION_IS_NOT_ALLOWED;

class LoginXmlTest {

    private static final String url = "/checkLogin";
    private static RequestSpecification spec;

    @BeforeAll
    static void setUpClass() {
        CommonPart.setServer();

        RequestSpecBuilder builder = CommonPart.getRequestSpecBuilder(null);
        builder.addParam("format", "xml");
        spec = builder.build();
    }

    /**
     * get c токеном правильного пользователя
     * <br>
     * user1111 может выполнять действие /checkLogin
     */
    @Test
    void correctUser() {
        given().spec(spec)
                .param("token", "user1111")
                .get(url)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.ok", is("ok")
                );
    }

    /**
     * post c токеном правильного пользователя
     * <br>
     * user1111 может выполнять действие /checkLogin
     */
    @Test
    void correctUserPost() {
        given().spec(spec)
                .param("token", "user1111")
                .post(url)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.ok", is("ok")
                );
    }

    /**
     * get c токеном правильного пользователя
     * <br>
     * user1111 может выполнять действие /checkLogin
     */
    @Test
    void wrongUser() {
        given().spec(spec)
                .param("token", "user2222")
                .get(url)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code.toInteger()", is(ACTION_IS_NOT_ALLOWED.code())
                );
    }

    /**
     * post c токеном правильного пользователя
     * <br>
     * user1111 может выполнять действие /checkLogin
     */
    @Test
    void wrongUserPost() {
        given().spec(spec)
                .param("token", "user2222")
                .get(url)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code.toInteger()", is(ACTION_IS_NOT_ALLOWED.code())
                );
    }

    /**
     * url в верхнем регистре
     */
    @Test
    void upperCaseUrl() {
        given().spec(spec)
                .param("token", "user1111")
                .get(url.toUpperCase(Locale.ENGLISH))
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.ok", is("ok")
                );
    }

    /**
     * url в верхнем регистре
     */
    @Test
    void lowerCaseUrl() {
        given().spec(spec)
                .param("token", "user1111")
                .get(url.toLowerCase(Locale.ENGLISH))
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.ok", is("ok")
                );
    }

    /**
     * url в разном регистре
     */
    @SuppressWarnings("Duplicates")
    @Test
    void differentCaseUrl() {
        int k = 0;
        char[] charArray = url.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            if ((k = 1 - k) == 0) {
                charArray[i] = Character.toUpperCase(charArray[i]);
            } else {
                charArray[i] = Character.toLowerCase(charArray[i]);
            }
        }
        String anUrl = String.valueOf(charArray);
        given().spec(spec)
                .param("token", "user1111")
                .log().ifValidationFails(LogDetail.URI)
                .get(anUrl)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.ok", is("ok")
                );
    }
}
