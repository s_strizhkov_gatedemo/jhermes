package ru.verna.jhermes.kiasclient;

import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.verna.commons.log.Log;
import ru.verna.commons.model.ActionParam;
import ru.verna.commons.model.ParamStructure;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 */
class RequestMakerTest {

    private String xmlHead = "";

    /**
     * Test of newRequest method, of class RequestMaker.
     */
    @Test
    void testNewRequest() {
        RequestMaker instance = RequestMaker.newRequest(123);
        String expResult = xmlHead
                + "<data><request>"
                + "<AppId>123</AppId>"
                + "<RequestIp/>"
                + "<reqName/>"
                + "<params></params>"
                + "</request></data>";
        String convertToString = instance.convertToString(false);
        Assertions.assertEquals(expResult, convertToString);
    }

    /**
     * Test of setRequestName method, of class RequestMaker.
     */
    @Test
    void testSetRequestName() {
        RequestMaker instance = RequestMaker.newRequest(123);

        String reqName = "вася";
        instance.setRequestName(reqName);

        String expResult = xmlHead
                + "<data><request>"
                + "<AppId>123</AppId>"
                + "<RequestIp/>"
                + "<reqName>вася</reqName>"
                + "<params></params>"
                + "</request></data>";
        String convertToString = instance.convertToString(false);
        Assertions.assertEquals(expResult, convertToString);
    }

    /**
     * Test of addParam method, of class RequestMaker.
     */
    @Test
    void testAddParam() {
        RequestMaker instance = RequestMaker.newRequest(123);

        ActionParam param = new ActionParam("insurer-doc-number", "insurer_doc_num", "123456");
        instance.addParam(param);

        String expResult = xmlHead
                + "<data><request>"
                + "<AppId>123</AppId>"
                + "<RequestIp/>"
                + "<reqName/>"
                + "<params><insurer_doc_num>123456</insurer_doc_num></params>"
                + "</request></data>";
        String convertToString = instance.convertToString(true);
        Assertions.assertEquals(expResult, convertToString);
    }

    /**
     * Test of addParam method, of class RequestMaker.
     */
    @Test
    void testAddParamNoFlatten() {
        RequestMaker instance = RequestMaker.newRequest(123);

        ActionParam param = new ActionParam("insurer-doc-number", "insurer_doc_num", "123456");
        instance.addParam(param);

        String expResult = xmlHead
                + "<data><request>"
                + "<AppId>123</AppId>"
                + "<RequestIp/>"
                + "<reqName/>"
                + "<params><insurer><doc><number>123456</number></doc></insurer></params>"
                + "</request></data>";
        String convertToString = instance.convertToString(false);
        Assertions.assertEquals(expResult, convertToString);
    }

    /**
     * Test of addParam method, of class RequestMaker.
     */
    @Test
    void testAddParam2() {
        RequestMaker instance = RequestMaker.newRequest(123);

        ActionParam param = new ActionParam("insurer-doc-number", "insurer_doc_num", "123456");
        instance.addParam(param);
        ActionParam param2 = new ActionParam("insurant", "insurant", "вася");
        instance.addParam(param2);
        ActionParam param3 = new ActionParam("insurer-doc-series", "insurer_doc_ser", "$#@!");
        instance.addParam(param3);

        String expResult = xmlHead
                + "<data><request>"
                + "<AppId>123</AppId>"
                + "<RequestIp/>"
                + "<reqName/>"
                + "<params>"
                + "<insurer_doc_num>123456</insurer_doc_num>"
                + "<insurer_doc_ser>$#@!</insurer_doc_ser>"
                + "<insurant>вася</insurant>"
                + "</params>"
                + "</request></data>";
        String convertToString = instance.convertToString(true);
        Assertions.assertEquals(expResult, convertToString);
    }

    /**
     * Test of addParam method, of class RequestMaker.
     */
    @Test
    void testAddParam3() {
        RequestMaker instance = RequestMaker.newRequest(123);

        ActionParam param = new ActionParam("insurer-doc-number", "insurer_doc_num", "123456");
        ActionParam param2 = new ActionParam("insurant", "insurant", "вася");
        instance.addParam(param).addParam(param2);

        String expResult = xmlHead
                + "<data><request>"
                + "<AppId>123</AppId>"
                + "<RequestIp/>"
                + "<reqName/>"
                + "<params>"
                + "<insurer_doc_num>123456</insurer_doc_num>"
                + "<insurant>вася</insurant>"
                + "</params>"
                + "</request></data>";
        String convertToString = instance.convertToString(true);
        Assertions.assertEquals(expResult, convertToString);

        ActionParam param3 = new ActionParam("insurer-doc-series", "insurer_doc_ser", "$#@!");
        instance.addParam(param3);
        ActionParam param4 = new ActionParam("insurer-doc-number", "insurer_doc_num", "фывапролджэ");
        instance.addParam(param4);

        expResult = xmlHead
                + "<data><request>"
                + "<AppId>123</AppId>"
                + "<RequestIp/>"
                + "<reqName/>"
                + "<params>"
                + "<insurer_doc_num>фывапролджэ</insurer_doc_num>"
                + "<insurer_doc_ser>$#@!</insurer_doc_ser>"
                + "<insurant>вася</insurant>"
                + "</params>"
                + "</request></data>";
        convertToString = instance.convertToString(true);
        Assertions.assertEquals(expResult, convertToString);

    }

    /**
     * Test of addParams method, of class RequestMaker.
     */
    @Test
    void testAddParams() {
        RequestMaker instance = RequestMaker.newRequest(123);

        Set<ActionParam> params = new LinkedHashSet<>();
        ActionParam param = new ActionParam("insurer-doc-number", "insurer_doc_num", "123456");
        params.add(param);
        ActionParam param2 = new ActionParam("insurant", "insurant", "вася");
        params.add(param2);
        ActionParam param3 = new ActionParam("insurer-doc-series", "insurer_doc_ser", "$#@!");
        params.add(param3);

        instance.addParams(params);

        String expResult = xmlHead
                + "<data><request>"
                + "<AppId>123</AppId>"
                + "<RequestIp/>"
                + "<reqName/>"
                + "<params>"
                + "<insurer_doc_num>123456</insurer_doc_num>"
                + "<insurer_doc_ser>$#@!</insurer_doc_ser>"
                + "<insurant>вася</insurant>"
                + "</params>"
                + "</request></data>";
        String convertToString = instance.convertToString(true);
        Assertions.assertEquals(expResult, convertToString);
    }

    private ParamStructure makeParamStructure() {
        String x = "{\n"
                + "    'insurer': [\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Краснодар',\n"
                + "                'street': 'Красная'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Москва',\n"
                + "                'street': 'Тверская'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Los Angeles',\n"
                + "                'street': 'Schwarzenegger avenue'\n"
                + "            }\n"
                + "        }\n"
                + "    ],"
                + "    solo: {\n"
                + "        param1: 'value1',\n"
                + "        param2: value2\n"
                + "    }\n"
                + "}";
        ParamStructure instance = new ParamStructure();
        try {
            instance.readJSON(x);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }

        String y = "{\n"
                + "    'doc': [\n"
                + "        {\n"
                + "            'type': 'Паспорт',\n"
                + "            'number': '123456'\n"
                + "        },\n"
                + "        {\n"
                + "            'type': 'Права',\n"
                + "            'number': '987654'\n"
                + "        },\n"
                + "    ]\n"
                + "}";
        ParamStructure addedInstance = new ParamStructure();
        try {
            addedInstance.readJSON(y);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }
        addedInstance.add(new ActionParam("doc-series", "doc_series", "1234"));
        addedInstance.add(new ActionParam("phone", "phone", "+7900-123-45-67"));
        instance.addStructure("insurer", addedInstance);
        return instance;
    }

    /**
     * Test of addParamStructure method, of class RequestMaker.
     */
    @Test
    void testAddParamStructure() {
        RequestMaker instance = RequestMaker.newRequest(123);

        instance.setRequestName("act!@#$%^*()");

        ParamStructure paramStructure = makeParamStructure();
        instance.addParamStructure(paramStructure);

        String expResult = xmlHead + "<data>"
                + "<request>"
                + "<AppId>123</AppId>"
                + "<RequestIp/>"
                + "<reqName>act!@#$%^*()</reqName>"
                + "<params>"
                + "<insurer>"
                + "<address><city>Краснодар</city><street>Красная</street></address>"
                + "<doc><type>Паспорт</type><number>123456</number><series>1234</series></doc>"
                + "<doc><type>Права</type><number>987654</number><series>1234</series></doc>"
                + "<phone>+7900-123-45-67</phone>"
                + "</insurer>"
                + "<insurer>"
                + "<address><city>Москва</city><street>Тверская</street></address>"
                + "<doc><type>Паспорт</type><number>123456</number><series>1234</series></doc>"
                + "<doc><type>Права</type><number>987654</number><series>1234</series></doc>"
                + "<phone>+7900-123-45-67</phone>"
                + "</insurer>"
                + "<insurer>"
                + "<address><city>Los Angeles</city><street>Schwarzenegger avenue</street></address>"
                + "<doc><type>Паспорт</type><number>123456</number><series>1234</series></doc>"
                + "<doc><type>Права</type><number>987654</number><series>1234</series></doc>"
                + "<phone>+7900-123-45-67</phone>"
                + "</insurer>"
                + "<solo><param1>value1</param1><param2>value2</param2></solo>"
                + "</params>"
                + "</request>"
                + "</data>";
        String convertToString = instance.convertToString(false);
        Assertions.assertEquals(expResult, convertToString);
    }

    /**
     * Test of clearParams method, of class RequestMaker.
     */
    @Test
    void testClearParams() {
        RequestMaker instance = RequestMaker.newRequest(123);

        Set<ActionParam> params = new LinkedHashSet<>();
        ActionParam param = new ActionParam("insurer-doc-number", "insurer_doc_num", "123456");
        params.add(param);
        ActionParam param2 = new ActionParam("insurant", "insurant", "вася");
        params.add(param2);
        ActionParam param3 = new ActionParam("insurer-doc-series", "insurer_doc_ser", "$#@!");
        params.add(param3);
        instance.addParams(params);

        String expResult = xmlHead
                + "<data><request>"
                + "<AppId>123</AppId>"
                + "<RequestIp/>"
                + "<reqName/>"
                + "<params>"
                + "<insurer_doc_num>123456</insurer_doc_num>"
                + "<insurer_doc_ser>$#@!</insurer_doc_ser>"
                + "<insurant>вася</insurant>"
                + "</params>"
                + "</request></data>";
        String convertToString = instance.convertToString(true);
        Assertions.assertEquals(expResult, convertToString);

        instance.clearParams();
        expResult = xmlHead
                + "<data><request>"
                + "<AppId>123</AppId>"
                + "<RequestIp/>"
                + "<reqName/>"
                + "<params></params>"
                + "</request>"
                + "</data>";
        convertToString = instance.convertToString(false);
        Assertions.assertEquals(expResult, convertToString);

        ActionParam param4 = new ActionParam("param4", "param4", "!\"№;%:");
        instance.addParam(param4);

        expResult = xmlHead
                + "<data><request>"
                + "<AppId>123</AppId>"
                + "<RequestIp/>"
                + "<reqName/>"
                + "<params>"
                + "<param4>!&quot;№;%:</param4>"
                + "</params>"
                + "</request>"
                + "</data>";
        convertToString = instance.convertToString(false);
        Assertions.assertEquals(expResult, convertToString);
    }

    /**
     */
    @Test
    void bigTest() {
        ActionParam param = new ActionParam("insurer-document-number", "insurer_doc_num", "123456");
        ActionParam param2 = new ActionParam("insurant", "insurant", "вася");
        ActionParam param3 = new ActionParam("insurer-document-series", "insurer_doc_ser", "$#@!");
        ActionParam param4 = new ActionParam("param4", "param4", "!№;%:");
        ActionParam param5 = new ActionParam("insurer-document-number", "insurer_doc_num",
                "new insurer document number");

        Set<ActionParam> params = new LinkedHashSet<>();
        params.add(param);
        params.add(param2);
        params.add(param3);

        RequestMaker instance = RequestMaker.newRequest(123)
                .addParams(params)
                .clearParams()
                .addParam(param5)
                .clearParams()
                .addParam(param5)
                .addParams(params)
                .addParam(param4);

        String expResult1 = xmlHead
                + "<data><request>"
                + "<AppId>123</AppId>"
                + "<RequestIp/>"
                + "<reqName/>"
                + "<params>"
                + "<insurer_doc_num>123456</insurer_doc_num>"
                + "<insurer_doc_ser>$#@!</insurer_doc_ser>"
                + "<insurant>вася</insurant>"
                + "<param4>!№;%:</param4>"
                + "</params>"
                + "</request></data>";
        String result1 = instance.convertToString(true);
        Assertions.assertEquals(expResult1, result1);

        String expResult2 = xmlHead
                + "<data><request>"
                + "<AppId>123</AppId>"
                + "<RequestIp/>"
                + "<reqName/>"
                + "<params>"
                + "<insurer><document><number>123456</number><series>$#@!</series></document></insurer>"
                + "<insurant>вася</insurant>"
                + "<param4>!№;%:</param4>"
                + "</params>"
                + "</request></data>";
        String result2 = instance.convertToString(false);
        Assertions.assertEquals(expResult2, result2);

        instance.addParam(param5);

        String expResult3 = xmlHead
                + "<data><request>"
                + "<AppId>123</AppId>"
                + "<RequestIp/>"
                + "<reqName/>"
                + "<params>"
                + "<insurer_doc_num>new insurer document number</insurer_doc_num>"
                + "<insurer_doc_ser>$#@!</insurer_doc_ser>"
                + "<insurant>вася</insurant>"
                + "<param4>!№;%:</param4>"
                + "</params>"
                + "</request></data>";
        String result3 = instance.convertToString(true);
        Assertions.assertEquals(expResult3, result3);
    }
}
