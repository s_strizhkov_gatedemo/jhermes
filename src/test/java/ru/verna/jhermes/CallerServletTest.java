package ru.verna.jhermes;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;
import javax.servlet.http.HttpServletResponse;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

class CallerServletTest {

    private static RequestSpecification spec;

    @BeforeAll
    static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder builder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_WWW);
        spec = builder.build();
    }

    @Disabled // Отключен, так как используется для быстрого вызова действия через CallerServlet и тестом не является
    @Test
    void coverageTerritory() {
        given().spec(spec)
                .param("AgrISN", "23228448")
                .log().ifValidationFails(LogDetail.URI)
                .get("caller/GETAGREEMENT")
                .then()
                .log().all()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.row.size().toInteger()", is(2))
                .extract().response().getBody().asString();
    }
}
