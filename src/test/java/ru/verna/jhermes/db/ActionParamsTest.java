package ru.verna.jhermes.db;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;
import ru.verna.commons.log.Log;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ActionParamsTest {

    private Connection connection;

    @BeforeEach
    void setUp() throws SQLException {
        connection = CommonPart.getConnection();
        List<String> sqls = new ArrayList<>();
        sqls.add("INSERT INTO actions (id, actiontype_id, descr, dependant, flatten_params) VALUES ('test-action', 'call', 'Тестовый экшен', 'TEST-DEPENDANT', FALSE)");
        sqls.add("INSERT INTO action_params (action_id, param_id, is_required) VALUES ('test-action', 'success_url', TRUE), ('test-action', 'failure_url', TRUE)");
        sqls.add("INSERT INTO roles_actions (role_id, action_id) VALUES (1, 'test-action'), (10, 'test-action')");
        sqls.add("INSERT INTO user_defined_params(role_id, user_id, action_id, param_id, value) VALUES(1, NULL , 'test-action', 'success_url', 'google.com'), (10, NULL , 'test-action', 'success_url', 'yandex.ru'), (10, NULL , 'test-action', 'failure_url', 'ya .ru')");
        try (Statement statement = connection.createStatement()) {
            sqls.forEach(s -> {
                try {
                    statement.addBatch(s);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            statement.executeBatch();
        }
    }

    @AfterEach
    void tearDown() throws SQLException {
        List<String> sqls = new ArrayList<>();
        sqls.add("DELETE FROM action_params WHERE action_id = 'test-action'");
        sqls.add("DELETE FROM user_defined_params WHERE action_id = 'test-action'");
        sqls.add("DELETE FROM roles_actions WHERE action_id = 'test-action'");
        sqls.add("DELETE FROM actions WHERE id = 'test-action'");
        try (Statement statement = connection.createStatement()) {
            sqls.forEach(s -> {
                try {
                    statement.addBatch(s);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            statement.executeBatch();
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Test
    void getUserActionParams() {
        String sql = "SELECT count(*) FROM get_user_action_params(?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, "test-action");
            statement.setInt(2, 1);
            ResultSet resultSet = statement.executeQuery();
            int rowCount = 0;
            if (resultSet.next()) {
                rowCount = resultSet.getInt(1);
            }
            Assertions.assertEquals(3, rowCount);
        } catch (SQLException ex) {
            Log.TEST.error(ex.getMessage());
        }
    }

    @Test
    void checkAndGetUserActionParamsFail() throws SQLException {
        String sql = "SELECT count(*) FROM check_and_get_user_action_params(?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, "test-action");
            statement.setInt(2, 1);
            assertThrows(SQLException.class, statement::executeQuery);
        }
    }

    @Test
    void checkAndGetUserActionParams() throws SQLException {
        String updateSql = "UPDATE user_defined_params SET role_id = 92 WHERE role_id = 10 AND action_id = 'test-action' AND param_id = 'success_url'";
        try (Statement statement = connection.createStatement()) {
            statement.addBatch(updateSql);
            statement.executeBatch();
        }
        String sql = "SELECT count(*) FROM check_and_get_user_action_params(?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, "test-action");
            statement.setInt(2, 1);
            ResultSet resultSet = statement.executeQuery();
            int rowCount = 0;
            if (resultSet.next()) {
                rowCount = resultSet.getInt(1);
            }
            Assertions.assertEquals(2, rowCount);
        }
    }
}
