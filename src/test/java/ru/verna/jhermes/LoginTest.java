package ru.verna.jhermes;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;

import javax.servlet.http.HttpServletResponse;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;
import static ru.verna.commons.model.Constants.ERRORCODE.ACTION_IS_NOT_ALLOWED;
import static ru.verna.commons.model.Constants.ERRORCODE.AUTH_TOKEN_NOT_FOUND;
import static ru.verna.commons.model.Constants.ERRORCODE.USER_NOT_FOUND;

/**
 *
 */
class LoginTest {

    private static final String url = "checklogin";
    private static final String checkUserUrl = "cabinet/checkUser";
    private static RequestSpecification spec;

    @BeforeAll
    static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder builder = CommonPart.getRequestSpecBuilder(null);
        spec = builder.build();
    }

    /**
     * get без токена авторизации
     */
    @Test
    void noUser() {
        given().spec(spec)
                .get(url)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code.toInteger()", equalTo(AUTH_TOKEN_NOT_FOUND.code()),
                        "data.error.text", equalTo("Токен авторизации не определен"));
    }

    /**
     * post без токена авторизации
     */
    @Test
    void noUserPost() {
        given().spec(spec)
                .post(url)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code.toInteger()", equalTo(AUTH_TOKEN_NOT_FOUND.code()),
                        "data.error.text", equalTo("Токен авторизации не определен"));
    }

    /**
     * get c несуществующим токеном в параметре xml
     */
    @Test
    void wrongUserInParam() {
        given().spec(spec)
                .param("token", "nosuchtoken")
                .get(url)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code.toInteger()", is(USER_NOT_FOUND.code()));
    }

    /**
     * post c несуществующим токеном в параметре xml
     */
    @Test
    void wrongUserInParamPost() {
        given().spec(spec)
                .param("token", "nosuchtoken")
                .post(url)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code.toInteger()", is(USER_NOT_FOUND.code()));
    }

    /**
     * get c несуществующим токеном в заголовке
     */
    @Test
    void wrongUserInHeader() {
        given().spec(spec)
                .header("Authorization", "Bearer nosuchtoken")
                .get(url)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code.toInteger()", is(USER_NOT_FOUND.code()));
    }

    /**
     * post c несуществующим токеном в заголовке
     */
    @Test
    void wrongUserInHeaderPost() {
        given().spec(spec)
                .header("Authorization", "Bearer nosuchtoken")
                .post(url)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code.toInteger()", is(USER_NOT_FOUND.code()));
    }

    /**
     * get c токеном пользователя с истекшим сроком годности
     */
    @Test
    void expiredUser() {
        given().spec(spec)
                .param("token", "expired_user_token")
                .get(url)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code.toInteger()", is(USER_NOT_FOUND.code()));
    }

    /**
     * post c токеном пользователя с истекшим сроком годности
     */
    @Test
    void expiredUserPost() {
        given().spec(spec)
                .param("token", "expired_user_token")
                .log().ifValidationFails(LogDetail.URI)
                .post(url)
                .then()
                .log().ifValidationFails(LogDetail.ALL)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code.toInteger()", is(USER_NOT_FOUND.code()));
    }

    /**
     * get c токеном неактивного пользователя
     */
    @Test
    void notActiveUser() {
        given().spec(spec)
                .param("token", "expired_user_token")
                .get(url)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code.toInteger()", is(USER_NOT_FOUND.code()));
    }

    /**
     * post c токеном неактивного пользователя
     */
    @Test
    void notActiveUserPost() {
        given().spec(spec)
                .param("token", "expired_user_token")
                .post(url)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code.toInteger()", is(USER_NOT_FOUND.code()));
    }

    /**
     * get c токеном правильного пользователя
     */
    @Test
    void correctUser() {
        given().spec(spec)
                .param("token", "user1111")
                .get(url)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.ok", is("ok"));
    }

    /**
     * post c токеном правильного пользователя
     */
    @Test
    void correctUserPost() {
        given().spec(spec)
                .param("token", "user1111")
                .post(url)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.ok", is("ok"));
    }

    /**
     * get c токеном неправильного пользователя
     * <br>
     * user2222 не может выполнять действие /checkLogin
     */
    @Test
    void wrongUser() {
        given().spec(spec)
                .param("token", "user2222")
                .get(url)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code.toInteger()", is(ACTION_IS_NOT_ALLOWED.code())
                );
    }

    /**
     * post c токеном неправильного пользователя
     * <br>
     * user2222 не может выполнять действие /checkLogin
     */
    @Test
    void wrongUserPost() {
        given().spec(spec)
                .param("token", "user2222")
                .post(url)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code.toInteger()", is(ACTION_IS_NOT_ALLOWED.code())
                );
    }

    /**
     * url в верхнем регистре
     */
    @Test
    void upperCaseUrl() {
        given().spec(spec)
                .param("token", "user1111")
                .get(url.toUpperCase())
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.ok", is("ok")
                );
    }

    /**
     * url в нижнем регистре
     */
    @Test
    void lowerCaseUrl() {
        given().spec(spec)
                .param("token", "user1111")
                .get(url.toLowerCase())
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.ok", is("ok")
                );
    }

    /**
     * url в разном регистре
     */
    @Test
    void differentCaseUrl() {
        int k = 0;
        char[] charArray = url.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            if ((k = 1 - k) == 0) {
                charArray[i] = Character.toUpperCase(charArray[i]);
            } else {
                charArray[i] = Character.toLowerCase(charArray[i]);
            }
        }
        String anUrl = String.valueOf(charArray);
        given().spec(spec)
                .param("token", "user1111")
                .log().ifValidationFails(LogDetail.URI)
                .get(anUrl)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.ok", is("ok")
                );
    }

    /**
     * Проверка c именем и паролем правильного пользователя кабинета
     */
    @Test
    void correctCabinetUser() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .param("Name", "hermes")
                .param("Pwd", "37281168756c06be6829e70e76736342e4e3d26e47bebabfd7deab780ad7af99f2fce061080c6f650cc04261cf5c5488c91ffbd53ac93c79508c99979856c36b")
                .param("token", CommonPart.TOKEN_CABINET_USER)
                .post(checkUserUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.result.Sid", not(isEmptyOrNullString()),
                        "data.result.UserDetails.ISN", is("31757895"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(checkUserUrl, response);
    }

    /**
     * Проверка c именем и паролем неправильного пользователя кабинета
     */
    @Test
    void uncorrectedCabinetUser() {
        String response = given().spec(spec)
                .log().ifValidationFails(LogDetail.URI)
                .param("Name", "cabinet")
                .param("Pwd", "123")
                .param("token", CommonPart.TOKEN_CABINET_USER)
                .post(checkUserUrl)
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.error.code", is("ORA-20001"))
                .extract().response().getBody().asString();
        CommonPart.writeToDb(checkUserUrl, response);
    }
}
