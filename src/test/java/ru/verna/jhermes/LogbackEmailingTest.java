package ru.verna.jhermes;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import ru.verna.CommonPart;
import javax.servlet.http.HttpServletResponse;
import static io.restassured.RestAssured.given;

class LogbackEmailingTest {
    private static RequestSpecification spec;

    @BeforeAll
    static void setUpClass() {
        CommonPart.setServer();
        RequestSpecBuilder builder = CommonPart.getRequestSpecBuilder(CommonPart.TOKEN_USER_WWW);
        spec = builder.build();
    }

    /**
     * Вызываем (любое) действие шлюза с неправильным json-параметром params,
     * шлюз логирует ошибку (сообщение с уровнем error),
     * logback согласно своим настройкам присылает журнальное сообщение на почту.
     */
    @Disabled("чтоб не получать лишние письма")
    @Test
    void raiseErrorByWrongJson() {
        given().spec(spec)
                .param("params", "{'broken_json:")
                .log().ifValidationFails(LogDetail.URI)
                .get("catalog/list")
                .then()
                .log().ifValidationFails(LogDetail.BODY)
                .statusCode(HttpServletResponse.SC_OK);
    }
}
