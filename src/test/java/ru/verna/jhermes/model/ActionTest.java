package ru.verna.jhermes.model;

import static org.junit.Assert.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Тестирование класса {@link Action}.
 */
class ActionTest {

    /**
     * {@link Action#getLogsize()} при пустом {@link #logsize} возвращает 0.
     */
    @Test
    void testGetLogsize() {
        Action instance = new Action();
        Integer expResult = 0;

        Integer result = instance.getLogsize();
        Assertions.assertEquals(expResult, result);

        int x = instance.getLogsize();
        Assertions.assertEquals(0, x);
    }

    /**
     * {@link Action#isLogsizeNull()} при пустом {@link #logsize} возвращает true.
     */
    @Test
    void testIsLogsizeNull() {
        Action instance = new Action();
        boolean x = instance.isLogsizeNull();
        Assertions.assertTrue(x);

        instance.setLogsize(0);
        boolean y = instance.isLogsizeNull();
        Assertions.assertFalse(y);
    }
}
