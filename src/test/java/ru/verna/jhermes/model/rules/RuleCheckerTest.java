package ru.verna.jhermes.model.rules;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static ru.verna.commons.model.Constants.ERRORCODE.RULE_VIOLATION;

/**
 *
 * @author StrizhkovSI
 */
class RuleCheckerTest {

    private String json = "{\n" + "  'coverage': {\n" + "    'risks': {\n" + "      'risk': [\n" + "        {\n"
            + "          'id': 'ucRiskNSDisablement123',\n" + "          'value': 300000\n" + "        },\n"
            + "        {\n" + "          'id': 'ucRiskNSDeath',\n" + "          'value': 180000\n" + "        },\n"
            + "        {\n" + "          'id': 'ucRiskNSTrauma',\n" + "          'value': 180000\n" + "        }\n"
            + "      ]\n" + "    }\n" + "  },\n" + "  'term': {\n" + "    'end': '13.07.2018',\n"
            + "    'begin': '31.07.2018'\n" + "  },\n" + "  'attributes': {\n" + "    'attribute': [\n" + "      {\n"
            + "        'id': 'ucAddAtrNSCoveragePeriod',\n" + "        'value': '1329051'\n" + "      },\n"
            + "      {\n" + "        'id': 'ucAddAtrNSCoverageArea',\n" + "        'value': '1326811'\n" + "      },\n"
            + "      {\n" + "        'id': 'ucAddAtrNSKindOfSport',\n" + "        'value': '1329061'\n" + "      },\n"
            + "      {\n" + "        'id': 'ucAddAtrNSOccupation',\n" + "        'value': '1328091'\n" + "      }\n"
            + "    ]\n" + "  },\n" + "  'insurants': {\n" + "    'insurant': [6, 9]\n" + "  }\n" + "}";
    RuleChecker ruleChecker = null;

    @BeforeEach
    void setUp() {
        ruleChecker = new RuleChecker(json);
    }

    /**
     * Проверяет массив на допустимые значения
     */
    @Test
    void wrongJsonpath() {
        Rule rule = new Rule();
        rule.setJsonpath("$.coverage.risks.risk[*].value");
        rule.setAsList(true);
        rule.setMatcher("hasItems( [$1, $2] as Integer[] )");
        rule.setMatcherArg1("$.coverage.risks.risk[-1:].value");
        rule.setMatcherArg2("400000");
        rule.setErrorMsg(
                "В суммах страхования рисков должны присутствовать значения $1 и 400000, а заданные суммы $value");
        RuleError result = ruleChecker.check(rule);

        String resultText = result.getText();
        String expected = "В суммах страхования рисков должны присутствовать значения 180000 и 400000, а заданные суммы [300000,180000,180000]";
        Assertions.assertEquals(expected, resultText);

        int code = result.getCode();
        int expectedCode = RULE_VIOLATION.code();
        Assertions.assertEquals(expectedCode, code);
    }

    /**
     * Test of check method, of class RuleChecker.
     */
    @Test
    void greaterThan() {
        Rule rule = new Rule();
        rule.setJsonpath("$.coverage.risks.risk[?(@.id == 'ucRiskNSDisablement123')].value");
        rule.setAsList(false);
        rule.setMatcher("greaterThan($1)");
        String matcherArg1 = "1000000";
        rule.setMatcherArg1(matcherArg1);
        rule.setMatcherArg2("");
        rule.setErrorMsg("Сумма страхования риска должна быть больше $1, а она равна $value");

        String expected = "Сумма страхования риска должна быть больше " + matcherArg1 + ", а она равна 300000";
        RuleError result = ruleChecker.check(rule);
        String resultText = result.getText();
        Assertions.assertEquals(expected, resultText);
    }

    /**
     * Test of check method, of class RuleChecker.
     */
    @Test
    void lessThan() {
        Rule rule = new Rule();
        rule.setJsonpath("$.coverage.risks.risk[?(@.id == 'ucRiskNSDisablement123')].value");
        rule.setAsList(false);
        rule.setMatcher("lessThan($1)");
        String matcherArg1 = "10000";
        rule.setMatcherArg1(matcherArg1);
        rule.setMatcherArg2(null);
        rule.setErrorMsg("Сумма страхования риска должна быть меньше $1, а она равна $value");

        String expected = "Сумма страхования риска должна быть меньше " + matcherArg1 + ", а она равна 300000";
        RuleError result = ruleChecker.check(rule);
        String resultText = result.getText();
        Assertions.assertEquals(expected, resultText);
    }

    /**
     * В одной проверке два правила.
     */
    @Test
    void allOf() {
        Rule rule = new Rule();
        rule.setJsonpath("$.coverage.risks.risk[?(@.id == 'ucRiskNSDisablement123')].value");
        rule.setAsList(false);
        rule.setMatcher("allOf( [ lessThan($1), greaterThan($2) ] )");
        String matcherArg1 = "10000";
        String matcherArg2 = "1000000";
        rule.setMatcherArg1(matcherArg1);
        rule.setMatcherArg2(matcherArg2);
        rule.setErrorMsg("Сумма страхования риска должна быть меньше $1 и больше $2, а она равна $value");

        String expected = "Сумма страхования риска должна быть меньше " + matcherArg1 + " и больше " + matcherArg2
                + ", а она равна 300000";
        RuleError result = ruleChecker.check(rule);
        String resultText = result.getText();
        Assertions.assertEquals(expected, resultText);
    }

    /**
     */
    @Test
    void hasItemsIntegers() {
        Rule rule = new Rule();
        rule.setJsonpath("$.coverage.risks.risk[*].value");
        rule.setAsList(true);
        rule.setMatcher("hasItems( [300000, 180000] as Integer[])");
        rule.setMatcherArg1(null);
        rule.setMatcherArg2(null);
        rule.setErrorMsg(
                "В суммах страхования рисков должны присутствовать значения 180000 и 300000, а текущие суммы $value");

        RuleError result = ruleChecker.check(rule);
        String resultText = result.getText();

        String expected = "";
        Assertions.assertEquals(expected, resultText);
    }

    /**
     * Проверяет массив на допустимые значения
     */
    @Test
    void hasItemsIntegersWrong() {
        Rule rule = new Rule();
        rule.setJsonpath("$.coverage.risks.risk[*].value");
        rule.setAsList(true);
        rule.setMatcher("hasItems( [$1, $2] as Integer[] )");
        rule.setMatcherArg1("$.coverage.risks.risk[-1:].value");
        rule.setMatcherArg2("400000");
        rule.setErrorMsg("Разрешенные суммы страхования рисков: $1 и 400000, а заданные суммы $value");

        RuleError result = ruleChecker.check(rule);
        String resultText = result.getText();
        String expected = "Разрешенные суммы страхования рисков: 180000 и 400000, а заданные суммы [300000,180000,180000]";
        Assertions.assertEquals(expected, resultText);

        int code = result.getCode();
        int expectedCode = RULE_VIOLATION.code();
        Assertions.assertEquals(expectedCode, code);
    }

    /**
     */
    @Test
    void hasItemsStrings() {
        Rule rule = new Rule();
        rule.setJsonpath("$.coverage.risks.risk[?(@.value > 0)].id");
        rule.setAsList(true);
        rule.setMatcher("hasItems( ['ucRiskNSDeath', 'ucRiskNSDisablement123'] as String[])");
        rule.setMatcherArg1(null);
        rule.setMatcherArg2(null);
        rule.setErrorMsg(
                "В рисках должны присутствовать значения 'ucRiskNSDeath' и 'ucRiskNSDisablement123', а текущие риски $value");

        RuleError result = ruleChecker.check(rule);
        String resultText = result.getText();

        String expected = "";
        Assertions.assertEquals(expected, resultText);
    }

    /**
     */
    @Test
    void hasItemsStringsWrong() {
        Rule rule = new Rule();
        rule.setJsonpath("$.coverage.risks.risk[?(@.value > 0)].id");
        rule.setAsList(true);
        rule.setMatcher("hasItems( ['ucRiskNSDEATH', 'ucRiskNSDisablement123'] as String[])");
        rule.setMatcherArg1(null);
        rule.setMatcherArg2(null);
        rule.setErrorMsg(
                "В рисках должны присутствовать значения 'ucRiskNSDEATH' и 'ucRiskNSDisablement123', а текущие риски $value");

        RuleError result = ruleChecker.check(rule);
        String resultText = result.getText();

        String expected = "В рисках должны присутствовать значения 'ucRiskNSDEATH' и 'ucRiskNSDisablement123', а текущие риски [\"ucRiskNSDisablement123\",\"ucRiskNSDeath\",\"ucRiskNSTrauma\"]";
        Assertions.assertEquals(expected, resultText);
    }
}
