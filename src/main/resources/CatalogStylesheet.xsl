<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml"/>
    <xsl:template match="/">
        <data>
            <xsl:if test="data/result/row">
                <result>
                    <xsl:for-each select="data/result/row">
                        <row>
                            <ISN>
                                <xsl:value-of select="ISN"/>
                            </ISN>
                            <FullName>
                                <xsl:value-of select="FullName"/>
                            </FullName>
                            <UserConstName>
                                <xsl:value-of select="UserConstName"/>
                            </UserConstName>
                            <xsl:if test="isMicrostate">
                                <isMicrostate>
                                    <xsl:value-of select="isMicrostate"/>
                                </isMicrostate>
                            </xsl:if>
                        </row>
                    </xsl:for-each>
                </result>
            </xsl:if>
            <xsl:if test="data/error/row">
                <error>
                    <xsl:for-each select="data/error/row">
                        <row>
                            <attr_name>
                                <xsl:value-of select="attr_name"/>
                            </attr_name>
                            <code>
                                <xsl:value-of select="code"/>
                            </code>
                            <text>
                                <xsl:value-of select="text"/>
                            </text>
                        </row>
                    </xsl:for-each>
                </error>
            </xsl:if>
        </data>
    </xsl:template>
</xsl:stylesheet>
