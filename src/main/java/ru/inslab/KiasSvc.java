/**
 * KiasSvc.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.inslab;

public interface KiasSvc extends javax.xml.rpc.Service {
    public java.lang.String getkiasSvcSoapAddress();

    public ru.inslab.KiasSvcSoap getkiasSvcSoap() throws javax.xml.rpc.ServiceException;

    public ru.inslab.KiasSvcSoap getkiasSvcSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
