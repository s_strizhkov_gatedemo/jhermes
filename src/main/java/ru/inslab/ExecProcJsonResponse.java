/**
 * ExecProcJsonResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package ru.inslab;

@SuppressWarnings("rawtypes")
public class ExecProcJsonResponse implements java.io.Serializable {
    static final long serialVersionUID = 1L;
    private java.lang.String execProcJsonResult;

    public ExecProcJsonResponse() {
    }

    public ExecProcJsonResponse(
            java.lang.String execProcJsonResult) {
        this.execProcJsonResult = execProcJsonResult;
    }

    /**
     * Gets the execProcJsonResult value for this ExecProcJsonResponse.
     *
     * @return execProcJsonResult
     */
    public java.lang.String getExecProcJsonResult() {
        return execProcJsonResult;
    }

    /**
     * Sets the execProcJsonResult value for this ExecProcJsonResponse.
     *
     * @param execProcJsonResult
     */
    public void setExecProcJsonResult(java.lang.String execProcJsonResult) {
        this.execProcJsonResult = execProcJsonResult;
    }

    private java.lang.Object __equalsCalc = null;

    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ExecProcJsonResponse)) {
            return false;
        }
        ExecProcJsonResponse other = (ExecProcJsonResponse) obj;
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true
                && ((this.execProcJsonResult == null && other.getExecProcJsonResult() == null)
                        || (this.execProcJsonResult != null
                                && this.execProcJsonResult.equals(other.getExecProcJsonResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;

    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getExecProcJsonResult() != null) {
            _hashCode += getExecProcJsonResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
            ExecProcJsonResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://inslab.ru/", ">ExecProcJsonResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("execProcJsonResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://inslab.ru/", "ExecProcJsonResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
            java.lang.String mechType,
            java.lang.Class _javaType,
            javax.xml.namespace.QName _xmlType) {
        return new org.apache.axis.encoding.ser.BeanSerializer(
                _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
            java.lang.String mechType,
            java.lang.Class _javaType,
            javax.xml.namespace.QName _xmlType) {
        return new org.apache.axis.encoding.ser.BeanDeserializer(
                _javaType, _xmlType, typeDesc);
    }

}
