/**
 * KiasSvcSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.inslab;

public interface KiasSvcSoap extends java.rmi.Remote {
    public ru.inslab.ExecProcResponseExecProcResult execProc(java.lang.String pData) throws java.rmi.RemoteException;
    public java.lang.String execProcJson(java.lang.String pData) throws java.rmi.RemoteException;
}
