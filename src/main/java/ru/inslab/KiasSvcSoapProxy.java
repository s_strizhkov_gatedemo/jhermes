package ru.inslab;

public class KiasSvcSoapProxy implements ru.inslab.KiasSvcSoap {
    private String _endpoint = null;
    private ru.inslab.KiasSvcSoap kiasSvcSoap = null;

    public KiasSvcSoapProxy() {
        _initKiasSvcSoapProxy();
    }

    public KiasSvcSoapProxy(String endpoint) {
        _endpoint = endpoint;
        _initKiasSvcSoapProxy();
    }

    private void _initKiasSvcSoapProxy() {
        try {
            //TODO тут точно нужен new KiasSvcLocator? Или использовать один раз созданный KiasSvcLocator?
            kiasSvcSoap = (new ru.inslab.KiasSvcLocator()).getkiasSvcSoap();
            if (kiasSvcSoap != null) {
                if (_endpoint != null) {
                    ((javax.xml.rpc.Stub) kiasSvcSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
                } else {
                    _endpoint = (String) ((javax.xml.rpc.Stub) kiasSvcSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
                }
            }

        } catch (javax.xml.rpc.ServiceException serviceException) {
        }
    }

    public String getEndpoint() {
        return _endpoint;
    }

    public void setEndpoint(String endpoint) {
        _endpoint = endpoint;
        if (kiasSvcSoap != null) {
            ((javax.xml.rpc.Stub) kiasSvcSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        }

    }

    public ru.inslab.KiasSvcSoap getKiasSvcSoap() {
        if (kiasSvcSoap == null) {
            _initKiasSvcSoapProxy();
        }
        return kiasSvcSoap;
    }

    public ru.inslab.ExecProcResponseExecProcResult execProc(java.lang.String pData)
            throws java.rmi.RemoteException {
        if (kiasSvcSoap == null) {
            _initKiasSvcSoapProxy();
        }
        return kiasSvcSoap.execProc(pData);
    }

    public java.lang.String execProcJson(java.lang.String pData) throws java.rmi.RemoteException {
        if (kiasSvcSoap == null) {
            _initKiasSvcSoapProxy();
        }
        return kiasSvcSoap.execProcJson(pData);
    }

}
