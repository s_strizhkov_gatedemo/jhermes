/**
 * KiasSvcLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package ru.inslab;

import ru.verna.jhermes.Util;

@SuppressWarnings("rawtypes")
public class KiasSvcLocator extends org.apache.axis.client.Service implements ru.inslab.KiasSvc {
    static final long serialVersionUID = 1L;

    public KiasSvcLocator() {
    }

    public KiasSvcLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public KiasSvcLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName)
            throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    /**
     * Возвращает адрес веб-службы KiasSvc.
     *
     * @return
     */
    @Override
    public java.lang.String getkiasSvcSoapAddress() {
        return Util.getKiasSvcEndpoint();
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String kiasSvcSoapWSDDServiceName = "kiasSvcSoap";

    private java.lang.String getkiasSvcSoapWSDDServiceName() {
        return kiasSvcSoapWSDDServiceName;
    }

    @Override
    public ru.inslab.KiasSvcSoap getkiasSvcSoap() throws javax.xml.rpc.ServiceException {
        java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(getkiasSvcSoapAddress());
        } catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getkiasSvcSoap(endpoint);
    }

    @Override
    public ru.inslab.KiasSvcSoap getkiasSvcSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ru.inslab.KiasSvcSoapStub _stub = new ru.inslab.KiasSvcSoapStub(portAddress, this);
            _stub.setPortName(getkiasSvcSoapWSDDServiceName());
            return _stub;
        } catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    /**
     * For the given interface, get the stub implementation. If this service has no port for the given interface, then
     * ServiceException is thrown.
     *
     * @param serviceEndpointInterface
     * @return
     * @throws javax.xml.rpc.ServiceException
     */
    @Override
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ru.inslab.KiasSvcSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                ru.inslab.KiasSvcSoapStub _stub = new ru.inslab.KiasSvcSoapStub(
                        new java.net.URL(getkiasSvcSoapAddress()), this);
                _stub.setPortName(getkiasSvcSoapWSDDServiceName());
                return _stub;
            }
        } catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  "
                + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation. If this service has no port for the given interface, then
     * ServiceException is thrown.
     *
     * @param portName
     * @param serviceEndpointInterface
     * @return
     * @throws javax.xml.rpc.ServiceException
     */
    @Override
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface)
            throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("kiasSvcSoap".equals(inputPortName)) {
            return getkiasSvcSoap();
        } else {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    @Override
    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://inslab.ru/", "kiasSvc");
    }

    private java.util.HashSet ports = null;

    @Override
    @SuppressWarnings("unchecked")
    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://inslab.ru/", "kiasSvcSoap"));
        }
        return ports.iterator();
    }

}
