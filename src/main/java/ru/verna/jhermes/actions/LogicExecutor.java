package ru.verna.jhermes.actions;

import org.slf4j.Logger;
import ru.verna.commons.log.Log;
import ru.verna.commons.Utils;
import ru.verna.jhermes.actions.logic.ILogic;
import ru.verna.jhermes.model.Action;
import ru.verna.commons.model.ParamStructure;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Выполняет действие типа logic.
 */
public class LogicExecutor implements Executor {

    @Override
    public String execute(HttpServletRequest request, Action action, ParamStructure parameters) {
        Logger mainLogger = Log.getLogger("main");

        String logicClassname = action.getDependant();
        if (mainLogger.isTraceEnabled()) {
            mainLogger.trace("запущен для вызова logic-класса {} с параметрами {}", logicClassname, parameters);
        }
        try {
            @SuppressWarnings("unchecked")
            Class<ILogic> logicClass = (Class<ILogic>) Class.forName(logicClassname);
            Constructor<ILogic> constructor = logicClass.getConstructor();

            ILogic logicInstance = constructor.newInstance();
            mainLogger.debug("создан новый экземпляр {} ({}), запускается с параметрами {}", logicClassname,
                    logicInstance, parameters);
            request.setAttribute("actions", action.getId());
            String executeResult = logicInstance.execute(request, parameters);
            return executeResult;

        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException
                | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            long ticket = Utils.getRandomLong(4);
            mainLogger.error("Инцидент " + ticket, e);
            return Utils.getErrorMessage(198, "Невозможно выполнить требуемое действие: "
                    + e.getClass().getSimpleName()
                    + ". Пожалуйста, сообщите администратору системы номер инцидента: " + ticket);
        }
    }
}
