package ru.verna.jhermes.actions.logic;

import javax.servlet.http.HttpServletRequest;
import ru.verna.commons.model.ParamStructure;

/**
 * Тестовое logic-действие.
 */
public class TestParams implements ILogic {
    @Override
    public String execute(HttpServletRequest request, ParamStructure parameters) {
        return "<data><result><ok>ok</ok></result></data>";
    }

}
