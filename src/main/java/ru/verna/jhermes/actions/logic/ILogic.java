package ru.verna.jhermes.actions.logic;

import javax.servlet.http.HttpServletRequest;
import ru.verna.commons.model.ParamStructure;

/**
 * Интерфейс действий типа 'logic'.
 */
public interface ILogic {

    /**
     * Действие типа logic.
     *
     * @param request http-запрос, где лежит вся входящая информация
     * @param parameters параметры выполнения действия, сформированные из пришедших в request'е параметров
     * @return строковый результат выполнения действия
     */
    String execute(HttpServletRequest request, ParamStructure parameters);
}
