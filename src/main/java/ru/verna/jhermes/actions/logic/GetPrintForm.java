package ru.verna.jhermes.actions.logic;

import org.slf4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import ru.verna.commons.Utils;
import ru.verna.commons.XmlUtils;
import ru.verna.commons.log.Log;
import ru.verna.commons.model.Constants;
import ru.verna.commons.model.ParamStructure;
import ru.verna.jhermes.actions.ActionExecutor;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Действие по получению печатной формы привязанной к объекту учета КИАС.
 */
public class GetPrintForm implements ILogic {

    @Override
    public String execute(HttpServletRequest request, ParamStructure parameters) {
        Logger mainLogger = Log.getLogger("main");
        ActionExecutor getPrintForm = new ActionExecutor();
        String gateResponse = getPrintForm.execute(request, "getPrintableDocument", parameters);
        mainLogger.trace("получили данные договора: {}", gateResponse);
        try {
            Document paymentDataAsXmlDoc = XmlUtils.parseToXml(gateResponse);
            Node dataNode = XmlUtils.getChildByName(paymentDataAsXmlDoc, "Document", false);
            if (dataNode == null) {
                return gateResponse;
            }
            // если gateResponse вернул ошибки, то выкидываем их наверх
            Node errorNode = XmlUtils.getChildByName(dataNode, "Error", false);
            if (errorNode != null) {
                return Utils.getErrorMessage(Constants.ERRORCODE.KIAS_WEB_METHOD_ERROR.code(), errorNode.getTextContent());
            }
        } catch (SAXException | IOException e) {
            mainLogger.error(null, e);
            return Utils.getErrorMessage(Constants.ERRORCODE.KIAS_WEB_METHOD_ERROR.code(),
                    e.getLocalizedMessage() + ". Пожалуйста, обратитесь к администратору системы.");
        }
        return gateResponse;
    }
}
