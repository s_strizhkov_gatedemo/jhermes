package ru.verna.jhermes.actions.logic;

import org.slf4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import ru.verna.commons.log.Log;
import ru.verna.commons.Utils;
import ru.verna.commons.XmlUtils;
import ru.verna.jhermes.actions.ActionExecutor;
import ru.verna.commons.model.ParamStructure;
import javax.servlet.http.HttpServletRequest;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import static ru.verna.commons.model.Constants.ERRORCODE.DB_ERROR;

/**
 *
 */
public class GetCountryByName implements ILogic {

    @Override
    public String execute(HttpServletRequest request, ParamStructure parameters) {
        Logger mainLogger = Log.getLogger("main");

        try {
            ActionExecutor actionExecutor = new ActionExecutor();
            String countryListString = actionExecutor.execute(request, "getCountryList", null);

            Document dataAsXml = XmlUtils.parseToXml(countryListString);

            String dataErrorCode = null;
            try {
                dataErrorCode = XmlUtils.dataErrorCodeXpath.evaluate(dataAsXml);
            } catch (XPathExpressionException e) {
                mainLogger.error(null, e);
            }

            if (Utils.notEmpty(dataErrorCode)) {
                // если в ответе ошибка, возвращаем просто ответ вызова сервиса
                //TODO Лучше здесь не выдавать ответ внутренней веб-службы, а генерировать уникальный тикет,
                //журналировать его для последующих разбирательств и выдавать клиенту.
                return countryListString;

            } else {
                String query = request.getParameter("query");
                if (query == null) {
                    query = "";
                }
                query = query.toLowerCase();

                Node dataResultNode = (Node) XmlUtils.dataResultXpath.evaluate(dataAsXml, XPathConstants.NODE);
                Node rowsetNode = XmlUtils.getChildByName(dataResultNode, "ROWSET", false);
                Node rowNode = rowsetNode.getFirstChild();
                while (rowNode != null) {
                    Node nextSibling = rowNode.getNextSibling();

                    Node fullNameNode = XmlUtils.getChildByName(rowNode, "FullName", false);
                    String fullName = fullNameNode.getTextContent();
                    if (!fullName.toLowerCase().contains(query)) {
                        rowsetNode.removeChild(rowNode);
                    }

                    rowNode = nextSibling;
                }
                String result = XmlUtils.convertToString(dataAsXml);
                return result;
            }
        } catch (SAXException | XPathExpressionException | IOException e) {
            mainLogger.error(null, e);
            String result = Utils.getErrorMessage(DB_ERROR.code(), e.getLocalizedMessage() + ". Пожалуйста, обратитесь к администратору системы.");
            mainLogger.debug("ответ: {}", result);
            return result;
        }
    }
}
