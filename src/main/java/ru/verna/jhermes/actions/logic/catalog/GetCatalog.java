package ru.verna.jhermes.actions.logic.catalog;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import ru.verna.commons.Utils;
import ru.verna.commons.XmlUtils;
import ru.verna.commons.log.Log;
import ru.verna.commons.model.ParamStructure;
import ru.verna.jhermes.actions.ActionExecutor;
import ru.verna.jhermes.actions.logic.ILogic;

import javax.servlet.http.HttpServletRequest;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Действие для трансформации ответа КИАС на запрос получения справочника, путем XSLT трансформации отрезает ненужные поля.
 */
public class GetCatalog implements ILogic {

    @Override
    public String execute(HttpServletRequest request, ParamStructure parameters) {
        Document responseAsXml = executeAction(request, parameters);
        String dataErrorCode = getErrorCode(responseAsXml);
        Document result = Utils.notEmpty(dataErrorCode) ? responseAsXml : transformKiasResponse(responseAsXml);
        return XmlUtils.convertToString(result);
    }

    /**
     * Вызывает действие для получения данных справочника.
     *
     * @param request    входящий запрос
     * @param parameters параметры запроса
     * @return преобразованый ответ КИАСа
     */
    private Document executeAction(HttpServletRequest request, ParamStructure parameters) {
        String kiasResponse = new ActionExecutor().execute(request, getActionId(request), parameters);
        Document result = null;
        try {
            result = XmlUtils.parseToXml(kiasResponse);
        } catch (SAXException | IOException e) {
            Log.getLogger("main").error("Ошибка при обработке ответа из КИАС, {}", e.getLocalizedMessage());
        }
        return result;
    }

    /**
     * Возвращает имя нужного действия, так как запрос справочника стран Шенгена вызывается отдельным действием, неоходима дополнительная проверка.
     *
     * @param request входящий запрос
     * @return имя действия, если запрос справочника стран Шенгена то "catalog/schengen" иначе "catalog/list"
     */
    private String getActionId(HttpServletRequest request) {
        return isSchengen(request) ? "catalog/schengen" : "catalog/list";
    }

    /**
     * Проверяет является ли входящий запрос запросом на получение справочника стран Шенгена.
     *
     * @param request входящий запрос
     * @return {@code true} если запрашивается справочник стран Шенгена иначе {@code false}
     */
    private boolean isSchengen(HttpServletRequest request) {
        String schengen = "schengen";
        return request.getRequestURI().contains(schengen) || ((String) request.getAttribute("actions")).contains(schengen);
    }

    /**
     * Пробуем получить из ответа КИАС код ошибки.
     *
     * @param responseAsXml ответ КИАС
     * @return код ошибки в виде строки или {@code null}
     */
    private String getErrorCode(Document responseAsXml) {
        String dataErrorCode = null;
        try {
            dataErrorCode = XmlUtils.dataErrorCodeXpath.evaluate(responseAsXml);
        } catch (XPathExpressionException e) {
            Log.getLogger("main").warn("Ошибка при попытке получения кода ошибки из ответа КИАС, {}", e.getLocalizedMessage());
        }
        return dataErrorCode;
    }

    /**
     * Путем XSLT трансформации фильтруем поля справочников.
     *
     * @param responseAsXml ответ КИАС
     * @return преобразованый ответ КИАСа
     */
    Document transformKiasResponse(Document responseAsXml) {
        DOMResult result = new DOMResult();
        try (InputStream xsltInputStream = getXsltInputStream()) {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer(new StreamSource(xsltInputStream));
            transformer.transform(new DOMSource(responseAsXml), result);
        } catch (Exception e) {
            Log.getLogger("main").error("Ошибка при трансформации XML ответа от КИАС", e);
        }
        return (Document) result.getNode();
    }

    /**
     * Запрашиваем путь к файлу XSLT и возвращаем его {@link InputStream}.
     *
     * @return {@link InputStream} XSLT файла
     */
    private InputStream getXsltInputStream() {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        String xsltPath = Utils.APP_PROPERTIES.getProperty("catalog.xslt.path");
        return contextClassLoader.getResourceAsStream(xsltPath);
    }
}
