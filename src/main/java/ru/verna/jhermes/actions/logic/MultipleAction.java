package ru.verna.jhermes.actions.logic;

import ru.verna.commons.log.Log;
import ru.verna.commons.log.ThreadUtil;
import ru.verna.commons.model.ParamStructure;
import ru.verna.jhermes.actions.ActionExecutor;

import javax.servlet.http.HttpServletRequest;
import java.util.AbstractMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * Выполнение списка действий, переданные действия не должны иметь параметры.
 */
public class MultipleAction implements ILogic {

    @Override
    public String execute(HttpServletRequest request, ParamStructure parameters) {
        Map<String, String[]> parameterMap = request.getParameterMap();
        String[] actions = parameterMap.get("actions");
        int capacity = actions.length;
        ExecutorService executor = Executors.newFixedThreadPool(capacity);
        Map<String, Future<String>> futureResult = new ConcurrentHashMap<>(capacity);

        Map<String, String> parentThreadVariables = ThreadUtil.getAllThreadVariables();

        for (String str : actions) {
            futureResult.put(str, executor.submit(() -> {
                ThreadUtil.putAllToThreadVariables(parentThreadVariables);
                return new ActionExecutor().execute(request, str, parameters);
            }));
        }

        Map<String, String> result = futureResult.entrySet()
                .stream()
                .map(e -> new AbstractMap.SimpleEntry<>(e.getKey(), getThreadResult(e.getValue())))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, ConcurrentHashMap::new));

        executor.shutdown();
        return getAsXml(result);
    }

    /**
     * Получение результата из {@link Future}.
     *
     * @param future переданный экземпляр {@link Future}
     * @return результат выполнения {@link Future}
     */
    private String getThreadResult(Future<String> future) {
        try {
            while (!future.isDone()) {
                Thread.sleep(200);
            }
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            Log.getLogger("main").warn("Cannot get result from future {}", e);
        }
        return null;
    }

    /**
     * Оборачивает весь сформированный ответ тэгом {@code data}, удаляет данный тэг из тела подзапросов. Заменяем в именах действий - на _.
     *
     * @param map экземпляр {@link Map} где ключи имена действий, а значения результаты запросов в КИАС
     * @return сформированый из {@link Map} XML
     */
    private String getAsXml(Map<String, String> map) {
        StringBuffer buffer = new StringBuffer("<data>");
        String key;
        for (Map.Entry<String, String> entry : map.entrySet()) {
            key = entry.getKey().replaceAll("/", "_") + ">";
            String value = entry.getValue()
                    .replaceFirst("<.*xml.*?>", "")
                    .replaceAll("<data>", "")
                    .replaceAll("</data>", "");
            buffer.append("<").append(key).append(value).append("</").append(key);
        }
        buffer.append("</data>");
        return buffer.toString();
    }
}
