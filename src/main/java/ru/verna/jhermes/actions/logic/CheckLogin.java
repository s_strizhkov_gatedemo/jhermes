package ru.verna.jhermes.actions.logic;

import javax.servlet.http.HttpServletRequest;
import ru.verna.commons.model.ParamStructure;

/**
 * Проверка авторизации пользователя. Если действие возвращает ok, значит, пользователь прошел проверку авторизации.
 */
public class CheckLogin implements ILogic {
    @Override
    public String execute(HttpServletRequest request, ParamStructure parameters) {
        return "<data><result><ok>ok</ok></result></data>";
    }

}
