package ru.verna.jhermes.actions.logic;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import ru.verna.commons.DbUtils;
import ru.verna.commons.Utils;
import ru.verna.commons.XmlUtils;
import ru.verna.commons.http.CommonHttpClient;
import ru.verna.commons.log.Log;
import ru.verna.commons.model.Constants;
import ru.verna.commons.model.ParamStructure;
import ru.verna.jhermes.actions.ActionExecutor;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Формирование url'а оплаты через Альфа-банк по ISN счета.
 */
public class GetInvoicePaymentUrl implements ILogic {

    @Override
    public String execute(HttpServletRequest request, ParamStructure parameters) {
        Logger mainLogger = Log.getLogger("main");

        try {
            ActionExecutor paymentDataGetter = new ActionExecutor();
            String paymentDataXml = paymentDataGetter.execute(request, "getInvoiceInfo", parameters);
            mainLogger.trace("получили данные счета: {}", paymentDataXml);

            Document paymentDataAsXmlDoc = XmlUtils.parseToXml(paymentDataXml);

            Node dataNode = XmlUtils.getChildByName(paymentDataAsXmlDoc, "data", false);
            if (dataNode == null) {
                return paymentDataXml;
            }

            // если paymentData вернул ошибки, то выкидываем ее наверх
            Node errorNode = XmlUtils.getChildByName(dataNode, "error", false);
            if (errorNode != null) {
                return paymentDataXml;
            }

            // в paymentData нет ошибок
            // invoiceIsn будет взят из request'а или переданных параметров
            String invoiceIsn = (request.getParameter("isn") != null) ? request.getParameter("isn")
                    : (String) parameters.getParam("isn").getValue();
            String partnerSuccessURL = parameters.getParam("partner_success_url").getValueAsString();
            String partnerFailureURL = parameters.getParam("partner_failure_url").getValueAsString();
            String partnerCallbackURL = parameters.getParam("partner_callback_url").getValueAsString();
            String postbankURL = parameters.getParam("postbank_url").getValueAsString();

            double calcSum = 0; // в рублях
            Node dataResultNode = XmlUtils.getChildByName(dataNode, "result", false);
            Node calcSumNode = XmlUtils.getChildByName(dataResultNode, "amount", false);
            if (calcSumNode != null) {
                String calcSumAsText = calcSumNode.getTextContent();
                if (Utils.notEmpty(calcSumAsText)) {
                    calcSumAsText = calcSumAsText.replace(',', '.');
                    calcSum = Double.parseDouble(calcSumAsText);
                }
            }
            if (calcSum == 0) {
                return Utils.getErrorMessage(Constants.ERRORCODE.PARAMETER_REQUIRED.code(),
                        "Не заполнена сумма оплаты");
            }
            long calcSumLong = getCalcSumLong(calcSum); // в копейках
            if (calcSumLong >= Long.valueOf(Utils.APP_PROPERTIES.getProperty("payment.excess.amount"))) {
                return Utils.getErrorMessage(Constants.ERRORCODE.EXCESS_OF_THE_AMOUNT.code(),
                        "Превышена разрешенная сумма оплаты он-лайн");
            }

            Node docDateNode = XmlUtils.getChildByName(dataResultNode, "docDate", false);
            String docDate = "";
            if (docDateNode != null) {
                docDate = docDateNode.getTextContent();
            }

            String amount = Long.toString(calcSumLong);

            String scheme = request.getScheme();
            String serverName = request.getServerName();
            int serverPort = request.getServerPort();
            String successUrl = scheme + "://" + serverName + ":" + serverPort + "/public/abi_postpayment?success=1";
            String failureUrl = scheme + "://" + serverName + ":" + serverPort + "/public/abi_postpayment?success=0";

            // в БД в таблице config настройка alfabank_mode задает, к какому серверу Альфа-банка подключаться, к
            // боевому или тестовому
            Map<String, String> alfabankModeConfig = DbUtils.getConfigs("alfabank_mode");
            String alfabankMode = alfabankModeConfig.get("alfabank_mode");

            Map<String, String> alfabankConfigs = DbUtils.getConfigs(alfabankMode);

            Map<String, String> params = new HashMap<>();

            String ourOrderId = generateOrderId(invoiceIsn);

            params.put("userName", alfabankConfigs.get("username"));
            params.put("password", alfabankConfigs.get("password"));
            params.put("orderNumber", ourOrderId);
            params.put("amount", amount); // в копейках
            params.put("returnUrl", successUrl);
            params.put("failUrl", failureUrl);
            params.put("description", "Оплата по счету № " + invoiceIsn + " от " + docDate);
            params.put("currency", "810");
            params.put("language", "ru");

            Map<String, String> extraParams = new HashMap<>();
            extraParams.put("invoice_isn", invoiceIsn);
            extraParams.put("partner_success_url", partnerSuccessURL);
            extraParams.put("partner_failure_url", partnerFailureURL);
            extraParams.put("partner_callback_url", partnerCallbackURL);
            extraParams.put("postbank_url", postbankURL);
            String extraParamsJson = new ObjectMapper().writeValueAsString(extraParams);
            params.put("jsonParams", extraParamsJson);

            Map<String, String> customerDetails = new HashMap<>();

            Map<String, String> quantity = new HashMap<>();
            quantity.put("value", "1");
            quantity.put("measure", "шт");

            Map<String, Object> item = new HashMap<>();
            item.put("positionId", "1");
            item.put("name", "Оплата по счету № " + invoiceIsn + ((Utils.notEmpty(docDate)) ? " от " + docDate : ""));
            item.put("quantity", quantity);
            item.put("itemAmount", amount);
            item.put("itemPrice", amount);
            item.put("itemCode", ourOrderId);

            List<Map<String, Object>> items = new ArrayList<>();
            items.add(item);

            Map<String, List<Map<String, Object>>> cartItems = new HashMap<>();
            cartItems.put("items", items);

            Map<String, Object> orderBundle = new HashMap<>();
            orderBundle.put("customerDetails", customerDetails);
            orderBundle.put("cartItems", cartItems);

            String orderBundleJson = new ObjectMapper().writeValueAsString(orderBundle);
            params.put("orderBundle", orderBundleJson);

            String alfaServer = alfabankConfigs.get("baseurl") + alfabankConfigs.get("registerOrder");
            return requestedUrlFromBank(params, alfaServer);

        } catch (Exception e) {
            mainLogger.error(null, e);
            return Utils.getErrorMessage(Constants.ERRORCODE.DB_ERROR.code(),
                    e.getLocalizedMessage() + ". Пожалуйста, обратитесь к администратору системы.");
        }
    }

    /**
     * Переданную сумму в формате {@link Double} приводит к сумме в копейках.
     *
     * @param calcSum переданная сумма в формате {@link Double}
     * @return переданная сумма в копейках в формате {@link Long}
     */
    long getCalcSumLong(double calcSum) {
        return BigDecimal.valueOf(calcSum).multiply(BigDecimal.valueOf(100)).longValue();
    }

    /**
     * Генерируем на основе пришедшего invoiceIsn уникальный номер заказа для отправки в банк.
     *
     * @param invoiceIsn пришедший invoiceIsn
     * @return сгенерированный номер заказа
     */
    private String generateOrderId(String invoiceIsn) {
        long ourOrderId = Utils.getRandomLong(4);
        return invoiceIsn + "/" + ourOrderId;
    }

    /**
     * Запрашиваем у банка URL для перехода.
     *
     * @param params собранные параметры запроса
     * @param alfaServer адрес сервера банка
     * @return URL для перехода в виде строки
     * @throws Exception при ошибках во время выполнения запроса
     */
    private String requestedUrlFromBank(Map<String, String> params, String alfaServer) throws Exception {
        Logger mainLogger = Log.getLogger("main");
        String bankResponse = CommonHttpClient.send(HttpGet.METHOD_NAME, alfaServer, params, null);
        JSONObject bankResponseAsJSON = new JSONObject(bankResponse);
        if (bankResponseAsJSON.has("formUrl")) {
            String acquirerUrl = bankResponseAsJSON.getString("formUrl");
            mainLogger.trace("Альфа-банк вернул адрес страницы оплаты: {}", acquirerUrl);
            // кодируем, чтобы символы '&' в строке paymentPageUrl не воспринимались как начало xml-сущностей
            String encodedUrl = URLEncoder.encode(acquirerUrl, "UTF-8");
            return "<data><result><url>" + encodedUrl + "</url></result></data>";

        } else if (bankResponseAsJSON.has("errorCode")) {
            // банк вернул ошибку
            String errorCode = bankResponseAsJSON.getString("errorCode");
            mainLogger.error("Альфа-банк вернул ошибку: alfabankErrorCode:[{}], alfabankErrorText:[{}]", errorCode,
                    bankResponse);
            return Utils.getErrorMessage(2000 + Integer.parseInt(errorCode),
                    bankResponseAsJSON.has("errorMessage") ? bankResponseAsJSON.getString("errorMessage")
                            : ("Банк вернул ошибку " + errorCode));

        } else {
            // ни url'а, ни ошибки
            long ticket = Utils.getRandomLong(4);
            String errMsg = "Необработанный ответ эквайрера";
            mainLogger.error("Инцидент {} {}: {}", ticket, errMsg, bankResponse);
            return Utils.getErrorMessage(299,
                    errMsg + ". Пожалуйста, сообщите администратору системы номер инцидента: " + ticket);
        }
    }
}
