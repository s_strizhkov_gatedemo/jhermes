package ru.verna.jhermes.actions.logic.medins;

import org.slf4j.Logger;
import ru.verna.commons.log.Log;
import ru.verna.commons.model.ActionParam;
import ru.verna.commons.model.ParamStructure;
import ru.verna.jhermes.actions.ActionExecutor;
import ru.verna.jhermes.actions.logic.ILogic;
import javax.servlet.http.HttpServletRequest;

/**
 * Действие по созданию договора для сервиса Medins.
 */
public class ObtainCertificate implements ILogic {

    @Override
    public String execute(HttpServletRequest request, ParamStructure parameters) {
        Logger mainLogger = Log.getLogger("main");
        ParamStructure newParamStructure = transformParamsForGate(parameters);
        ActionExecutor createMigrantAgreement = new ActionExecutor();
        String gateResponse = createMigrantAgreement.execute(request, "createMigrantAgreement", newParamStructure);
        mainLogger.trace("получили данные договора: {}", gateResponse);
        return gateResponse;
    }

    /**
     * Трасформирует полученные из сервиса Medins параметры в параметры для шлюза.
     *
     * @param parameters полученные из сервиса Medins параметры
     * @return новую структуру параметров соответсвующую параметрам вызова в шлюзе действия {@code createMigrantAgreementPerm}
     */
    private ParamStructure transformParamsForGate(ParamStructure parameters) {
        ParamStructure result = new ParamStructure();
        result.add(new ActionParam("patentId", "patentId", parameters.getParam("applicationId").getValueAsString()));
        result.add(new ActionParam("bso_number", "bso_number", parameters.getParam("bso_number").getValueAsString()));
        result.add(new ActionParam("bso_series", "bso_series", parameters.getParam("bso_series").getValueAsString()));
        result.add(new ActionParam("bso_type_isn", "bso_type_isn", parameters.getParam("bso_type_isn").getValueAsString()));
        result.add(new ActionParam("sale_point_id", "sale_point_id", parameters.getParam("sale_point_id").getValueAsString()));
        result.add(new ActionParam("insurer-firstName", "insurer-firstName", parameters.getParam("PersonDataType-name").getValueAsString()));
        result.add(new ActionParam("insurer-middleName", "insurer-middleName", parameters.getParam("PersonDataType-secondName").getValueAsString()));
        result.add(new ActionParam("insurer-lastName", "insurer-lastName", parameters.getParam("PersonDataType-lastName").getValueAsString()));
        result.add(new ActionParam("insurer-firstNameLat", "insurer-firstNameLat", parameters.getParam("PersonDataType-name_lat").getValueAsString()));
        result.add(new ActionParam("insurer-middleNameLat", "insurer-middleNameLat", parameters.getParam("PersonDataType-secondName_lat").getValueAsString()));
        result.add(new ActionParam("insurer-lastNameLat", "insurer-lastNameLat", parameters.getParam("PersonDataType-lastName_lat").getValueAsString()));
        result.add(new ActionParam("insurer-birthDate", "insurer-birthDate", parameters.getParam("PersonDataType-dateOfBirth").getValueAsString()));
        result.add(new ActionParam("insurer-citizenship", "insurer-citizenship", parameters.getParam("PersonDataType-citizenship").getValueAsString()));
        result.add(new ActionParam("insurer-birthCountry", "insurer-birthCountry", parameters.getParam("PersonDataType-countryOfBirth").getValueAsString()));
        result.add(new ActionParam("insurer-birthPlace", "insurer-birthPlace", parameters.getParam("PersonDataType-placeOfBirth").getValueAsString()));
        result.add(new ActionParam("insurer-address", "insurer-address", parameters.getParam("PersonDataType-address").getValueAsString()));
        result.add(new ActionParam("insurer-phone", "insurer-phone", parameters.getParam("PersonDataType-phone").getValueAsString()));
        result.add(new ActionParam("insurer-gender", "insurer-gender", parameters.getParam("PersonDataType-sex").getValueAsString()));
        result.add(new ActionParam("insurer-documents-document-date", "insurer-documents-document-date",
                parameters.getParam("PassportType-issueDate").getValueAsString()));
        result.add(new ActionParam("insurer-documents-document-issuedBy", "insurer-documents-document-issuedBy",
                parameters.getParam("PassportType-issuedBy").getValueAsString()));
        result.add(new ActionParam("insurer-documents-document-number", "insurer-documents-document-number",
                parameters.getParam("PassportType-number").getValueAsString()));
        result.add(new ActionParam("insurer-documents-document-series", "insurer-documents-document-series",
                parameters.getParam("PassportType-series").getValueAsString()));
        Log.getLogger("main").debug("Собраны параметры для запроса в шлюз: [{}]", result);
        return result;
    }
}
