package ru.verna.jhermes.actions.logic.estate;

import ru.verna.commons.XmlUtils;
import ru.verna.commons.model.ActionParam;
import ru.verna.commons.model.ParamStructure;
import ru.verna.jhermes.actions.ActionExecutor;
import ru.verna.jhermes.actions.logic.ILogic;

import javax.servlet.http.HttpServletRequest;

/**
 * Комплексное действие по созданию и возврату страницы для оплаты, на данный момент используется для тестирования доступа к сервису оплаты Альфа банка.
 */
public class CreateAndBuy implements ILogic {

    @Override
    public String execute(HttpServletRequest request, ParamStructure parameters) {
        String result = null;
        result = createApartmentCalc(request, parameters);
        String calcId;
        String nodeName = "calcId";
        if (result.contains(nodeName)) {
            calcId = XmlUtils.getNodeValue(result, nodeName);
        } else {
            return result;
        }
        ParamStructure newParamStructure = new ParamStructure();
        newParamStructure.readRequest(request.getParameterMap(), true);
        newParamStructure.add(new ActionParam("calc_id", "calc_id", calcId));
        result = new ActionExecutor().execute(request, "getAlfabankUrl", newParamStructure);
        return result;
    }

    /**
     * Вызывает действие по созданию котировки.
     *
     * @param request    входящий запрос
     * @param parameters параметры запроса
     * @return результат ответа сервиса КИАС
     */
    private String createApartmentCalc(HttpServletRequest request, ParamStructure parameters) {
        String result = new ActionExecutor().execute(request, getCreateEstateAction(request), parameters);
        return result;
    }

    /**
     * Из переданого запроса формирует имя действия для создания котировки.
     *
     * @param request входящий запрос
     * @return имя действия для создания котировки
     */
    private String getCreateEstateAction(HttpServletRequest request) {
        String result = request.getRequestURI().replace("/gate/", "").replace("createAndBuy", "createCalc");
        return result;
    }
}
