package ru.verna.jhermes.actions;

import javax.servlet.http.HttpServletRequest;
import ru.verna.jhermes.model.Action;
import ru.verna.commons.model.ParamStructure;

/**
 * Интерфейс выполнения действий.
 */
public interface Executor {

    /**
     * Выполнение действия.
     *
     * @param request http-запрос, где лежит вся входящая информация
     * @param action выполняемое действие
     * @param parameters параметры выполнения действия, сформированные из пришедших в request'е параметров
     * @return строковый результат выполнения действия
     */
    String execute(HttpServletRequest request, Action action, ParamStructure parameters);
}
