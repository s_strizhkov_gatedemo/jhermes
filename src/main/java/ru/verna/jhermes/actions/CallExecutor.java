package ru.verna.jhermes.actions;

import org.slf4j.Logger;
import ru.verna.commons.log.Log;
import ru.verna.commons.model.ActionParam;
import ru.verna.commons.model.ParamStructure;
import ru.verna.jhermes.Util;
import ru.verna.jhermes.kiasclient.KiasClient;
import ru.verna.jhermes.kiasclient.RequestMaker;
import ru.verna.jhermes.model.Action;
import ru.verna.jhermes.model.User;
import javax.servlet.http.HttpServletRequest;
import static ru.verna.commons.model.Constants.SID_PARAMETER_NAME;

/**
 * Выполняет действия типа 'call'.
 */
public class CallExecutor implements Executor {
    /**
     * Вызывает метод веб-сервиса КИАС.
     *
     * @return строковый результат выполнения действия
     */
    @Override
    public String execute(HttpServletRequest request, Action action, ParamStructure parameters) {
        Logger mainLogger = Log.getLogger("main");

        if (mainLogger.isTraceEnabled()) {
            String valuesAsJson = parameters.getValuesAsJson(false);
            mainLogger.trace("запущен для вызова сервиса {} с параметрами {}", action.getDependant(), valuesAsJson);
        }

        User user = Util.getUser(request);
        if (user == null) {
            throw new IllegalStateException("user is null");
        }
        ActionParam sidParam;
        String sid = request.getHeader(SID_PARAMETER_NAME);
        if (sid == null) {
            sid = (String) request.getAttribute(SID_PARAMETER_NAME);
        }
        if (sid != null) {
            sidParam = new ActionParam(SID_PARAMETER_NAME, SID_PARAMETER_NAME, sid);
        } else {
            sidParam = Util.getSidProvider(request).getSidParam(user);
        }
        int kiasAppId = user.getKiasAppId();

        boolean flattenParams = action.isFlattenParams();

        String requestString = RequestMaker.newRequest(kiasAppId)
                .setRequestName(action.getDependant())
                .addParam(sidParam)
                .addParamStructure(parameters)
                .convertToString(flattenParams);
        mainLogger.trace("собран запрос {}", requestString);

        String kiasResponse = KiasClient.execProcXmlString(requestString, action.getLogsize());
        return kiasResponse;
    }
}
