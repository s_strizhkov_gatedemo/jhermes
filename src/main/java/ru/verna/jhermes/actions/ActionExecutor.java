package ru.verna.jhermes.actions;

import org.json.JSONException;
import org.slf4j.Logger;
import ru.verna.commons.DbUtils;
import ru.verna.commons.Utils;
import ru.verna.commons.log.Log;
import ru.verna.commons.model.ActionParam;
import ru.verna.commons.model.ParamStructure;
import ru.verna.commons.model.paramchecker.ErrorList;
import ru.verna.jhermes.Util;
import ru.verna.jhermes.model.Action;
import ru.verna.jhermes.model.User;
import ru.verna.jhermes.model.rules.RuleChecker;
import ru.verna.jhermes.model.rules.RuleErrorList;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;

import static ru.verna.commons.model.Constants.ERRORCODE.ACTION_IS_NOT_ALLOWED;
import static ru.verna.commons.model.Constants.ERRORCODE.DB_ERROR;

/**
 * Класс, выполняющий заданное действие.
 */
public class ActionExecutor implements Executor {

    /**
     * Выполняет заданное действие.
     *
     * @param request            http-запрос
     * @param actionId           идентификатор действия.
     * @param explicitParameters явно заданные параметры для действия. Имеют приоритет перед параметрами запроса, то
     *                           есть экстра-параметр перетрет параметр из запроса с таким же именем.
     * @return строковый результат выполнения действия
     */
    public String execute(HttpServletRequest request, String actionId, ParamStructure explicitParameters) {
        Action action = new Action();
        action.setId(actionId);
        return execute(request, action, explicitParameters);
    }

    /**
     * Выполняет заданное действие.
     *
     * @param parameters явно заданные параметры для действия. Имеют приоритет перед параметрами запроса, то
     *                   есть экстра-параметр перетрет параметр из запроса с таким же именем.
     * @param action     (неизменяемый в методе) контейнер для идентификатора выполняемого действия. В методе будет
     *                   произведен поиск "настоящего" объекта действия по базе данных (таблица actions).
     * @return строковый результат выполнения действия
     */
    @Override
    public String execute(HttpServletRequest request, Action action, ParamStructure parameters) {
        Logger mainLogger = Log.getLogger("main");
        Logger dbLogger = Log.getLogger("db");

        String actionId = action.getId();

        if (mainLogger.isTraceEnabled()) {
            mainLogger.debug("запущен для вызова действия {}", actionId);
        }

        User user = Util.getUser(request);

        // итоговая структура параметров
        ParamStructure fullParamStructure = new ParamStructure();

        ParamStructure explicitParamStructure = (parameters != null) ? parameters : new ParamStructure();

        Action actionFromDb = getActionFromDb(action, user);

        if (actionFromDb.isNotAllowed()) {
            // нет действия с таким id, или не досупно текущему пользователю
            dbLogger.debug("returned nothing");
            String errorMessage = Utils.getErrorMessage(ACTION_IS_NOT_ALLOWED.code(), "Действие " + actionId + " не найдено");
            mainLogger.debug("ответ: {}", errorMessage);
            return errorMessage;
        }

        // собираем и проверяем параметры
        try {
            // чтобы соблюсти структуру пришедших параметров, считываем её в fullParamStructure
            fullParamStructure.readRequest(request.getParameterMap());
            // fullParamStructure сейчас содержит правильную структуру, но все ActionParam'ы там помечены как confirmed=false
        } catch (JSONException e) {
            mainLogger.error("ошибка разбора параметров запроса", e);
            return Utils.getErrorMessage("", "ошибка разбора параметров запроса");
        }

        try {
            fullParamStructure = enrichParamStructure(fullParamStructure, explicitParamStructure, actionId, user.getId());
        } catch (SQLException e) {
            dbLogger.error(null, e);
            String errorMessage = Utils.getErrorMessage(DB_ERROR.code(), e.getLocalizedMessage() + ". Пожалуйста, обратитесь к администратору системы.");
            dbLogger.debug("ответ: {}", errorMessage);
            return errorMessage;
        }

        fullParamStructure.removeNonconfirmed();
        // закончили формирование fullParamStructure

        ErrorList badParams = fullParamStructure.check();
        if (badParams.isEmpty()) {
            mainLogger.debug("проблемных параметров нет");
        } else {
            String errorsMessage = badParams.getErrorsMessage();
            mainLogger.debug("найдены проблемные параметры, ответ: {}", errorsMessage);
            return errorsMessage;
        }

        String paramsAsJson = fullParamStructure.getValuesAsJson(actionFromDb.isFlattenParams());
        RuleChecker ruleChecker = new RuleChecker(paramsAsJson);
        RuleErrorList ruleErrors = ruleChecker.check(actionFromDb.getId());
        if (ruleErrors.isEmpty()) {
            mainLogger.debug("нарушений правил нет");
        } else {
            String errorsMessage = ruleErrors.getErrorsMessage();
            mainLogger.debug("найдены нарушения правил, ответ: {}", errorsMessage);
            return errorsMessage;
        }

        return executeAction(actionFromDb, request, fullParamStructure, explicitParamStructure);
    }

    /**
     * Определяем действие.
     *
     * @param entryAction переданное на выполнение действие
     * @param user        пользователь полученный из запроса
     * @return если найдено в БД, то полностью проинициализированное действие, в противном случае просто пустой объект
     */
    private Action getActionFromDb(Action entryAction, User user) {
        Logger dbLogger = Log.getLogger("db");
        Action action = new Action();
        String actionId = entryAction.getId();
        final String actionSQL = "SELECT a.actiontype_id, a.dependant, a.flatten_params, a.logsize "
                + "FROM actions a, "
                + "v_user_actions ua, "
                + "users u "
                + "WHERE LOWER(a.id) = LOWER(?) "
                + "AND a.id = ua.action_id "
                + "AND ua.user_id = u.id "
                + "AND u.token = ?";
        try (Connection conn = DbUtils.getConnection();
             PreparedStatement ps = conn.prepareStatement(actionSQL)) {
            String userToken = user.getToken();
            ps.setString(1, actionId);
            ps.setString(2, userToken);
            dbLogger.debug("|{}|, args: actionId={}, usertoken={}", actionSQL, actionId, userToken);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    String actionType = rs.getString(1);
                    String dependant = rs.getString(2);
                    boolean flattenParams = rs.getBoolean(3);
                    int logsize = rs.getInt(4);
                    dbLogger.debug("returned row: actionType={}, dependant={}, flatten_params={}, logsize={}", actionType, dependant, flattenParams, logsize);

                    action.setId(actionId);
                    action.setType(actionType);
                    action.setDependant(dependant);
                    action.setFlattenParams(flattenParams);
                    if (entryAction.isLogsizeNull()) {
                        action.setLogsize(logsize);
                    } else {
                        action.setLogsize(entryAction.getLogsize());
                    }
                }
            }
        } catch (SQLException e) {
            dbLogger.error("Ошибка при попытке получить данные действия из БД", e);
        }
        return action;
    }

    /**
     * Обогащаем {@link ActionParam} в полученной из запроса {@link ParamStructure} данными из БД и передаными явно параметрами.
     *
     * @param fullPS     полученная из запроса структура параметров
     * @param explicitPS переданные явно параметры
     * @param actionId   ID действия, соотвествует колонке id в таблице public.actions
     * @param userId     ID пользователя, полученный из запроса
     * @return обогащенная структура данных
     * @throws SQLException при проблемах во время обращения к БД
     */
    private ParamStructure enrichParamStructure(ParamStructure fullPS, ParamStructure explicitPS, String actionId, int userId) throws SQLException {
        Logger dbLogger = Log.getLogger("db");
        final String paramsSql = "SELECT public_name, kias_name, descr, type_id, minlength, maxlength, mask, is_required, value "
                + "  FROM check_and_get_user_action_params(?, ?)";
        try (Connection conn = DbUtils.getConnection();
             PreparedStatement ps = conn.prepareStatement(paramsSql)) {
            ps.setString(1, actionId);
            ps.setInt(2, userId);
            dbLogger.debug("|{}|, args: action_id={}, userId={}", paramsSql, actionId, userId);
            try (ResultSet rs = ps.executeQuery()) {
                int cnt = 0;
                while (rs.next()) {
                    cnt++;
                    String paramPublicName = rs.getString("public_name");
                    String paramKiasName = rs.getString("kias_name");
                    String descr = rs.getString("descr");
                    String typeId = rs.getString("type_id");
                    boolean required = rs.getBoolean("is_required");
                    Integer minlength = rs.getInt("minlength");
                    Integer maxlength = rs.getInt("maxlength");
                    String mask = rs.getString("mask");
                    String userDefinedValue = rs.getString("value");
                    dbLogger.trace(
                            "returned row #{}: public_name={}, kias_name={}, type_id={}, required={}, minlen={}, maxlen={}, mask={}, value={}",
                            cnt, paramPublicName, paramKiasName, typeId, required, minlength, maxlength, mask,
                            userDefinedValue);

                    String paramValue = selectParamValue(explicitPS.getParam(paramPublicName), userDefinedValue);

                    ActionParam param = new ActionParam(paramPublicName, paramKiasName, descr, typeId, required, minlength, maxlength, mask);
                    param.setValue(paramValue);
                    param.setConfirmed(true);
                    fullPS.add(param);
                }
                dbLogger.debug("return {} row(s)", cnt);
            }
        } catch (SQLException e) {
            dbLogger.error("Ошибка при попытке обогатить данными параметры действия", e);
            throw e;
        }
        return fullPS;
    }

    /**
     * Проверяем передано ли значение в дополнительных параметрах, если да то берем его, затем проверяем заполнено ли предопределенное значение
     * из таблицы public.user_defined_params, если оно есть то переопределяем возвращаемое значение.
     *
     * @param explicitParam    переданный в запросе дополнительный параметр
     * @param userDefinedValue значение из таблицы public.user_defined_params
     * @return заполненое значение параметра в виде строки
     */
    private String selectParamValue(ActionParam explicitParam, String userDefinedValue) {
        String result = null;
        if ((explicitParam != null) && (explicitParam.getValue() != null)) {
            result = explicitParam.getValueAsString();

        } else if (userDefinedValue != null) {
            result = userDefinedValue;
        }
        return result;
    }

    /**
     * Выбор и выполнение конкретной реализации {@link Executor}.
     *
     * @param actionFromDb           экземпляр выполняемого действия
     * @param request                экземпляр пришедшего запроса
     * @param fullParamStructure     заполненая структура параметров
     * @param explicitParamStructure структура дополнительных параметров
     * @return результат вызова в виде строки
     */
    private String executeAction(Action actionFromDb, HttpServletRequest request, ParamStructure fullParamStructure, ParamStructure explicitParamStructure) {
        Logger mainLogger = Log.getLogger("main");
        String result;
        switch (actionFromDb.getType().toLowerCase(Locale.ENGLISH)) {
            case "call":
                CallExecutor callExecutor = new CallExecutor();
                mainLogger.debug("вызывает CallExecutor для {}", actionFromDb.getDependant());
                result = callExecutor.execute(request, actionFromDb, fullParamStructure);
                mainLogger.debug("CallExecutor для {} вернул {}", actionFromDb.getDependant(),
                        Utils.restrictString(result, actionFromDb.getLogsize()));
                break;

            case "logic":
                LogicExecutor logicExecutor = new LogicExecutor();
                mainLogger.debug("вызывает LogicExecutor для {}", actionFromDb.getDependant());
                result = logicExecutor.execute(request, actionFromDb, fullParamStructure);
                mainLogger.debug("LogicExecutor для {} вернул {}", actionFromDb.getDependant(),
                        Utils.restrictString(result, actionFromDb.getLogsize()));
                break;

            case "alias":
                ActionExecutor actionExecutor = new ActionExecutor();
                mainLogger.debug("вызывает ActionExecutor для {}", actionFromDb.getDependant());

                // добавляем к текущим explicitParameters те параметры, значения которых мы уже получили (в том числе из user_defined_params)
                ParamStructure newExplicitParameters = new ParamStructure();
                newExplicitParameters.addStructure("", explicitParamStructure);
                newExplicitParameters.addStructure("", fullParamStructure);

                Action aliasedAction = new Action();
                aliasedAction.setId(actionFromDb.getDependant());
                aliasedAction.setLogsize(actionFromDb.getLogsize());
                result = actionExecutor.execute(request, aliasedAction, newExplicitParameters);
                mainLogger.debug("ActionExecutor для alias {} вернул {}", actionFromDb.getDependant(),
                        Utils.restrictString(result, actionFromDb.getLogsize()));
                break;

            default:
                String unsupported = "Необрабатываемый тип действия";
                mainLogger.warn("{}: действие {}", unsupported, actionFromDb);
                throw new UnsupportedOperationException(unsupported);
        }
        return result;
    }
}
