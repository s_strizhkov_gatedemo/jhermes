package ru.verna.jhermes.kiasclient;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import ru.inslab.ExecProcResponseExecProcResult;
import ru.verna.commons.Utils;
import ru.verna.commons.XmlUtils;
import ru.verna.commons.log.Log;
import ru.verna.commons.model.Constants;
import ru.verna.jhermes.Util;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;

/**
 * Класс взаимодействия с веб-службой КИАС.
 */
public final class KiasClient {

    /**
     * Закрытый конструктор.
     */
    private KiasClient() {
    }

    /**
     * Вызывает метод kias-сервиса, возвращает результат в формате xml.
     *
     * @param requestString аргументы вызова сервиса
     * @param limitLogMsg ограничение журналируемого сообщения для борьбы с слишком длинными. Если limitLogMsg > 0, то
     * будут зажурналированы только последние limitLogMsg символов отклика сервера.
     * @return строка xml с результатом вызова
     */
    public static String execProcXmlString(java.lang.String requestString, int... limitLogMsg) {
        Node serviceResponse = execProcXml(requestString, limitLogMsg);
        String result = XmlUtils.convertToString(serviceResponse);
        return result;
    }

    /**
     * Возвращает результат выполнения сервисом запроса {@code requestString}.
     *
     * @param requestString запрос к веб-службе КИАСа в виде строки
     * @param limitLogMsg ограничение журналируемого сообщения для борьбы с слишком длинными. Если limitLogMsg &lt; 0,
     * то будут зажурналированы только последние limitLogMsg символов отклика сервера.
     * @return xml-представление отклика веб-службы.
     */
    private static Node execProcXml(java.lang.String requestString, int... limitLogMsg) {
        return execProcXml(requestString, Level.INFO, limitLogMsg);
    }

    /**
     * Возвращает результат выполнения сервисом запроса {@code requestString}.
     *
     * @param requestString запрос к веб-службе КИАСа в виде строки
     * @param level уровень журналирования сообщений об этой транзакции
     * @param limitLogMsg ограничение журналируемого сообщения для борьбы с слишком длинными. Если limitLogMsg &lt; 0,
     * то будут зажурналированы только последние limitLogMsg символов отклика сервера.
     * @return xml-представление отклика веб-службы.
     */
    public static Node execProcXml(java.lang.String requestString, Level level, int... limitLogMsg) {
        Logger wsLogger = (Logger) Log.getLogger("ws");
        String kiasSvcHost = Util.getKiasSvcHost();
        Node responseNode = null;

        boolean hostIsReachable = Util.isReachable(kiasSvcHost, 80, 5000);

        if (hostIsReachable) {
            wsLogger.debug("Хост веб-службы КИАСа доступен по адресу: {}", kiasSvcHost);
            // типа уникальный id транзакции запрос-ответ
            // TODO надо ли перенести в тред локал?
            long txNumber = Utils.getRandomLong();

            wsLogger.log(null, Logger.FQCN, Level.toLocationAwareLoggerInteger(level),
                    "tx {} запрос: {}", new Object[]{txNumber, requestString}, null);

            ru.inslab.KiasSvcSoapProxy proxy = new ru.inslab.KiasSvcSoapProxy();
            try {
                ExecProcResponseExecProcResult execProc = proxy.execProc(requestString);
                responseNode = execProc.get_any()[0].getAsDocument();

                if (wsLogger.isEnabledFor(level)) {
                    String dataErrorCode = null;
                    try {
                        dataErrorCode = XmlUtils.dataErrorCodeXpath.evaluate(responseNode);
                    } catch (XPathExpressionException e) {
                    }
                    String responseAsString = XmlUtils.convertToString(responseNode);
                    // если не ошибка, то журналируем сокращенный вариант
                    String responseMsg;
                    if (Utils.isEmpty(dataErrorCode)) {
                        responseMsg = Utils.restrictString(responseAsString, limitLogMsg);
                    } else {
                        responseMsg = Utils.restrictString(responseAsString);
                    }
                    wsLogger.log(null, Logger.FQCN, Level.toLocationAwareLoggerInteger(level),
                            "tx {} ответ: {}", new Object[]{txNumber, responseMsg}, null);

                }

            } catch (Exception e) {
                wsLogger.error("tx " + txNumber + " ошибка:", e);
            }

        } else {
            wsLogger.warn("Хост веб-службы КИАСа НЕ доступен");

            try {
                String errorXml = Utils.getErrorMessage(Constants.ERRORCODE.KIASSVC_IS_UNREACHABLE.code(),
                        "Внутренняя информационная система недоступна, попробуйте позже");
                responseNode = XmlUtils.parseToXml(errorXml);
            } catch (SAXException | IOException e) {
                Log.MAIN.error("Ошибка при разборе xml", e);
            }
        }
        return responseNode;

    }
}
