package ru.verna.jhermes.kiasclient;

import org.json.JSONException;
import ru.verna.commons.log.Log;
import ru.verna.commons.model.ActionParam;
import ru.verna.commons.model.ParamStructure;

/**
 *
 */
public class RequestMaker {

    /**
     * Заготовка запроса к сервису.
     */
    private static final String BLANK_WHOLE = "{data: {request: {AppId: '', RequestIp: '', reqName: '', params: {}}}}";
    private ParamStructure whole = new ParamStructure();
    private ParamStructure params = new ParamStructure();
    private String xmlHeader = "";

    /**
     * Возвращает новый экземпляр генератора запроса для приложения сервиса с номером {@code appId}. См. таблицу
     * kiasapps.
     *
     * @param appId номер приложения сервиса (kiasapp)
     * @return новый экземпляр генератора запроса для приложения сервиса с номером {@code appId}.
     */
    public static RequestMaker newRequest(Integer appId) {
        RequestMaker instance = new RequestMaker();
        try {
            instance.whole.readJSON(BLANK_WHOLE);
            ActionParam appIdParam = new ActionParam("data-request-AppId", null, String.valueOf(appId));
            instance.whole.add(appIdParam);
        } catch (JSONException e) {
            Log.getLogger("main").error(null, e);
        }
        return instance;
    }

    /**
     * Устанавливает имя вызываемого метода.
     *
     * @param reqName имя вызываемого метода.
     * @return ссылку на текущий объект
     */
    public RequestMaker setRequestName(String reqName) {
        ActionParam reqNameParam = new ActionParam("data-request-reqName", null, reqName);
        this.whole.add(reqNameParam);
        return this;
    }

    /**
     * Добавляет параметр к запросу.
     *
     * @param param параметр
     * @return ссылку на текущий объект
     */
    public RequestMaker addParam(ActionParam param) {
        this.params.add(param);
        return this;
    }

    /**
     * Добавляет структуру параметров.
     *
     * @param addedParamStructure структура параметров.
     * @return ссылку на текущий объект
     */
    public RequestMaker addParamStructure(ParamStructure addedParamStructure) {
        this.params.addStructure("", addedParamStructure);
        return this;
    }

    /**
     * Добавляет параметры к запросу.
     *
     * @param params коллекция параметров
     * @return ссылку на текущий объект
     */
    public RequestMaker addParams(Iterable<ActionParam> params) {
        for (ActionParam param : params) {
            addParam(param);
        }
        return this;
    }

    /**
     * Очищает список параметров запроса.
     *
     * @return ссылку на текущий объект
     */
    public RequestMaker clearParams() {
        this.params = new ParamStructure();
        return this;
    }

    /**
     * Возвращает собранный запрос в виде строки.
     *
     * @param flattenParams нужно ли уплощать структуру парамтеров
     * @return запрос в виде строки
     */
    public String convertToString(boolean flattenParams) {
        try {
            // делаем структуру значений параметров
            String valuesAsJson = this.params.getValuesAsJson(flattenParams);
            ParamStructure valuesStruct = new ParamStructure();
            valuesStruct.readJSON(valuesAsJson);

            // добавляем структуру значений параметров в нужное место полного запроса
            this.whole.remove("data-request-params");
            whole.addStructure("data-request-params", valuesStruct);

            // выдаем xml-строку
            String result = whole.getValuesAsXml(false);
            return xmlHeader + result;

        } catch (JSONException e) {
            Log.getLogger("main").error(null, e);
            return "";
        }
    }

}
