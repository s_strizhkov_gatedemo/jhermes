package ru.verna.jhermes;

import org.slf4j.Logger;
import ru.verna.commons.log.Log;
import ru.verna.commons.Utils;
import ru.verna.jhermes.actions.CallExecutor;
import ru.verna.jhermes.model.Action;
import ru.verna.commons.model.ActionParam;
import ru.verna.commons.model.ParamStructure;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import static ru.verna.commons.model.Constants.ERRORCODE.ACTION_IS_NOT_SET;

/**
 * Сервлет позволяет вызывать киасовский метод (сервис веб-службы) напрямую, не через action'ы (то есть Киасовский метод
 * может не быть прописан в таблице actions).
 * <br>
 * Из соображений безопасности запускается только на localhost.
 * <br>
 * Токен любого активного пользователя в запросе нужен, сами права доступа не проверяются.
 */
@WebServlet(name = "CallerServlet", urlPatterns = { "/caller/*" })
public class CallerServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Logger mainLogger = Log.getLogger("main");

        String serverName = request.getServerName();
        mainLogger.trace("server name = {}", serverName);

        if (!"localhost".equalsIgnoreCase(serverName)) {
            response.setStatus(501);

        } else {
            if (mainLogger.isTraceEnabled()) {
                List<String> params = new LinkedList<>();
                request.getParameterMap().keySet().forEach((paramName) -> {
                    params.add(String.format("|%s|=|%s|", paramName, request.getParameter(paramName)));
                });
                mainLogger.trace("Запущен, параметры запроса: {}", Arrays.toString(params.toArray()));
            }

            String requestURI = request.getRequestURI();
            String contextPath = request.getContextPath();
            String service = requestURI.substring(contextPath.length() + 1);
            // сейчас service = caller/ИМЯ_МЕТОДА
            service = service.substring(service.lastIndexOf("/") + 1);
            // а сейчас service = ИМЯ_МЕТОДА
            mainLogger.debug("service = {}", service);

            String result;
            if (Utils.isEmpty(service)) {
                result = Utils.getErrorMessage(ACTION_IS_NOT_SET.code(), "Действие не определено");

            } else {
                ParamStructure parameters = new ParamStructure();
                Enumeration<String> parameterNames = request.getParameterNames();
                while (parameterNames.hasMoreElements()) {
                    String paramName = parameterNames.nextElement();
                    if (!"token".equalsIgnoreCase(paramName)
                            && !"f".equalsIgnoreCase(paramName)
                            && !"format".equalsIgnoreCase(paramName)) {
                        ActionParam ap = new ActionParam(paramName, paramName, request.getParameter(paramName));
                        parameters.add(ap);
                    }
                }
                CallExecutor callExecutor = new CallExecutor();
                Action callAction = new Action();
                callAction.setType("call");
                callAction.setDependant(service);
                callAction.setFlattenParams(false);
                result = callExecutor.execute(request, callAction, parameters);
            }

            mainLogger.debug("ответ: {}", result);
            response.getWriter().print(result);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the
    // code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>

}
