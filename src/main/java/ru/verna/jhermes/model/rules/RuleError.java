package ru.verna.jhermes.model.rules;

import ru.verna.commons.model.Error;

/**
 * Нарушение бизнес-правила (таблица rules).
 */
public class RuleError extends Error {
    /**
     * Null-объект для отсутствия ошибки.
     */
    public static final RuleError NO_ERROR = new RuleError(-1, "");

    /**
     * Конструктор.
     *
     * @param code код ошибки
     * @param text текст ошибки
     */
    public RuleError(int code, String text) {
        setCode(code);
        setText(text);
    }

}
