package ru.verna.jhermes.model.rules;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.ParseContext;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.codehaus.groovy.control.CompilationFailedException;
import org.slf4j.Logger;
import ru.verna.commons.DbUtils;
import ru.verna.commons.Utils;
import ru.verna.commons.log.Log;
import static ru.verna.commons.model.Constants.ERRORCODE.DB_ERROR;
import static ru.verna.commons.model.Constants.ERRORCODE.RULE_VIOLATION;

/**
 * Класс инкапсулирующий в себе проверку действий на соответствие бизнес правилам сохраненным в БД.
 */
public class RuleChecker {

    Configuration conf = Configuration.defaultConfiguration().addOptions(Option.DEFAULT_PATH_LEAF_TO_NULL);

    ParseContext parseContext = JsonPath.using(conf);

    private String json;

    private DocumentContext documentContext;

    /**
     * Конструктор принимающий JSON строку.
     *
     * @param json структура параметров в виде JSON строки
     */
    public RuleChecker(String json) {
        this.setJson(json);
    }

    /**
     * Выполняет проверки для заданного действия, возвращает список ошибок.
     *
     * @param actionId имя действия
     * @return the ru.verna.jhermes.model.rules.RuleErrorList
     */
    public RuleErrorList check(String actionId) {
        List<Rule> rulesList = retrieveRules(actionId);
        if (rulesList == null) {
            RuleError err = new RuleError(DB_ERROR.code(),
                    "При получении проверок произошла ошибка. Пожалуйста, обратитесь к администратору системы.");
            RuleErrorList result = new RuleErrorList();
            result.add(err);
            return result;
        } else {
            RuleErrorList checkRulesList = check(rulesList);
            return checkRulesList;
        }
    }

    /**
     * Проверка списка переданных правил.
     *
     * @param rulesList список правидл
     * @return список выявленных ошибок
     * @throws CompilationFailedException при ошибках компиляции
     */
    RuleErrorList check(Iterable<Rule> rulesList) throws CompilationFailedException {
        RuleErrorList result = new RuleErrorList();
        for (Rule rule : rulesList) {
            RuleError err = check(rule);
            result.add(err);
        }
        return result;
    }

    /**
     * Извлекает из БД список бизнес-правил для действия с идентификатором actionId. В случае ошибки обращения к БД
     * возвращает null.
     *
     * @param actionId имя действия
     * @return список бизнес-правил для заданного действия или null в случае ошибки БД
     */
    private List<Rule> retrieveRules(String actionId) {
        Logger mainLogger = Log.getLogger("main");
        Logger dbLogger = Log.getLogger("db");
        List<Rule> rulesList = new ArrayList<>();
        String sql = "SELECT rule_id, \"path\", as_list, matcher, matcher_arg1, matcher_arg2, error_message "
                + "FROM public.v_action_rules WHERE LOWER(action_id) = LOWER(?)";
        try (Connection conn = DbUtils.getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, actionId);
            dbLogger.debug("|{}|, args: actionId={}", sql, actionId);

            ResultSet rs = ps.executeQuery();
            int cnt = 0;
            while (rs.next()) {
                cnt++;
                String ruleId = rs.getString(1);
                String jsonpath = rs.getString(2);
                boolean asList = rs.getBoolean(3);
                String matcher = rs.getString(4);
                String matcherArg1 = rs.getString(5);
                String matcherArg2 = rs.getString(6);
                String errorMsg = rs.getString(7);

                Rule rule = new Rule();
                rule.setRuleId(ruleId);
                rule.setJsonpath(jsonpath);
                rule.setAsList(asList);
                rule.setMatcher(matcher);
                rule.setMatcherArg1(matcherArg1);
                rule.setMatcherArg2(matcherArg2);
                rule.setErrorMsg(errorMsg);

                rulesList.add(rule);

            }
            dbLogger.debug("return {} row(s)", cnt);

        } catch (SQLException e) {
            mainLogger.error(null, e);
            return null;
        } // close connection
        return rulesList;
    }

    /**
     * Возвращает результат проверки правила.
     *
     * // * @param path jsonpath-путь к проверяемой части строки, начинающийся с {@code $.}
     * // * @param asList если true, то результат выборки path рассматривается как список, иначе как скалярный объект
     * // * @param matcher правило проверки выборки. Разрешенные значения - методы класса org.hamcrest.Matchers.
     * // * @param matcherArg1 первый аргумент правила. Может быть готовым значением или path-путем к рассчитываемому
     * // * значению из проверяемой строки.
     * // * @param matcherArg2 второй аргумент правила. Может быть готовым значением или path-путем к рассчитываемому
     * // * значению из проверяемой строки.
     * // * @param errorMsg сообщение в случае ошибки. Можно ссылаться на: $value - проверяемое значение (результат
     * выборки path), $1, $2 - аргументы правила проверки.
     *
     * @param rule проверяемое правило
     * @return экземпляр {@link RuleError} если правило не прошло проверку иначе {@link RuleError#NO_ERROR}
     * @throws CompilationFailedException при ошибках компиляции
     */
    RuleError check(Rule rule) throws CompilationFailedException {
        Logger mainLogger = Log.getLogger("main");
        RuleError result = RuleError.NO_ERROR;

        Object jsonpathValue;
        try {
            jsonpathValue = getPathValue(rule.getJsonpath(), rule.isAsList());
        } catch (Exception e) {
            mainLogger.error("Ошибка при разборе строки по выражению |" + rule.getJsonpath() + "|: ", e);
            result = new RuleError(RULE_VIOLATION.code(), "Ошибка при разборе строки");
            return result;
        }

        String matcherArg1 = rule.getMatcherArg1();
        String matcherArg2 = rule.getMatcherArg2();
        String arg1 = Utils.isEmpty(matcherArg1)
                ? null
                : (matcherArg1.startsWith("$.") ? String.valueOf(getPathValue(matcherArg1, false)) : matcherArg1);
        String arg2 = Utils.isEmpty(matcherArg2)
                ? null
                : (matcherArg2.startsWith("$.") ? String.valueOf(getPathValue(matcherArg1, false)) : matcherArg2);
        String matcherExpr = rule.getMatcher().replaceAll("\\$1", arg1);
        matcherExpr = matcherExpr.replaceAll("\\$2", arg2);

        Binding binding = new Binding();
        binding.setVariable("testableValue", jsonpathValue);
        GroovyShell shell = new GroovyShell(binding);
        String groovyScript = "import static org.hamcrest.Matchers.*\n return " + matcherExpr
                + ".matches(testableValue)\n";

        Throwable catched = null;
        Object evaluate = null;
        try {
            evaluate = shell.evaluate(groovyScript);

        } catch (Throwable t) {
            catched = t;
            mainLogger.error(
                    "проверяемое значение: " + jsonpathValue
                            + (jsonpathValue != null ? (", тип " + jsonpathValue.getClass().getName()) : "")
                            + "\n" + groovyScript + " throws:\n",
                    t);

        } finally {
            if (catched == null) {
                mainLogger.trace("проверяемое значение: " + jsonpathValue
                        + (jsonpathValue != null ? (", тип " + jsonpathValue.getClass().getName())
                        : ""));
                mainLogger.trace(groovyScript + " returns " + evaluate);
            }
        }

        boolean checkResult = Boolean.parseBoolean(String.valueOf(evaluate));

        if (!checkResult) {
            // собрать сообщение об ошибке
            String errorMsg = rule.getErrorMsg();
            errorMsg = errorMsg.replaceAll("\\$value", String.valueOf(jsonpathValue));
            errorMsg = errorMsg.replaceAll("\\$1", arg1);
            errorMsg = errorMsg.replaceAll("\\$2", arg2);
            result = new RuleError(RULE_VIOLATION.code(), errorMsg);
        }
        return result;
    }

    /**
     * Возвращает значение переданого правила.
     *
     * @param rule   правило в виде строки
     * @param asList флаг, является ли значение переданого правила списком
     * @return значение переданого правилу
     */
    private Object getPathValue(String rule, boolean asList) {
        Object read = getDocumentContext().read(rule);
        Object result;
        if ((read instanceof List) && !((Collection) read).isEmpty() && !asList) {
            @SuppressWarnings({"unchecked", "rawtypes"})
            List list = (List) read;
            result = list.get(0);
        } else {
            result = read;
        }
        return result;
    }

    /**
     * @return the documentContext
     */
    private DocumentContext getDocumentContext() {
        if (Utils.isEmpty(this.json)) {
            throw new IllegalStateException("Не объявлена разбираемая строка json");
        }
        if (documentContext == null) {
            documentContext = parseContext.parse(json);
        }
        return documentContext;
    }

    /**
     * @param json the json to set
     */
    private void setJson(String json) {
        this.json = json;
        this.documentContext = null;
    }
}
