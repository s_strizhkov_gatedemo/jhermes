package ru.verna.jhermes.model.rules;

/**
 * Бизнес-правило (таблица rules).
 */
public class Rule {
    private String ruleId;
    private String jsonpath;
    private boolean asList;
    private String matcher;
    private String matcherArg1;
    private String matcherArg2;
    private String errorMsg;

    /**
     * @return the ruleId
     */
    public String getRuleId() {
        return ruleId;
    }

    /**
     * @param ruleId the ruleId to set
     */
    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    /**
     * @return the jsonpath
     */
    public String getJsonpath() {
        return jsonpath;
    }

    /**
     * @param jsonpath the jsonpath to set
     */
    public void setJsonpath(String jsonpath) {
        this.jsonpath = jsonpath;
    }

    /**
     * @return the asList
     */
    public boolean isAsList() {
        return asList;
    }

    /**
     * @param asList the asList to set
     */
    public void setAsList(boolean asList) {
        this.asList = asList;
    }

    /**
     * @return the matcher
     */
    public String getMatcher() {
        return matcher;
    }

    /**
     * @param matcher the matcher to set
     */
    public void setMatcher(String matcher) {
        this.matcher = matcher;
    }

    /**
     * @return the matcherArg1
     */
    public String getMatcherArg1() {
        return matcherArg1;
    }

    /**
     * @param matcherArg1 the matcherArg1 to set
     */
    public void setMatcherArg1(String matcherArg1) {
        this.matcherArg1 = matcherArg1;
    }

    /**
     * @return the matcherArg2
     */
    public String getMatcherArg2() {
        return matcherArg2;
    }

    /**
     * @param matcherArg2 the matcherArg2 to set
     */
    public void setMatcherArg2(String matcherArg2) {
        this.matcherArg2 = matcherArg2;
    }

    /**
     * @return the errorMsg
     */
    public String getErrorMsg() {
        return errorMsg;
    }

    /**
     * @param errorMsg the errorMsg to set
     */
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

}
