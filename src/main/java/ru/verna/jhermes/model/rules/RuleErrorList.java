package ru.verna.jhermes.model.rules;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Список нарушений правил проверки.
 *
 * @see RuleError
 */
public class RuleErrorList extends ArrayList<RuleError> {
    static final long serialVersionUID = 1L;

    @Override
    public boolean add(RuleError e) {
        if (e != null && !RuleError.NO_ERROR.equals(e)) {
            return super.add(e);
        }
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends RuleError> collection) {
        boolean result = false;
        for (RuleError next : collection) {
            result = add(next) || result;
        }
        return result;
    }

    @Override
    public void add(int index, RuleError element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, Collection<? extends RuleError> c) {
        throw new UnsupportedOperationException();
    }

    /**
     * Возвращает сообщение о нарушенных правилах.
     *
     * @return сообщение о нарушенных правилах в виде xml-строки
     */
    public String getErrorsMessage() {
        StringBuffer result = new StringBuffer("<data>\n");
        for (RuleError error : this) {
            result.append("<error>\n")
                    .append("<code>")
                    .append(error.getCode())
                    .append("</code>\n")
                    .append("<text>")
                    .append(error.getText())
                    .append("</text>\n")
                    .append("</error>\n");
        }
        result.append("</data>");
        return result.toString();
    }
}
