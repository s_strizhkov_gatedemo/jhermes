package ru.verna.jhermes.model;

import ru.verna.commons.Utils;

/**
 * Действие шлюза (таблица actions).
 */
public class Action {
    private String id;
    private String type;
    private String dependant;
    private boolean flattenParams;
    private Integer logsize;

    /**
     * Идентификатор действия.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Идентификатор действия.
     *
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Тип.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Тип.
     *
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Поле dependant.
     *
     * @return the dependant
     */
    public String getDependant() {
        return dependant;
    }

    /**
     * Поле dependant.
     *
     * @param dependant the dependant to set
     */
    public void setDependant(String dependant) {
        this.dependant = dependant;
    }

    /**
     * Метка о том, что действие ожидает на вход плоскую структуру параметров.
     *
     * @return the flattenParams
     */
    public boolean isFlattenParams() {
        return flattenParams;
    }

    /**
     * Метка о том, что действие ожидает на вход плоскую структуру параметров.
     *
     * @param flattenParams the flattenParams to set
     */
    public void setFlattenParams(boolean flattenParams) {
        this.flattenParams = flattenParams;
    }

    /**
     * Ограничение журналируемого сообщения для борьбы с слишком длинными. Если Х &lt; 0, будут зажурналированы
     * последние Х символов. Если Х &gt; 0, будут зажурналированы первые Х символов.
     *
     * @return the logsize
     */
    public int getLogsize() {
        return logsize == null ? 0 : logsize;
    }

    /**
     * Ограничение журналируемого сообщения для борьбы с слишком длинными. Если Х &lt; 0, будут зажурналированы
     * последние Х символов. Если Х &gt; 0, будут зажурналированы первые Х символов.
     *
     * @param logsize the logsize to set
     */
    public void setLogsize(Integer logsize) {
        this.logsize = logsize;
    }

    /**
     * Возвращает true, если {@link #logsize} == null.
     *
     * @return true, если {@link #logsize} == null, иначе false.
     */
    public boolean isLogsizeNull() {
        return logsize == null;
    }

    @Override
    public String toString() {
        return String.format("Action[id=%s, type=%s, dependant=%s]",
                this.getId(), this.getType(), this.getDependant());
    }

    /**
     * Проверяем заполнено ли поле id, если пустое, считаем, что данное действие не найдено или не доступно данному пользователю.
     *
     * @return {@code true} если поле id пустое, иначе {@code false}
     */
    public boolean isNotAllowed() {
        return Utils.isEmpty(id);
    }
}
