package ru.verna.jhermes.model;

import ch.qos.logback.classic.Level;
import org.slf4j.Logger;
import org.w3c.dom.Node;
import ru.verna.commons.Utils;
import ru.verna.commons.XmlUtils;
import ru.verna.commons.log.Log;
import ru.verna.commons.model.ActionParam;
import ru.verna.jhermes.kiasclient.KiasClient;
import ru.verna.jhermes.kiasclient.RequestMaker;
import javax.xml.xpath.XPathExpressionException;
import static ru.verna.commons.XmlUtils.dataResultSidXpath;
import static ru.verna.commons.model.Constants.*;

/**
 * Класс, обеспечивающий работу с Sid (сессиями сервиса КИАС).
 * <br>
 * В один момент времени для одного пользователя одного приложения сервиса КИАС (таблицы kiasappusers и kiasapps) может
 * быть активной только одна сессия.
 * <br>
 * Перед выдачей номера сессии класс проверяет активность сессии, в случае неактивности запрашивает и сохраняет новый
 * номер сессии.
 */
public class SidProvider {

    /**
     * Пока что sid один, т&#46;к&#46; пользователь только один.
     *
     * @param user
     * @return
     */
    private String sid;

    /**
     * Конструктор.
     */
    public SidProvider() {
        Log.getLogger("ws").trace("SidProvider создан");
    }

    /**
     * Возвращает параметр с Sid'ом для вставки в запросы.
     *
     * @param user пользователь
     * @return параметр с Sid'ом для вставки в запросы
     */
    public ActionParam getSidParam(User user) {
        return new ActionParam(SID_PARAMETER_NAME, SID_PARAMETER_NAME, getSid(user));
    }

    /**
     * Выдает номер сессии для пользователя user.
     *
     * @param user пользователь (привязанный к приложению сервиса)
     * @return номер сессии для переданного пользователя
     */
    public String getSid(User user) {
        int kiasAppId = user.getKiasAppId();
        RequestMaker requestMaker = RequestMaker.newRequest(kiasAppId);
        boolean sessionExpired = false;
        if (Utils.notEmpty(sid)) {
            sessionExpired = checkSession(requestMaker);
        } else {
            Log.getLogger("ws").trace("Sid is null or empty, needs to login");
            getAuthorization(user, requestMaker);
        }
        if (sessionExpired) {
            getAuthorization(user, requestMaker);
        }
        return sid;
    }

    /**
     * Проверяет текущее состояние КИАС сессии.
     *
     * @param requestMaker объект {@link RequestMaker} с установленым kiasAppId
     * @return {@code true} если текущая сессия истекла, иначе {@code false}
     */
    private boolean checkSession(RequestMaker requestMaker) {
        String checkSessionRequest = requestMaker.setRequestName(CHECKSESSION_METHOD_NAME)
                .addParam(new ActionParam(SID_PARAMETER_NAME, SID_PARAMETER_NAME, sid))
                .convertToString(false);
        Log.getLogger("ws").trace("отправка {}-запроса", CHECKSESSION_METHOD_NAME);

        Node checkSessionResponse = KiasClient.execProcXml(checkSessionRequest, Level.TRACE);

        String dataErrorCode = null;
        try {
            dataErrorCode = XmlUtils.dataErrorCodeXpath.evaluate(checkSessionResponse);
        } catch (XPathExpressionException ex) {
            Log.getLogger("main").info("Не получилось извлечь код ошибки из ответа КИАС, сессия не истекла", ex);
        }
        return "001".equalsIgnoreCase(dataErrorCode);
    }

    /**
     * Авторизует переданого пользователя в КИАС.
     *
     * @param user пользователь из запроса
     * @param requestMaker объект {@link RequestMaker} с установленым kiasAppId
     */
    private void getAuthorization(User user, RequestMaker requestMaker) {
        Logger wsLogger = Log.getLogger("ws");
        ActionParam usernameParam = new ActionParam(USERNAME_PARAMETER_NAME, USERNAME_PARAMETER_NAME,
                user.getKiasUsername());
        ActionParam passwordParam = new ActionParam(PASSWORD_PARAMETER_NAME, PASSWORD_PARAMETER_NAME,
                user.getKiasPassword());
        String authRequest = requestMaker.setRequestName(AUTH_METHOD_NAME).clearParams().addParam(usernameParam)
                .addParam(passwordParam).convertToString(false);
        wsLogger.trace("отправка {}-запроса", AUTH_METHOD_NAME);
        Node authResponse = KiasClient.execProcXml(authRequest, Level.TRACE, -100);
        try {
            String evaluatedSid = dataResultSidXpath.evaluate(authResponse);
            setSid(evaluatedSid);
        } catch (XPathExpressionException ex) {
            wsLogger.error(null, ex);
            setSid(null);
        }
    }

    /**
     * Сохранение Sid.
     *
     * @param sidParam the sidParam to set
     */
    private void setSid(String sidParam) {
        sid = sidParam;
        Log.getLogger("ws").debug("установлен Sid={}", sid);
    }
}
