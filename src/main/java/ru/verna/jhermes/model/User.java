package ru.verna.jhermes.model;

/**
 * Пользователь (таблица users).
 */
public class User {
    private int id;
    private String username;
    //private String password;
    //private Date expireDate;
    private String token;
    //private int kiasUserId;
    private int kiasAppId;
    private String kiasUsername;
    private String kiasPassword;

    /**
     * Конструктор.
     */
    public User() {
    }

    @Override
    public String toString() {
        return String.format("User[id=%s, kiasAppId=%s, kiasUsername=%s]", this.id, this.kiasAppId, this.kiasUsername);
    }

//    public User(int id, String username, String password) {
//        setId(id);
//        setUsername(username);
//        setPassword(password);
//    }
//
    /**
     * Идентификатор пользователя в БД.
     *
     * @return Идентификатор пользователя в БД
     */
    public int getId() {
        return id;
    }

    /**
     * Идентификатор пользователя в БД.
     *
     * @param id Идентификатор пользователя в БД
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }
//
//    /**
//     * @return the password
//     */
//    public String getPassword() {
//        return password;
//    }
//
//    /**
//     * @param password the password to set
//     */
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    /**
//     * @return the expireDate
//     */
//    public Date getExpireDate() {
//        return expireDate;
//    }
//
//    /**
//     * @param expireDate the expireDate to set
//     */
//    public void setExpireDate(Date expireDate) {
//        this.expireDate = expireDate;
//    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

//    /**
//     * @return the kiasUserId
//     */
//    public int getKiasUserId() {
//        return kiasUserId;
//    }
//
//    /**
//     * @param kiasUserId the kiasUserId to set
//     */
//    public void setKiasUserId(int kiasUserId) {
//        this.kiasUserId = kiasUserId;
//    }
    /**
     * @return the kiasAppId
     */
    public int getKiasAppId() {
        return kiasAppId;
    }

    /**
     * @param kiasAppId the kiasAppId to set
     */
    public void setKiasAppId(int kiasAppId) {
        this.kiasAppId = kiasAppId;
    }

    /**
     * @return the kiasUsername
     */
    public String getKiasUsername() {
        return kiasUsername;
    }

    /**
     * @param kiasUsername the kiasUsername to set
     */
    public void setKiasUsername(String kiasUsername) {
        this.kiasUsername = kiasUsername;
    }

    /**
     * @return the kiasPassword
     */
    public String getKiasPassword() {
        return kiasPassword;
    }

    /**
     * @param kiasPassword the kiasPassword to set
     */
    public void setKiasPassword(String kiasPassword) {
        this.kiasPassword = kiasPassword;
    }

}
