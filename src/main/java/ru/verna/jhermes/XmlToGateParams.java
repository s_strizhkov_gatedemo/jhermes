package ru.verna.jhermes;

import org.json.JSONObject;
import org.json.XML;
import ru.verna.commons.model.ParamStructure;

/**
 * Конвертирует переданный XML в список параметров для создания действий.
 */
final class XmlToGateParams {

    /**
     * Приватный конструктор.
     */
    private XmlToGateParams() {
    }

    /**
     * Main метод.
     *
     * @param args набор параметров
     */
    public static void main(String[] args) {
        getStructure().getFlatMap().keySet().forEach(System.out::println);
    }

    /**
     * Формирует из XML экземпляр {@link ParamStructure}.
     *
     * @return сформированный экземпляр {@link ParamStructure}
     */
    private static ParamStructure getStructure() {
        String rawData = "<?xml version=\"1.0\"?>\n"
                + "<AgreementCalc>\n"
                + "    <IsProlongation>0</IsProlongation>\n"
                + "    <IsAddendum>0</IsAddendum>\n"
                + "    <DateSign>30.11.2018 00:00:00</DateSign>\n"
                + "    <DateBeg>01.12.2018 00:00:00</DateBeg>\n"
                + "    <TimeBeg>30.12.1899 00:00:00</TimeBeg>\n"
                + "    <DateEnd>30.11.2019 00:00:00</DateEnd>\n"
                + "    <MaxDateBeg>29.01.2019 00:00:00</MaxDateBeg>\n"
                + "    <AddDateSign>30.11.2018 00:00:00</AddDateSign>\n"
                + "    <AddDateBeg>30.11.2018 00:00:00</AddDateBeg>\n"
                + "    <AddPremiumSum>0</AddPremiumSum>\n"
                + "    <AddPremiumSumStr>0,00 руб.</AddPremiumSumStr>\n"
                + "    <DriversRestriction>1</DriversRestriction>\n"
                + "    <PolicyKBM>13</PolicyKBM>\n"
                + "    <InsurancePremium>2059.00</InsurancePremium>\n"
                + "    <DateInsurancePremium>01.12.2018</DateInsurancePremium>\n"
                + "    <OSAGOKoef>\n"
                + "        <TB>4118.0</TB>\n"
                + "        <KT>1.0</KT>\n"
                + "        <KBM>0.5</KBM>\n"
                + "        <KO>1.0</KO>\n"
                + "        <KVS>1.0</KVS>\n"
                + "        <KM>1.0</KM>\n"
                + "        <KS>1.0</KS>\n"
                + "        <KN>1.0</KN>\n"
                + "    </OSAGOKoef>\n"
                + "    <PolicyOwner>\n"
                + "        <PhysicalPerson>\n"
                + "            <Resident>Y</Resident>\n"
                + "            <Sex>М</Sex>\n"
                + "            <Surname>МАЙБОРОДА</Surname>\n"
                + "            <Name>ЮРИЙ</Name>\n"
                + "            <Patronymic>АНАТОЛЬЕВИЧ</Patronymic>\n"
                + "            <BirthDate>02.11.1987</BirthDate>\n"
                + "            <PersonDocument>\n"
                + "                <DocPerson>20</DocPerson>\n"
                + "                <Serial>0307</Serial>\n"
                + "                <Number>686627</Number>\n"
                + "                <Date>23.11.2007</Date>\n"
                + "                <Issuer>Отделом УФМС России по Краснодарскому краю в Гулькевичском районе</Issuer>\n"
                + "                <IssuerCode>230-032</IssuerCode>\n"
                + "            </PersonDocument>\n"
                + "            <Address>\n"
                + "                <StrValue>Приморский край, г Артем, ул. АНГАРСКАЯ, д. 3, корп./стр. 2, кв. 20</StrValue>\n"
                + "                <RSACode>2500000300000</RSACode>\n"
                + "                <AreaKLADR>2500000300000</AreaKLADR>\n"
                + "                <ApartmentAddress>\n"
                + "                    <Street>Ангарская</Street>\n"
                + "                    <Home>3</Home>\n"
                + "                    <Building>2</Building>\n"
                + "                    <Flat>20</Flat>\n"
                + "                </ApartmentAddress>\n"
                + "            </Address>\n"
                + "        </PhysicalPerson>\n"
                + "    </PolicyOwner>\n"
                + "    <CAR>\n"
                + "        <Owner>\n"
                + "            <PhysicalPerson>\n"
                + "                <Resident>Y</Resident>\n"
                + "                <Sex>М</Sex>\n"
                + "                <Surname>МАЙБОРОДА</Surname>\n"
                + "                <Name>ЮРИЙ</Name>\n"
                + "                <Patronymic>АНАТОЛЬЕВИЧ</Patronymic>\n"
                + "                <BirthDate>02.11.1987</BirthDate>\n"
                + "                <PersonDocument>\n"
                + "                    <DocPerson>20</DocPerson>\n"
                + "                    <Serial>0307</Serial>\n"
                + "                    <Number>686627</Number>\n"
                + "                    <Date>23.11.2007</Date>\n"
                + "                    <Issuer>Отделом УФМС России по Краснодарскому краю в Гулькевичском районе</Issuer>\n"
                + "                    <IssuerCode>230-032</IssuerCode>\n"
                + "                </PersonDocument>\n"
                + "                <!-- Cведения об адресе -->\n"
                + "                <Address>\n"
                + "                    <StrValue>Приморский край, г Артем, ул. АНГАРСКАЯ, д. 3, корп./стр. 2, кв. 20</StrValue>\n"
                + "                    <RSACode>2500000300000</RSACode>\n"
                + "                    <AreaKLADR>2500000300000</AreaKLADR>\n"
                + "                    <ApartmentAddress>                        <!-- опциональный узел -->\n"
                + "                        <Street>Ангарская</Street>\n"
                + "                        <Home>3</Home>\n"
                + "                        <Building>2</Building>\n"
                + "                        <Flat>20</Flat>\n"
                + "                    </ApartmentAddress>\n"
                + "                </Address>\n"
                + "            </PhysicalPerson>\n"
                + "        </Owner>\n"
                + "        <CountryCar>1</CountryCar>\n"
                + "        <IsTransCar>0</IsTransCar>\n"
                + "        <TrailerKPR>0</TrailerKPR>\n"
                + "        <UseCar>1</UseCar>\n"
                + "        <UseCarName>Личная</UseCarName>\n"
                + "        <Rent>0</Rent>\n"
                + "        <LightBar>0</LightBar>\n"
                + "        <VIN>ОТСУТСТВУЕТ</VIN>\n"
                + "        <BodyNumber>KR27-0032835</BodyNumber>\n"
                + "        <LicensePlate>Е681АН125</LicensePlate>\n"
                + "        <MarkModelCarRSACode>395015225</MarkModelCarRSACode>\n"
                + "        <YearIssue>1995</YearIssue>\n"
                + "        <CatCar>B</CatCar>\n"
                + "        <DocCarId>30</DocCarId>\n"
                + "        <DocCarSerial>25ТЕ</DocCarSerial>\n"
                + "        <DocCarNumber>319983</DocCarNumber>\n"
                + "        <DocCarDate>28.07.2002</DocCarDate>\n"
                + "        <TicketCar>53</TicketCar>\n"
                + "        <TicketCarSerial></TicketCarSerial>\n"
                + "        <TicketCarNumber>065390041813558</TicketCarNumber>\n"
                + "        <TicketDiagnosticDate>2019-11-29</TicketDiagnosticDate>\n"
                + "        <EngCap>70</EngCap>\n"
                + "    </CAR>\n"
                + "    <DriversPart>\n"
                + "        <Driver>\n"
                + "            <IDCheck>1814432146</IDCheck>\n"
                + "            <Country>643</Country>\n"
                + "            <Surname>МАЙБОРОДА</Surname>\n"
                + "            <Name>ЮРИЙ</Name>\n"
                + "            <Patronymic>АНАТОЛЬЕВИЧ</Patronymic>\n"
                + "            <BirthDate>02.01.1951</BirthDate>\n"
                + "            <Experience>35</Experience>\n"
                + "            <DriverKBM>13</DriverKBM>\n"
                + "            <DriverDocument>\n"
                + "                <Serial>9901</Serial>\n"
                + "                <Number>548517</Number>\n"
                + "                <Date>23.08.1983</Date>\n"
                + "                <CategoryDriverLicense>B</CategoryDriverLicense>\n"
                + "            </DriverDocument>\n"
                + "        </Driver>\n"
                + "    </DriversPart>\n"
                + "</AgreementCalc>";
        JSONObject jsonObj = XML.toJSONObject(rawData);
        String jsonResult = jsonObj.toString(4);
        ParamStructure structure = new ParamStructure();
        structure.readJSON(jsonResult);
        return structure;
    }
}
