package ru.verna.jhermes;

import org.slf4j.Logger;
import ru.verna.commons.log.Log;
import ru.verna.commons.Utils;
import ru.verna.jhermes.actions.ActionExecutor;
import ru.verna.jhermes.model.User;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import static ru.verna.commons.model.Constants.ERRORCODE.ACTION_IS_NOT_SET;

/**
 * Основной сервлет приложения. Принимает после слэша имя действия, которое нужно выполнить.
 */
@WebServlet(name = "EntryServlet", urlPatterns = { "/*" })
public class EntryServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Logger mainLogger = Log.getLogger("main");

        if (mainLogger.isTraceEnabled()) {
            List<String> params = new LinkedList<>();
            request.getParameterMap().keySet().forEach((paramName) -> {
                params.add(String.format("|%s|=|%s|", paramName, request.getParameter(paramName)));
            });
            mainLogger.trace("Запущен, параметры запроса: {}", Arrays.toString(params.toArray()));
        }

        User user = Util.getUser(request);
        if (user == null) {
            throw new IllegalStateException("user is null");
        }

        String requestURI = request.getRequestURI();
        String contextPath = request.getContextPath();
        String actionId = requestURI.substring(contextPath.length());
        // убираем лишние слэши в начале имени действия (переход по адресу https://server/gate//actionname)
        mainLogger.trace("initial actionId = {}", actionId);
        while (actionId.startsWith("/") || actionId.startsWith("/")) {
            actionId = actionId.substring(1);
            mainLogger.trace("remove first symbol, now actionId = {}", actionId);
        }
        mainLogger.debug("actionId = {}", actionId);

        String result;
        if (Utils.isEmpty(actionId)) {
            result = Utils.getErrorMessage(ACTION_IS_NOT_SET.code(), "Действие не определено");

        } else {
            ActionExecutor actionExecutor = new ActionExecutor();
            result = actionExecutor.execute(request, actionId, null);
        }

        response.getWriter().print(result);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the
    // code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    // </editor-fold>

}
