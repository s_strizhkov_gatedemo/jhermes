package ru.verna.jhermes;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;
import javax.servlet.ServletRequest;
import org.slf4j.Logger;
import ru.verna.commons.log.Log;
import ru.verna.commons.model.Constants;
import ru.verna.jhermes.model.SidProvider;
import ru.verna.jhermes.model.User;

/**
 * Класс с методами общего назначения.
 */
public final class Util {
    private static String wsString = null;
    private static String wsHost = null;

    static {
        final String propertiesFile = "webservice.properties";
        Logger wsLogger = Log.getLogger("ws");
        try (InputStream appResourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(propertiesFile)) {
            Properties wsProperties = new Properties();
            wsLogger.trace("Чтение файла " + propertiesFile);
            wsProperties.load(appResourceAsStream);
            wsString = wsProperties.getProperty("url");

            try {
                URI wsUri = new URI(wsString);
                wsHost = wsUri.getHost();
            } catch (URISyntaxException e) {
                wsLogger.error("Ошибка при разборе адреса " + wsString + ": ", e);
                wsHost = null;
            }

        } catch (IOException e) {
            wsLogger.error("Ошибка при разборе файла " + propertiesFile + ": ", e);
            wsString = null;
            wsHost = null;

        } finally {
            wsLogger.trace("Установили адрес веб-службы КИАСа: " + wsString);
            wsLogger.trace("Установили хост веб-службы КИАСа: " + wsHost);
        }
    }

    /**
     * Закрытый конструктор.
     */
    private Util() {
    }

    /**
     * Возвращает хранимого пользователя.
     *
     * @param request запрос, по которому будет определен пользователь
     * @return User
     */
    public static User getUser(ServletRequest request) {
        return (User) request.getAttribute(Constants.USER_ENTITYNAME);
    }

    /**
     * Возвращает хранимый экземпляр SidProvider'а.
     *
     * @param request запрос
     * @return SidProvider
     */
    public static SidProvider getSidProvider(ServletRequest request) {
        return (SidProvider) request.getServletContext().getAttribute(Constants.SIDPROVIDER_ENTITYNAME);
    }

    /**
     * Возвращает адрес веб-службы KiasSvc (адрес веб-службы задается в файле webservice.properties).
     *
     * @return null, если properties-файл не был прочитан корректно
     */
    public static String getKiasSvcEndpoint() {
        return wsString;
    }

    /**
     * Возвращает хост веб-службы KiasSvc (адрес веб-службы задается в файле webservice.properties).
     *
     * @return null, если properties-файл не был прочитан корректно
     */
    public static String getKiasSvcHost() {
        return wsHost;
    }

    /**
     * Возвращает {@code true}, если хост доступен по порту.
     *
     * @param host хост
     * @param port любой открытый на этом хосте порт
     * @param timeOutMillis время ожидания отклика сервера в миллисекундах
     * @return {@code true}, если хост доступен по порту, иначе {@code false}
     */
    public static boolean isReachable(String host, int port, int timeOutMillis) {
        // Any Open port on other machine
        // openPort =  22 - ssh, 80 or 443 - webserver, 25 - mailserver etc.
        try {
            try (Socket soc = new Socket()) {
                soc.connect(new InetSocketAddress(host, port), timeOutMillis);
            }
            return true;
        } catch (Throwable ex) {
            //ex.printStackTrace();
            return false;
        }
    }
}
