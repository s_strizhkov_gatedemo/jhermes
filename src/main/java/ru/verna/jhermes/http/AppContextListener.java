package ru.verna.jhermes.http;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import ru.verna.commons.Utils;
import ru.verna.commons.log.Log;
import ru.verna.commons.log.ThreadUtil;
import ru.verna.commons.mail.CommonMailClient;
import ru.verna.commons.model.Constants;
import ru.verna.jhermes.actions.logic.services.CheckSessionByDelay;
import ru.verna.jhermes.model.SidProvider;

/**
 * Web application lifecycle listener.
 * <br>
 * Приложение не работает с сессиями, поэтому храним нужные экземпляры в контексте приложения.
 */
public class AppContextListener implements ServletContextListener, ServletRequestListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        Log.getLogger("main").trace("вызван");

        setMailSystemProperties();

        ServletContext servletContext = sce.getServletContext();
        SidProvider sidProvider = (SidProvider) servletContext.getAttribute(Constants.SIDPROVIDER_ENTITYNAME);
        if (sidProvider == null) {
            sidProvider = new SidProvider();
            servletContext.setAttribute(Constants.SIDPROVIDER_ENTITYNAME, sidProvider);
        }

        runRepetitiveTask();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        Log.getLogger("main").trace("вызван");
        ServletContext servletContext = sce.getServletContext();
        servletContext.removeAttribute(Constants.SIDPROVIDER_ENTITYNAME);
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        String txNumber = String.valueOf(Utils.getRandomLong());
        String callContext = ((HttpServletRequest) sre.getServletRequest()).getHeader(Log.CALL_CONTEXT_HEADER);
        ThreadUtil.setThreadVariable(Log.USERNAME, Log.UNKNOWN_USER);
        ThreadUtil.setThreadVariable(Log.TX_NUMBER, txNumber);
        ThreadUtil.setThreadVariable(Log.CALL_CONTEXT, callContext);
    }

    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        ThreadUtil.destroy();
    }

    /**
     * Создаем в потоке-демоне автобудилку КИАСа.
     */
    private void runRepetitiveTask() {
        Thread daemonThread = new Thread(new CheckSessionByDelay());
        daemonThread.setDaemon(true);
        daemonThread.start();
    }

    /**
     * Установка нужных системных свойств (properties) для отправки писем.
     * Настройки используются logback-аппендером ch.qos.logback.classic.net.SMTPAppender.
     */
    private void setMailSystemProperties() {
        final String propName = "mail.smtp.ssl.trust";
        String propValue = CommonMailClient.getProperty(propName);
        System.setProperty(propName, propValue);
        Log.getLogger("main").info("Set system property '{}' to '{}'", propName, propValue);
    }

}
