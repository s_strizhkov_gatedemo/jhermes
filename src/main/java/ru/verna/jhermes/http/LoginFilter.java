package ru.verna.jhermes.http;

import org.slf4j.Logger;
import ru.verna.commons.DbUtils;
import ru.verna.commons.Utils;
import ru.verna.commons.http.SkippableFilter;
import ru.verna.commons.log.Log;
import ru.verna.commons.log.ThreadUtil;
import ru.verna.commons.model.Constants;
import ru.verna.jhermes.model.User;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import static ru.verna.commons.model.Constants.ERRORCODE.AUTH_TOKEN_NOT_FOUND;
import static ru.verna.commons.model.Constants.ERRORCODE.USER_NOT_FOUND;

/**
 * Обеспечение авторизации на каждом запросе.
 */
public class LoginFilter extends SkippableFilter {
    /**
     * Конструктор.
     */
    public LoginFilter() {
    }

    /**
     * Каждый запрос имеет токен авторизации, по нему определяем пользователя системы и пользователя сервиса КИАС, через
     * которого будет идти запрос в сервис.
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        Logger mainLogger = Log.getLogger("main");
        Logger dbLogger = Log.getLogger("db");

        boolean skipRequest = skipRequest(httpServletRequest.getRequestURI());

        if (skipRequest) {
            chain.doFilter(request, response);
        } else {
            String authorizationToken = null;
            String authorizationHeader = httpServletRequest.getHeader("Authorization");
            if (Utils.notEmpty(authorizationHeader)) {
                String[] splitted = authorizationHeader.split(" ");
                if (splitted.length == 2 && "Bearer".equalsIgnoreCase(splitted[0])) {
                    authorizationToken = splitted[1];
                }
            } else {
                authorizationToken = httpServletRequest.getParameter("token");
            }

            if (Utils.notEmpty(authorizationToken)) {
                String usersSql = "SELECT u.id, u.username as name, kau.app_id, kau.username, kau.password "
                        + "FROM env.kiasappusers kau, users u "
                        + "WHERE kau.id = u.kiasuser_id AND u.active = true "
                        + "AND u.token = ? and u.expiredate >= ?";
                try (Connection conn = DbUtils.getConnection();
                        PreparedStatement ps = conn.prepareStatement(usersSql)) {
                    ps.setString(1, authorizationToken);
                    Date date = new java.sql.Date(getToday());
                    ps.setDate(2, date);
                    dbLogger.debug("|{}|, args: token={}, today={}", usersSql, authorizationToken, date);
                    ResultSet rs = ps.executeQuery();
                    if (rs.next()) {
                        int userId = rs.getInt("id");
                        int kiasAppId = rs.getInt("app_id");
                        String username = rs.getString("name");
                        String kiasUsername = rs.getString("username");
                        String kiasPassword = rs.getString("password");
                        dbLogger.debug("returned row: id={}, kiasAppId={}, kiasUsername={}, kiasPassword={}{}",
                                userId, kiasAppId, kiasUsername, kiasPassword.substring(0, 5), "*****");

                        User user = new User();
                        user.setId(userId);
                        user.setUsername(username);
                        user.setToken(authorizationToken);
                        user.setKiasAppId(kiasAppId);
                        user.setKiasUsername(kiasUsername);
                        user.setKiasPassword(kiasPassword);
                        mainLogger.debug("Создан пользователь {}", user);
                        request.setAttribute(Constants.USER_ENTITYNAME, user);
                        ThreadUtil.setThreadVariable(Log.USERNAME, getUserNameForLog(request, username));

                        // есть активный пользователь с таким токеном, все хорошо
                        chain.doFilter(request, response);

                    } else {
                        // нет такого пользователя
                        dbLogger.debug("returns nothing");
                        String result = Utils.getErrorMessage(USER_NOT_FOUND.code(),
                                "Пользователь не найден или не активен");
                        response.getWriter().write(result);
                    }
                } catch (SQLException e) {
                    mainLogger.error(null, e);
                    String result = Utils.getErrorMessage(e.getErrorCode(),
                            e.getLocalizedMessage() + ". Пожалуйста, обратитесь к администратору системы.");
                    response.getWriter().write(result);
                }

            } else {
                // токен не определен
                String result = Utils.getErrorMessage(AUTH_TOKEN_NOT_FOUND.code(), "Токен авторизации не определен");
                response.getWriter().write(result);
            }
        }
    }

    /**
     * Проверяем передано ли в заголовках запроса имя пользователя и если да то добавляем его для логирования.
     *
     * @param request входящий запрос
     * @param username имя пользователя под которым выполняются запросы в КИАС
     * @return объединненое имя при наличиии в заголовках или просто переданное имя пользователя
     */
    private String getUserNameForLog(ServletRequest request, String username) {
        String requestUsername = ((HttpServletRequest) request).getHeader(Log.USERNAME);
        try {
            return Utils.notEmpty(requestUsername)
                    ? username + "-" + URLDecoder.decode(requestUsername, StandardCharsets.UTF_8.toString()) : username;
        } catch (UnsupportedEncodingException e) {
            Log.getLogger("main").warn("Невозможно декодировать текст [{}] в заголовке: [{}]", requestUsername,
                    Log.USERNAME);
        }
        return username;
    }

    /**
     * Возвращает текущую дату со временем время 23:59:59.
     *
     * @return текущую дату со временем время 23:59:59
     */
    private long getToday() {
        Calendar cld = Calendar.getInstance();
        // CHECKSTYLE:OFF
        cld.set(Calendar.AM_PM, 0);
        cld.set(Calendar.HOUR, 23);
        cld.set(Calendar.MINUTE, 59);
        cld.set(Calendar.SECOND, 59);
        // CHECKSTYLE:ON
        return cld.getTimeInMillis();
    }
}
