package ru.verna.jhermes;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

/**
 * Утилитарный класс для разработчиков. Конвертер json &lt;-&gt; xml.
 */
public final class XmlJsonConverter {

    private static final int PRETTY_PRINT_INDENT_FACTOR = 4;

    /**
     * Закрытый конструктор.
     */
    private XmlJsonConverter() {

    }

    // CHECKSTYLE:OFF
    public static void main(String[] args) {
        try {
            method3();

        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    private static void method2() throws JSONException {
        String json = "{\"address\": {\n"
                + "            \"region\": {\n"
                + "                \"custom_value\": \"г Москва\",\n"
                + "                \"name\": \"Москва\",\n"
                + "                \"abbr\": \"г\",\n"
                + "                \"code\": \"7700000000000\"\n"
                + "            },\n"
                + "            \"district\": {\n"
                + "                \"custom_value\": \"\"\n"
                + "            },\n"
                + "            \"city\": {\n"
                + "                \"custom_value\": \"г Москва\",\n"
                + "                \"name\": \"Москва\",\n"
                + "                \"abbr\": \"г\",\n"
                + "                \"code\": \"7700000000000\"\n"
                + "            },\n"
                + "            \"locality\": {\n"
                + "                \"custom_value\": \"\"\n"
                + "            },\n"
                + "            \"street\": {\n"
                + "                \"custom_value\": \"ул Тверская\",\n"
                + "                \"name\": \"Тверская\",\n"
                + "                \"abbr\": \"ул\",\n"
                + "                \"code\": \"77000000000287700\"\n"
                + "            },\n"
                + "            \"house\": \"12\",\n"
                + "            \"building\": \"\",\n"
                + "            \"flat\": \"\",\n"
                + "            \"postcode\": \"125009\"\n"
                + "        }}";
        String xml = jsonToXml(json);
        System.out.println(xml);
    }

    private static void method1() throws JSONException {
        String xml = "<?xmlversion=\"1.0\"encoding=\"UTF-8\"?>\n"
                + "<data>\n"
                + "    <request>\n"
                + "        <params>\n"
                + "            <productId>ucProductMyApartment3600</productId>\n"
                + "            <premium></premium>\n"
                + "            <calcId></calcId>\n"
                + "            <isn></isn>\n"
                + "            <promocode></promocode>\n"
                + "            <term>\n"
                + "                <begin>26.06.2018</begin>\n"
                + "                <end>25.06.2019</end>\n"
                + "            </term>\n"
                + "            <attributes>\n"
                + "                <attribute>\n"
                + "                    <id>ucAddAtrNSCoveragePeriod</id>\n"
                + "                    <value>1329041</value>\n"
                + "                </attribute>\n"
                + "            </attributes>\n"
                + "            <insurer>\n"
                + "                <isn></isn>\n"
                + "                <birthDate>01.01.1980</birthDate>\n"
                + "                <firstName>20=</firstName>\n"
                + "                <middleName>20=28G</middleName>\n"
                + "                <lastName>20>2</lastName>\n"
                + "                <phone>+79381111111</phone>\n"
                + "                <email>verna@test.ru</email>\n"
                + "                <gender></gender>\n"
                + "                <documents>\n"
                + "                    <document>\n"
                + "                        <type>ucRFPassport</type>\n"
                + "                        <series>0715</series>\n"
                + "                        <number>111111</number>\n"
                + "                        <date>02.01.1998</date>\n"
                + "                        <issuedBy>#$!</issuedBy>\n"
                + "                    </document>\n"
                + "                </documents>\n"
                + "            </insurer>\n"
                + "            <insuranceObject>\n"
                + "                <address>\n"
                + "                    <region>\n"
                + "                        <custom_value>3A:20</custom_value>\n"
                + "                        <name>A:20</name>\n"
                + "                        <abbr>3</abbr>\n"
                + "                        <code>7700000000000</code>\n"
                + "                    </region>\n"
                + "                    <district>\n"
                + "                        <custom_value></custom_value>\n"
                + "                    </district>\n"
                + "                    <city>\n"
                + "                        <custom_value>3A:20</custom_value>\n"
                + "                        <name>A:20</name>\n"
                + "                        <abbr>3</abbr>\n"
                + "                        <code>7700000000000</code>\n"
                + "                    </city>\n"
                + "                    <locality>\n"
                + "                        <custom_value></custom_value>\n"
                + "                    </locality>\n"
                + "                    <street>\n"
                + "                        <custom_value>C;\"25@A:0O</custom_value>\n"
                + "                        <name>\"25@A:0O</name>\n"
                + "                        <abbr>C;</abbr>\n"
                + "                        <code>77000000000287700</code>\n"
                + "                    </street>\n"
                + "                    <house>12</house>\n"
                + "                    <building></building>\n"
                + "                    <flat></flat>\n"
                + "                    <postcode></postcode>\n"
                + "                </address>\n"
                + "            </insuranceObject>\n"
                + "            <clauses>\n"
                + "                <clause>\n"
                + "                    <id>ucClausesCommonSalesChannel</id>\n"
                + "                    <value>ucClausesCommonSalesChannel_Online</value>\n"
                + "                </clause>\n"
                + "            </clauses>\n"
                + "        </params>\n"
                + "    </request>\n"
                + "</data>";
        String jsonPrettyPrintString = xmlToJson(xml);
        System.out.println(jsonPrettyPrintString);
    }

    private static void method3() throws Exception {
        String xml = "<subject>\n"
                + "    <isn>идентификатор контрагента</isn>\n"
                + "    <juridical>y|n , юрик/физик</juridical>\n"
                + "    <shortName>ООО КАСКАД|Фамилия И.О.</shortName>    \n"
                + "    <fullName>Общество с ограниченной ответственностью \"КАСКАД\" |Фамилия Имя Отчество</fullName>    \t\n"
                + "    <nameLat>Familia I.O.</nameLat>\n"
                + "    <firstName>Имя</firstName>\n"
                + "    <middleName>Отчество</middleName>\n"
                + "    <lastName>Фамилия</lastName>\n"
                + "    <birthDate>31.12.2009</birthDate>\n"
                + "    <birthPlace>Место рождения (строка до 255 символов)</birthPlace>\n"
                + "    <sex>М</sex>\n"
                + "    <resident>y|n</resident>\n"
                + "    <vip>y|n|null : vip | anti-vip | обычный</vip>\n"
                + "    <inn>ИНН</inn>\n"
                + "    <kpp>КПП</kpp>\n"
                + "    <iin>ИИН/БИН для РК</iin>\n"
                + "    <okpo>Код ОКПО</okpo>\n"
                + "    <ogrn>Код ОГРН</ogrn>\n"
                + "    <okvd>Код ОКВЭД</okvd>\n"
                + "    <okvdIsn>Основной вид экономической деятелььности (из справочника)</okvdIsn>\n"
                + "    <orgFormIsn>Организационно-правовая форма, справочник ConstName = 'ucOrgForm'</orgFormIsn>\n"
                + "    <parentIsn>Ссылка на головную фирму (FK_SUBJECT)</parentIsn>\n"
                + "    <countryIsn>Страна из справочника</countryIsn>\n"
                + "    <familyStateIsn>Семейное положение (из справочника)</familyStateIsn>\n"
                + "    <educationIsn>Образование (из справочника)</educationIsn>\n"
                + "    <professionIsn>Профессия (из справочника)</professionIsn>\n"
                + "    <remark>Примечание</remark>\n"
                + "    <!-- Адреса -->\n"
                + "    <addresses>\n"
                + "        <address>\n"
                + "            <type>Тип адреса, справочник 'ucAddressType'</type>\n"
                + "            <region><!-- субъект РФ -->\n"
                + "                <code>кладровский код</code>\n"
                + "                <name>кладровское имя</name>\n"
                + "                <abbr>аббревиатура</abbr>\n"
                + "                <custom_value>всегда непустое</custom_value>\n"
                + "            </region>\n"
                + "            <district><!-- район субъекта РФ -->\n"
                + "                <code>кладровский код</code>\n"
                + "                <name>кладровское имя</name>\n"
                + "                <abbr>аббревиатура</abbr>\n"
                + "                <custom_value>всегда непустое</custom_value>\n"
                + "            </district>\n"
                + "            <city><!-- город -->\n"
                + "                <code>7700000000000</code>\n"
                + "                <custom_value>г Москва</custom_value>\n"
                + "                <name>Москва</name>\n"
                + "                <abbr>г</abbr>\n"
                + "            </city>\n"
                + "            <locality><!-- населенный пункт внутри города -->\n"
                + "                <code></code>\n"
                + "                <custom_value>г Москва</custom_value>\n"
                + "                <name>Москва</name>\n"
                + "                <abbr>г</abbr>\n"
                + "            </locality>\n"
                + "            <street><!-- улица -->\n"
                + "                <code>77000000000287700</code>\n"
                + "                <custom_value>ул Тверская</custom_value>\n"
                + "                <name>Тверская</name>\n"
                + "                <abbr>ул</abbr>\n"
                + "            </street>\n"
                + "            <house>дом</house>\n"
                + "            <building>корпус</building>\n"
                + "            <flat>квартира</flat>\n"
                + "            <postcode>почтовый индекс</postcode>\n"
                + "        </address>\n"
                // + " <address>...</address>\n"
                + "    </addresses>\n"
                + "    <!-- Документы -->\n"
                + "    <documents>\n"
                + "        <document>\n"
                + "            <type>Тип документа, справочник 'ucDocClass'</type><!-- ucRFPassport = паспорт РФ -->\n"
                + "            <series>0405</series>\n"
                + "            <number>053911</number>\n"
                + "            <date>16.12.2005</date>\n"
                + "            <issuedBy>УВД Октябрьского р-на г.Красноярска</issuedBy>\n"
                + "        </document>\n"
                // + " <document>...</document>\n"
                + "    </documents>\n"
                + "    <!-- Контактные данные -->\n"
                + "    <contacts>\n"
                + "        <mobile>+79991234567</mobile>\n"
                + "        <home>+71234567890</home>\n"
                + "        <work>+12345678901</work>\n"
                + "\t\t<contact>+72345678902</contact>\n"
                + "        <email>e.mail@domain.domain</email>\n"
                + "        <site>http://www.site.ru/</site>\n"
                + "    </contacts>\n"
                + "    <!-- Банковские счета -->\n"
                + "    <accounts>\n"
                + "        <account>\n"
                + "            <isn>идентификатор</isn>\n"
                + "            <orderno>порядковый номер (предпочтительность в списке счетов)</orderno>\n"
                + "            <bank>\n"
                + "                <bik>БИК банка, обязательное поле</bik>\n"
                + "                <name>Наименование банка</name>\n"
                + "                <inn>ИНН банка</inn>\n"
                + "                <kpp>КПП банка</kpp>\n"
                + "                <corraccount>Корреспондентский счет банка</corraccount>\n"
                + "            </bank>\n"
                + "            <payaccount>Расчетный счет контрагента</payaccount>\n"
                + "        </account>\n"
                // + " <account>...</account>\n"
                + "    </accounts>\n"
                + "</subject>";
        String jsonPrettyPrintString = xmlToJson(xml);
        System.out.println(jsonPrettyPrintString);
    }
    // CHECKSTYLE:ON

    /**
     * Преобразовывает xml в json.
     *
     * @param xml строка xml, которую надо преобразовать в json
     * @return json
     * @throws JSONException в случае проблемы с json
     */
    private static String xmlToJson(String xml) throws JSONException {
        JSONObject jsonObj = XML.toJSONObject(xml);
        String jsonResult = jsonObj.toString(PRETTY_PRINT_INDENT_FACTOR);
        return jsonResult;
    }

    /**
     * Преобразовывает json в xml.
     *
     * @param json строка json, которую надо преобразовать в xml
     * @return xml
     * @throws JSONException в случае проблемы с json
     */
    private static String jsonToXml(String json) throws JSONException {
        JSONObject jsonObj = new JSONObject(json);
        String xmlResult = XML.toString(jsonObj);
        return xmlResult;
    }

}
