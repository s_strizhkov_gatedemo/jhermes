package ru.verna.jhermes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.verna.commons.log.Log;
import ru.verna.commons.Utils;
import java.util.Iterator;
import java.util.Map;

/**
 *
 */
public final class HttpUtil {

    /**
     * Закрытый конструктор.
     */
    private HttpUtil() {
    }

    /**
     * Возвращает плоскую коллекцию c регистро-независимым поиском по ключу и значениями-строками вместо массивов.
     *
     * @param sourceMap map, из которого берется наполнение выдаваемой карты.
     * @return карту c регистро-независимым поиском по ключу
     */
    public static Map<String, String> getCaseInsensitiveParameterMap(Map<String, String[]> sourceMap) {
        Map<String, String> map = Utils.newCaseInsensitiveMap();
        if (sourceMap != null) {
            if (sourceMap.containsKey("params")) {
                // режим json: параметры пришли в json-структуре под именем params
                String params = sourceMap.get("params")[0];
                try {
                    JSONObject paramsAsJSON = new JSONObject(params);
                    fillMap(map, "", paramsAsJSON);
                } catch (JSONException e) {
                    Log.getLogger("main").warn("Неверная json-структура в параметре params |{}|", params);
                }

            } else {
                // стандартный режим: параметры передаются линейным списком
                sourceMap.keySet().forEach(
                        (String key) -> {
                            String[] val = sourceMap.get(key);
                            map.put(key, val.length > 0 ? val[0] : null);
                        });
            }
        }
        return map;
    }

    /**
     * Разбирает json-структуру параметров в плоскую коллекцию Map&lt;String, String&gt;.
     *
     * @param map коллекция, куда заливаются пришедшие параметры
     * @param parentPrefix префикс для определения полного пути параметра в структуре. В первоначальном вызове должен
     *            равняться пустой строке.
     * @param jsonObject json-структура с параметрами
     */
    private static void fillMap(Map<String, String> map, String parentPrefix, JSONObject jsonObject) {
        Iterator<String> keys = jsonObject.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            Object value = null;
            try {
                value = jsonObject.get(key);
            } catch (JSONException e) {
            }
            if (value instanceof JSONObject) {
                fillMap(map, parentPrefix + key + "-", (JSONObject) value);

            } else if (value instanceof JSONArray) {
                throw new UnsupportedOperationException("Во входных json-параметрах найден массив");

            } else {
                String valueString = String.valueOf(value);
                Log.getLogger("main").trace("parameters map: put {} = {}", parentPrefix + key, valueString);
                map.put(parentPrefix + key, valueString);
            }
        }

    }

    /**
     * Возвращает карту c регистро-независимым поиском по ключу.
     *
     * @param sourceMap map, из которого берется наполнение выдаваемой карты.
     * @return карту c регистро-независимым поиском по ключу
     */
    public static Map<String, String> getCaseInsensitiveMap(Map<String, String> sourceMap) {
        Map<String, String> map = Utils.newCaseInsensitiveMap();
        if (sourceMap != null) {
            map.putAll(sourceMap);
        }
        return map;
    }

    /**
     * Возвращает значение аргумента obj, преобразуя его к типу String. Метод предполагает, что аргумент может быть типа
     * String или String[].
     *
     * @param obj преобразуемый к String объект
     * @return значение аргумента obj, преобразуя его к типу String.
     */
    public static String getParameterAsString(Object obj) {
        if (obj == null) {
            return null;
        } else if (obj instanceof String[]) {
            String[] arr = (String[]) obj;
            if (arr.length > 0) {
                return arr[0];
            } else {
                return null;
            }
        } else if (obj instanceof String) {
            return (String) obj;
        }
        return String.valueOf(obj);
    }

}
